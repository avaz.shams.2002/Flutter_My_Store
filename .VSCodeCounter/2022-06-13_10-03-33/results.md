# Summary

Date : 2022-06-13 10:03:33

Directory c:\\Users\\avazs.LAPTOP-I4FB2CH8\\StudioProjects\\My_Store

Total : 108 files,  17096 codes, 647 comments, 1166 blanks, all 18909 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 84 | 16,626 | 520 | 1,102 | 18,248 |
| JSON | 3 | 184 | 0 | 2 | 186 |
| XML | 9 | 123 | 46 | 11 | 180 |
| Groovy | 3 | 93 | 3 | 22 | 118 |
| YAML | 2 | 41 | 77 | 18 | 136 |
| Swift | 1 | 12 | 0 | 2 | 14 |
| Properties | 2 | 8 | 1 | 2 | 11 |
| Markdown | 2 | 4 | 0 | 3 | 7 |
| Kotlin | 1 | 4 | 0 | 3 | 7 |
| C++ | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 108 | 17,096 | 647 | 1,166 | 18,909 |
| android | 14 | 206 | 48 | 36 | 290 |
| android\\app | 10 | 162 | 47 | 25 | 234 |
| android\\app\\src | 8 | 66 | 44 | 12 | 122 |
| android\\app\\src\\debug | 1 | 4 | 3 | 1 | 8 |
| android\\app\\src\\main | 6 | 58 | 38 | 10 | 106 |
| android\\app\\src\\main\\kotlin | 1 | 4 | 0 | 3 | 7 |
| android\\app\\src\\main\\kotlin\\com | 1 | 4 | 0 | 3 | 7 |
| android\\app\\src\\main\\kotlin\\com\\example | 1 | 4 | 0 | 3 | 7 |
| android\\app\\src\\main\\kotlin\\com\\example\\my_store | 1 | 4 | 0 | 3 | 7 |
| android\\app\\src\\main\\res | 4 | 26 | 32 | 6 | 64 |
| android\\app\\src\\main\\res\\drawable | 1 | 4 | 7 | 2 | 13 |
| android\\app\\src\\main\\res\\drawable-v21 | 1 | 4 | 7 | 2 | 13 |
| android\\app\\src\\main\\res\\values | 1 | 9 | 9 | 1 | 19 |
| android\\app\\src\\main\\res\\values-night | 1 | 9 | 9 | 1 | 19 |
| android\\app\\src\\profile | 1 | 4 | 3 | 1 | 8 |
| android\\gradle | 1 | 5 | 1 | 1 | 7 |
| android\\gradle\\wrapper | 1 | 5 | 1 | 1 | 7 |
| ios | 7 | 222 | 2 | 9 | 233 |
| ios\\Runner | 7 | 222 | 2 | 9 | 233 |
| ios\\Runner\\Assets.xcassets | 3 | 148 | 0 | 4 | 152 |
| ios\\Runner\\Assets.xcassets\\AppIcon.appiconset | 1 | 122 | 0 | 1 | 123 |
| ios\\Runner\\Assets.xcassets\\LaunchImage.imageset | 2 | 26 | 0 | 3 | 29 |
| ios\\Runner\\Base.lproj | 2 | 61 | 2 | 2 | 65 |
| lib | 83 | 16,613 | 509 | 1,095 | 18,217 |
| lib\\database | 1 | 508 | 29 | 112 | 649 |
| lib\\database\\db | 1 | 508 | 29 | 112 | 649 |
| lib\\models | 9 | 423 | 0 | 36 | 459 |
| lib\\pages | 15 | 6,233 | 242 | 242 | 6,717 |
| lib\\providers | 18 | 928 | 36 | 186 | 1,150 |
| lib\\screens | 20 | 5,028 | 137 | 262 | 5,427 |
| lib\\widgets | 19 | 3,403 | 65 | 255 | 3,723 |
| test | 1 | 13 | 11 | 7 | 31 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)