# Details

Date : 2022-06-13 10:03:33

Directory c:\\Users\\avazs.LAPTOP-I4FB2CH8\\StudioProjects\\My_Store

Total : 108 files,  17096 codes, 647 comments, 1166 blanks, all 18909 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 1 | 0 | 1 | 2 |
| [analysis_options.yaml](/analysis_options.yaml) | YAML | 3 | 23 | 4 | 30 |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | 57 | 3 | 13 | 73 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | 39 | 0 | 0 | 39 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | 28 | 6 | 1 | 35 |
| [android/app/src/main/kotlin/com/example/my_store/MainActivity.kt](/android/app/src/main/kotlin/com/example/my_store/MainActivity.kt) | Kotlin | 4 | 0 | 3 | 7 |
| [android/app/src/main/res/drawable-v21/launch_background.xml](/android/app/src/main/res/drawable-v21/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [android/app/src/main/res/drawable/launch_background.xml](/android/app/src/main/res/drawable/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [android/app/src/main/res/values-night/styles.xml](/android/app/src/main/res/values-night/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [android/app/src/profile/AndroidManifest.xml](/android/app/src/profile/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [android/build.gradle](/android/build.gradle) | Groovy | 28 | 0 | 5 | 33 |
| [android/gradle.properties](/android/gradle.properties) | Properties | 3 | 0 | 1 | 4 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 5 | 1 | 1 | 7 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | 8 | 0 | 4 | 12 |
| [ios/Runner/AppDelegate.swift](/ios/Runner/AppDelegate.swift) | Swift | 12 | 0 | 2 | 14 |
| [ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json](/ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json) | JSON | 122 | 0 | 1 | 123 |
| [ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json](/ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json) | JSON | 23 | 0 | 1 | 24 |
| [ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md](/ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md) | Markdown | 3 | 0 | 2 | 5 |
| [ios/Runner/Base.lproj/LaunchScreen.storyboard](/ios/Runner/Base.lproj/LaunchScreen.storyboard) | XML | 36 | 1 | 1 | 38 |
| [ios/Runner/Base.lproj/Main.storyboard](/ios/Runner/Base.lproj/Main.storyboard) | XML | 25 | 1 | 1 | 27 |
| [ios/Runner/Runner-Bridging-Header.h](/ios/Runner/Runner-Bridging-Header.h) | C++ | 1 | 0 | 1 | 2 |
| [lib/database/db/product_maker_db.dart](/lib/database/db/product_maker_db.dart) | Dart | 508 | 29 | 112 | 649 |
| [lib/main.dart](/lib/main.dart) | Dart | 90 | 0 | 2 | 92 |
| [lib/models/category_maker_model.dart](/lib/models/category_maker_model.dart) | Dart | 30 | 0 | 3 | 33 |
| [lib/models/contragent_maker_model.dart](/lib/models/contragent_maker_model.dart) | Dart | 93 | 0 | 7 | 100 |
| [lib/models/dicount_detail.dart](/lib/models/dicount_detail.dart) | Dart | 9 | 0 | 3 | 12 |
| [lib/models/discount_maker_model.dart](/lib/models/discount_maker_model.dart) | Dart | 15 | 0 | 3 | 18 |
| [lib/models/product_make_model.dart](/lib/models/product_make_model.dart) | Dart | 152 | 0 | 6 | 158 |
| [lib/models/receipt_model.dart](/lib/models/receipt_model.dart) | Dart | 28 | 0 | 3 | 31 |
| [lib/models/receipt_product_model.dart](/lib/models/receipt_product_model.dart) | Dart | 28 | 0 | 3 | 31 |
| [lib/models/supplier_invoice_details_model.dart](/lib/models/supplier_invoice_details_model.dart) | Dart | 38 | 0 | 4 | 42 |
| [lib/models/supplier_invoice_model.dart](/lib/models/supplier_invoice_model.dart) | Dart | 30 | 0 | 4 | 34 |
| [lib/pages/archive_page.dart](/lib/pages/archive_page.dart) | Dart | 144 | 0 | 4 | 148 |
| [lib/pages/categoryPage.dart](/lib/pages/categoryPage.dart) | Dart | 147 | 1 | 4 | 152 |
| [lib/pages/create_product_page.dart](/lib/pages/create_product_page.dart) | Dart | 1,909 | 64 | 51 | 2,024 |
| [lib/pages/discount_page.dart](/lib/pages/discount_page.dart) | Dart | 146 | 0 | 6 | 152 |
| [lib/pages/home_page.dart](/lib/pages/home_page.dart) | Dart | 77 | 3 | 5 | 85 |
| [lib/pages/kontragent_page.dart](/lib/pages/kontragent_page.dart) | Dart | 176 | 5 | 8 | 189 |
| [lib/pages/login_page.dart](/lib/pages/login_page.dart) | Dart | 258 | 38 | 14 | 310 |
| [lib/pages/oplata_page.dart](/lib/pages/oplata_page.dart) | Dart | 233 | 1 | 14 | 248 |
| [lib/pages/payment_account_page.dart](/lib/pages/payment_account_page.dart) | Dart | 195 | 0 | 11 | 206 |
| [lib/pages/receipt_page.dart](/lib/pages/receipt_page.dart) | Dart | 150 | 18 | 5 | 173 |
| [lib/pages/receipt_page_about.dart](/lib/pages/receipt_page_about.dart) | Dart | 311 | 8 | 22 | 341 |
| [lib/pages/registor_page.dart](/lib/pages/registor_page.dart) | Dart | 166 | 19 | 10 | 195 |
| [lib/pages/sales_page.dart](/lib/pages/sales_page.dart) | Dart | 349 | 9 | 12 | 370 |
| [lib/pages/supplier_creater_page.dart](/lib/pages/supplier_creater_page.dart) | Dart | 1,131 | 15 | 18 | 1,164 |
| [lib/pages/supplier_page.dart](/lib/pages/supplier_page.dart) | Dart | 841 | 61 | 58 | 960 |
| [lib/providers/barcode_provider.dart](/lib/providers/barcode_provider.dart) | Dart | 39 | 9 | 9 | 57 |
| [lib/providers/category_provider.dart](/lib/providers/category_provider.dart) | Dart | 58 | 0 | 13 | 71 |
| [lib/providers/check_field_login_page.dart](/lib/providers/check_field_login_page.dart) | Dart | 22 | 0 | 4 | 26 |
| [lib/providers/check_field_reg_page.dart](/lib/providers/check_field_reg_page.dart) | Dart | 15 | 0 | 3 | 18 |
| [lib/providers/contact_person_provider.dart](/lib/providers/contact_person_provider.dart) | Dart | 84 | 15 | 16 | 115 |
| [lib/providers/contragent_provider.dart](/lib/providers/contragent_provider.dart) | Dart | 60 | 0 | 13 | 73 |
| [lib/providers/counries_from_api_provider.dart](/lib/providers/counries_from_api_provider.dart) | Dart | 31 | 0 | 6 | 37 |
| [lib/providers/current_page_provider.dart](/lib/providers/current_page_provider.dart) | Dart | 9 | 0 | 4 | 13 |
| [lib/providers/discount_provider.dart](/lib/providers/discount_provider.dart) | Dart | 64 | 0 | 14 | 78 |
| [lib/providers/payment_account_provider.dart](/lib/providers/payment_account_provider.dart) | Dart | 39 | 0 | 8 | 47 |
| [lib/providers/prices_provider.dart](/lib/providers/prices_provider.dart) | Dart | 39 | 3 | 11 | 53 |
| [lib/providers/product_cart_provider.dart](/lib/providers/product_cart_provider.dart) | Dart | 14 | 0 | 4 | 18 |
| [lib/providers/product_maker_provider.dart](/lib/providers/product_maker_provider.dart) | Dart | 157 | 9 | 25 | 191 |
| [lib/providers/receipt_and_tovar_provider.dart](/lib/providers/receipt_and_tovar_provider.dart) | Dart | 102 | 0 | 19 | 121 |
| [lib/providers/shtrihcode_provider.dart](/lib/providers/shtrihcode_provider.dart) | Dart | 25 | 0 | 6 | 31 |
| [lib/providers/supplier_invoice_details_provider.dart](/lib/providers/supplier_invoice_details_provider.dart) | Dart | 71 | 0 | 13 | 84 |
| [lib/providers/supplier_invoice_provider.dart](/lib/providers/supplier_invoice_provider.dart) | Dart | 59 | 0 | 12 | 71 |
| [lib/providers/user_check.dart](/lib/providers/user_check.dart) | Dart | 40 | 0 | 6 | 46 |
| [lib/screens/category_archive.dart](/lib/screens/category_archive.dart) | Dart | 184 | 1 | 4 | 189 |
| [lib/screens/category_product_screen.dart](/lib/screens/category_product_screen.dart) | Dart | 183 | 0 | 6 | 189 |
| [lib/screens/catergory_creator_screen.dart](/lib/screens/catergory_creator_screen.dart) | Dart | 366 | 1 | 15 | 382 |
| [lib/screens/change_price_screen.dart](/lib/screens/change_price_screen.dart) | Dart | 665 | 26 | 50 | 741 |
| [lib/screens/change_price_supplier_screen.dart](/lib/screens/change_price_supplier_screen.dart) | Dart | 389 | 21 | 28 | 438 |
| [lib/screens/check_screen.dart](/lib/screens/check_screen.dart) | Dart | 334 | 8 | 10 | 352 |
| [lib/screens/contragent_archive.dart](/lib/screens/contragent_archive.dart) | Dart | 73 | 7 | 5 | 85 |
| [lib/screens/counterparty_screen.dart](/lib/screens/counterparty_screen.dart) | Dart | 60 | 3 | 4 | 67 |
| [lib/screens/countries.dart](/lib/screens/countries.dart) | Dart | 55 | 0 | 4 | 59 |
| [lib/screens/discount_creator_screen.dart](/lib/screens/discount_creator_screen.dart) | Dart | 178 | 0 | 14 | 192 |
| [lib/screens/oplacheno_screen.dart](/lib/screens/oplacheno_screen.dart) | Dart | 70 | 0 | 6 | 76 |
| [lib/screens/oplata_razdelno_screen.dart](/lib/screens/oplata_razdelno_screen.dart) | Dart | 25 | 0 | 5 | 30 |
| [lib/screens/postavshik_tovar.dart](/lib/screens/postavshik_tovar.dart) | Dart | 38 | 1 | 3 | 42 |
| [lib/screens/product_screen.dart](/lib/screens/product_screen.dart) | Dart | 267 | 0 | 10 | 277 |
| [lib/screens/return_of_supplier_details_screen.dart](/lib/screens/return_of_supplier_details_screen.dart) | Dart | 555 | 31 | 29 | 615 |
| [lib/screens/return_of_tovar_screen.dart](/lib/screens/return_of_tovar_screen.dart) | Dart | 538 | 29 | 27 | 594 |
| [lib/screens/side_bar.dart](/lib/screens/side_bar.dart) | Dart | 212 | 1 | 10 | 223 |
| [lib/screens/supplier_each_detail_screen.dart](/lib/screens/supplier_each_detail_screen.dart) | Dart | 333 | 3 | 17 | 353 |
| [lib/screens/tovar_about.dart](/lib/screens/tovar_about.dart) | Dart | 457 | 2 | 12 | 471 |
| [lib/screens/type_code.dart](/lib/screens/type_code.dart) | Dart | 46 | 3 | 3 | 52 |
| [lib/widgets/all_after_category_product_widget.dart](/lib/widgets/all_after_category_product_widget.dart) | Dart | 231 | 8 | 14 | 253 |
| [lib/widgets/all_bottom_category_products_widget.dart](/lib/widgets/all_bottom_category_products_widget.dart) | Dart | 225 | 8 | 15 | 248 |
| [lib/widgets/all_categories_widget.dart](/lib/widgets/all_categories_widget.dart) | Dart | 91 | 1 | 7 | 99 |
| [lib/widgets/all_products_widget.dart](/lib/widgets/all_products_widget.dart) | Dart | 313 | 8 | 13 | 334 |
| [lib/widgets/all_supplier_invoice_details_search.dart](/lib/widgets/all_supplier_invoice_details_search.dart) | Dart | 95 | 0 | 12 | 107 |
| [lib/widgets/barcode_widget.dart](/lib/widgets/barcode_widget.dart) | Dart | 21 | 2 | 5 | 28 |
| [lib/widgets/categoryPage_search_widget.dart](/lib/widgets/categoryPage_search_widget.dart) | Dart | 91 | 0 | 7 | 98 |
| [lib/widgets/category_search_widget.dart](/lib/widgets/category_search_widget.dart) | Dart | 121 | 0 | 8 | 129 |
| [lib/widgets/contact_person_widget.dart](/lib/widgets/contact_person_widget.dart) | Dart | 122 | 1 | 7 | 130 |
| [lib/widgets/contragent_search_widget.dart](/lib/widgets/contragent_search_widget.dart) | Dart | 208 | 10 | 19 | 237 |
| [lib/widgets/custom_show_delegate_widget.dart](/lib/widgets/custom_show_delegate_widget.dart) | Dart | 128 | 0 | 11 | 139 |
| [lib/widgets/discount_search_widget.dart](/lib/widgets/discount_search_widget.dart) | Dart | 29 | 0 | 4 | 33 |
| [lib/widgets/each_products_search_widget.dart](/lib/widgets/each_products_search_widget.dart) | Dart | 109 | 0 | 10 | 119 |
| [lib/widgets/producf_search_widget.dart](/lib/widgets/producf_search_widget.dart) | Dart | 117 | 0 | 9 | 126 |
| [lib/widgets/search_receipt_page.dart](/lib/widgets/search_receipt_page.dart) | Dart | 317 | 18 | 27 | 362 |
| [lib/widgets/shtrihcode_widget.dart](/lib/widgets/shtrihcode_widget.dart) | Dart | 34 | 1 | 4 | 39 |
| [lib/widgets/supplier_invoice_each_search.dart](/lib/widgets/supplier_invoice_each_search.dart) | Dart | 432 | 6 | 38 | 476 |
| [lib/widgets/supplier_returned_search_widget.dart](/lib/widgets/supplier_returned_search_widget.dart) | Dart | 374 | 2 | 35 | 411 |
| [lib/widgets/widget.dart](/lib/widgets/widget.dart) | Dart | 345 | 0 | 10 | 355 |
| [pubspec.yaml](/pubspec.yaml) | YAML | 38 | 54 | 14 | 106 |
| [test/widget_test.dart](/test/widget_test.dart) | Dart | 13 | 11 | 7 | 31 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)