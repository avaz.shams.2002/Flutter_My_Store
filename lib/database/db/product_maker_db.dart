import 'dart:io';

import 'package:my_store/models/category_maker_model.dart';
import 'package:my_store/models/client_model.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/models/receipt_model.dart';
import 'package:my_store/models/receipt_product_model.dart';
import 'package:my_store/models/supplier_invoice_details_model.dart';
import 'package:my_store/models/supplier_invoice_model.dart';
import 'package:my_store/providers/discount_provider.dart';
import 'package:my_store/widgets/contact_person_widget.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

import 'package:path/path.dart';

import '../../models/contragent_maker_model.dart';
import '../../models/discount_maker_model.dart';

//  String? total;
//   String? cashier;
//   String? kassa;
//   String? cash;
//   String? dateTime;
//   String? changeMoney;

class ProductMakerDb {
  static final Future<Database> database = getDatabasesPath().then((value) {
    return openDatabase(join(value, 'product.db'),
        onCreate: (db, version) async {
          await db.execute(
              "CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image TEXT, archiv INTEGER, naimenovanie TEXT,"
              "groupe TEXT, code TEXT, artikul TEXT, minimalPrice REAL, minimalCurrency TEXT, zakupkiPrice REAL,"
              "zakupkiCurrency TEXT, prodazhiPrice REAL, prodazhiCurrency TEXT, opisanie TEXT, strana TEXT,"
              "uchetPoSerNomeram INTEGER, vesTovar INTEGER, alhogolProduct INTEGER, aksizMark INTEGER,"
              "kodVidaProduct TEXT, emkost TEXT, krepost TEXT,"
              "ves TEXT, obyom TEXT, edIzmereniy TEXT, nds TEXT, neSnizhemiyOstatok TEXT, postavshik TEXT,"
              " postavshikId INTEGER, shtrihkod TEXT, upakovka TEXT, kolvoUpakovka TEXT, vladelesSot TEXT, vladalesOtdel TEXT,"
              " discount_id INTEGER, quantity REAL, category_id TEXT )");

          await db.execute(
              "CREATE TABLE supplier (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, archiv INTEGER, status TEXT, "
              "naimenovanie TEXT, code TEXT, email TEXT, phoneNumber TEXT, factAdress TEXT, comment TEXT, typeContragent TEXT, "
              "polNaimenovanie TEXT, yurAdress TEXT, vladelesSot TEXT, vladalesOtdel TEXT)");

          await db.execute(
              "CREATE TABLE contact_face (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
              "contragent_id INTEGER, name TEXT, dolzhnost TEXT, phoneNumber TEXT, email TEXT, comment TEXT)");

          await db.execute(
              "CREATE TABLE category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, picture TEXT, created_at TEXT,"
              " updated_at TEXT, visible INTEGER, colorInt INTEGER)");

          await db.execute(
              "CREATE TABLE clients (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, email TEXT, phoneNumber TEXT,"
              " address TEXT, city TEXT, region TEXT, mailIndex TEXT, country TEXT, clientCode TEXT, clientNote TEXT, lastVisit TEXT, numberOfVisits INTEGER)");

          await db.execute(
              "CREATE TABLE discount (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, value REAL, percentOrSumma INTEGER)");

          await db.execute(
              "CREATE TABLE discount_details(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, discount_id INTEGER, product_id INTEGER)");

          await db.execute(
              "CREATE TABLE receipt (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, total TEXT, cashier TEXT, kassa TEXT,"
              " cash TEXT, dateTime TEXT, changeMoney TEXT, client_id INTEGER)");

          await db.execute(
              "CREATE TABLE product_receipt (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, receipt_id INTEGER, naimenovanie TEXT,"
              " quantity REAL, prodazhiPrice REAL, kolvoUpakovke INTEGER, returned INTEGER)");

          await db.execute(
              "CREATE TABLE supplier_invoice (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, supplier_id INTEGER, datatime TEXT,"
              "total REAL, quantity INTEGER, returned INTEGER, changeMoney REAL)");

          await db.execute(
              "CREATE TABLE supplier_invoice_details (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,supplier_invoice_id INTEGER,"
              "img TEXT, name TEXT, product_id INTEGER, quantity REAL, price REAL, barcode TEXT, kolvoUpakovka INTEGER, returned INTEGER)");
        },
        version: 1,
        onUpgrade: (db, oldVersion, newVersion) async {
          if (oldVersion < 1) {
            db.execute(
                "CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image TEXT, archiv INTEGER, naimenovanie TEXT,"
                "groupe TEXT, code TEXT, artikul TEXT, minimalPrice REAL, minimalCurrency TEXT, zakupkiPrice REAL,"
                "zakupkiCurrency TEXT, prodazhiPrice REAL, prodazhiCurrency TEXT, opisanie TEXT, strana TEXT,"
                "uchetPoSerNomeram INTEGER, vesTovar INTEGER, alhogolProduct INTEGER, aksizMark INTEGER,"
                "kodVidaProduct TEXT, emkost TEXT, krepost TEXT,"
                "ves TEXT, obyom TEXT, edIzmereniy TEXT, nds TEXT, neSnizhemiyOstatok TEXT, postavshik TEXT,"
                "postavshikId INTEGER, shtrihkod TEXT, upakovka TEXT, kolvoUpakovka TEXT, vladelesSot TEXT, vladalesOtdel TEXT, discount_id INTEGER, "
                " quantity REAL, category_id TEXT)");

            db.execute(
                "CREATE TABLE supplier (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, archiv INTEGER, status TEXT, "
                "naimenovanie TEXT, code TEXT, email TEXT, phoneNumber TEXT, factAdress TEXT, comment TEXT, typeContragent TEXT, "
                "polNaimenovanie TEXT, yurAdress TEXT, vladelesSot TEXT, vladalesOtdel TEXT)");

            db.execute(
                "CREATE TABLE contact_face (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                "contragent_id INTEGER, name TEXT, dolzhnost TEXT, phoneNumber TEXT, email TEXT, comment TEXT)");

            db.execute(
                "CREATE TABLE category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, picture TEXT, created_at TEXT,"
                " updated_at TEXT, visible INTEGER, colorInt INTEGER)");
            db.execute(
                "CREATE TABLE clients (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, email TEXT, phoneNumber TEXT,"
                " address TEXT, city TEXT, region TEXT, mailIndex TEXT, country TEXT, clientCode TEXT, clientNote TEXT, lastVisit TEXT, numberOfVisits INTEGER)");

            db.execute(
                "CREATE TABLE discount (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, value REAL, percentOrSumma INTEGER)");

            db.execute(
                "CREATE TABLE discount_details(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, discount_id INTEGER, product_id INTEGER PRIMARY KEY)");

            db.execute(
                "CREATE TABLE receipt (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, total TEXT, cashier TEXT, kassa TEXT,"
                " cash TEXT, dateTime TEXT, changeMoney TEXT, client_id INTEGER)");
            db.execute(
                "CREATE TABLE product_receipt (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, receipt_id INTEGER, naimenovanie TEXT,"
                " quantity REAL, prodazhiPrice REAL, kolvoUpakovke INTEGER, returned INTEGER)");

            db.execute(
                "CREATE TABLE supplier_invoice (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, supplier_id INTEGER, datatime TEXT,"
                "total REAL, quantity INTEGER, returned INTEGER, changeMoney REAL)");

            db.execute(
                "CREATE TABLE supplier_invoice_details (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,supplier_invoice_id INTEGER,"
                "img TEXT, name TEXT, product_id INTEGER, quantity REAL, price REAL, barcode TEXT, kolvoUpakovka INTEGER, returned INTEGER)");
          }
        });
  });

  static Future<void> insertTovar(ProductMaker productMaker) async {
    final db = await database;

    productMaker.id = await db.insert("products", productMaker.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> insertContragent(Contragent contragent) async {
    final db = await database;

    contragent.id = await db.insert('supplier', contragent.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> insertClient(Client client) async {
    final db = await database;

    client.id = await db.insert('clients', client.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> addItemToDbProduct(ProductMaker productMaker) async {
    final db = await database;

    // if (cheack.first['quantity'] == 1) {
    //   productMaker.quantity = 0;
    //   db.update('products', productMaker.toMap(),
    //       where: "id = ?", whereArgs: [productMaker.id]);
    // } else {
    productMaker.quantity += 1;
    db.update("products", productMaker.toMap(),
        where: "id = ? ", whereArgs: [productMaker.id]);
    // }
  }

  static Future<void> insertIntoDiscount(Discount discount) async {
    final db = await database;
    discount.id = await db.insert('discount', discount.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> setZeroItemToDbProduct(ProductMaker productMaker) async {
    final db = await database;
    productMaker.quantity = 0;
    // productMaker.kolvoUpaVibr = 0;
    db.update("products", productMaker.toMap(),
        where: "id = ? ", whereArgs: [productMaker.id]);
    // }
  }

  static Future<void> insertCategory(Category category) async {
    final db = await database;

    category.id = await db.insert('category', category.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> insertSupplierInvoiceDetails(
      List<SupplierInvoiceDetails> supplierInvoiceDetails,
      int supplierInvoiceId) async {
    final db = await database;

    for (int i = 0; i < supplierInvoiceDetails.length; i++) {
      supplierInvoiceDetails[i].supplierInvoiceId = await supplierInvoiceId;
      supplierInvoiceDetails[i].id = await db.insert(
          'supplier_invoice_details', supplierInvoiceDetails[i].toMap(),
          conflictAlgorithm: ConflictAlgorithm.ignore);
    }
  }

  static Future<void> insertReceipt(Receipt receipt) async {
    final db = await database;

    receipt.id = await db.insert('receipt', receipt.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> insertProductReceipt(
      int receiptId, List<ReceiptProduct> listProduct) async {
    final db = await database;

    for (int i = 0; i < listProduct.length; i++) {
      listProduct[i].receiptId = receiptId;

      listProduct[i].id = await db.insert(
          'product_receipt', listProduct[i].toMapForReceiptTable(),
          conflictAlgorithm: ConflictAlgorithm.ignore);

      // await db.rawInsert(
      //       "INSERT INTO product_receipt(receipt_id, naimenovanie, quantity, prodazhiPrice) VALUES ("
      //       "$receiptId, '${listProduct[i].naimenovanie}', ${listProduct[i].quantity}, '${listProduct[i].prodazhiPrice}')");
    }
  }

  static Future<void> insertIntoSupplierInvoice(
      SupplierInvoice supplierInvoice) async {
    final db = await database;

    supplierInvoice.id = await db.insert(
        'supplier_invoice', supplierInvoice.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  static Future<void> updateQuaintityAndPrice(ProductMaker productMaker) async {
    final db = await database;

    await db.update("products", productMaker.toMap(),
        where: "id = ?", whereArgs: [productMaker.id]);
  }

  static Future<void> updateClient(Client client) async {
    final db = await database;

    await db.update('clients', client.toMap(),
        where: 'id = ?', whereArgs: [client.id]);
  }

  static Future<void> updateDiscount(Discount discount) async {
    final db = await database;

    await db.update('discount', discount.toMap(),
        where: "id = ?", whereArgs: [discount.id]);
  }

  static Future<void> insertToContactFace(
      int contragentId, List<ContactPersonWidget> contactFace) async {
    final db = await database;

    for (int i = 0; i < contactFace.length; i++) {
      if (contactFace[i].id != null) {
        await db.rawUpdate(
            'UPDATE contact_face SET name = "${contactFace[i].name.text}", dolzhnost = "${contactFace[i].dolzhnost.text}",'
            ' phoneNumber = "${contactFace[i].telephone.text}", email = "${contactFace[i].email.text}",'
            ' comment = "${contactFace[i].comment.text}" WHERE id == ${contactFace[i].id}');
        continue;
      }
      contactFace[i].id = await db.rawInsert(
          'INSERT INTO contact_face (contragent_id , name, dolzhnost, phoneNumber,email ,comment) VALUES'
          ' ($contragentId, "${contactFace[i].name.text}", "${contactFace[i].dolzhnost.text}", "${contactFace[i].telephone.text}",'
          ' "${contactFace[i].email.text}", "${contactFace[i].comment.text}")');
    }
  }

  static Future<void> updateTovar(ProductMaker productMaker) async {
    final db = await database;

    await db.update('products', productMaker.toMap(),
        where: 'id = ?', whereArgs: [productMaker.id]);
  }

  static Future<void> updateCategory(Category category) async {
    final db = await database;

    await db.update("category", category.toMap(),
        where: "id = ? ", whereArgs: [category.id]);
  }

  static Future<void> updateContragent(Contragent contragent) async {
    final db = await database;
    await db.update("supplier", contragent.toMap(),
        where: "id = ?", whereArgs: [contragent.id]);
  }

  static Future<void> setZeroForOneProduct(ProductMaker productMaker) async {
    final db = await database;
    productMaker.quantity = 0;
    await db.update('products', productMaker.toMap(),
        where: "id = ?", whereArgs: [productMaker.id]);
  }

  static Future<void> deleteProduct(int id) async {
    final db = await database;

    await db.delete("products", where: "id = ?", whereArgs: [id]);
  }

  static Future<void> deleteDiscountDetailById(int id) async {
    final db = await database;

    await db
        .delete('discount_details', where: "product_id = ?", whereArgs: [id]);
  }

  static Future<void> setZeroQuantity(int zero, double zeroDouble) async {
    final db = await database;

    await db.rawUpdate(
        "UPDATE products SET quantity = $zero AND kolvoUpaVibr = $zero WHERE quantity > 0");
    await db.rawUpdate(
        "UPDATE products SET discount_id = null WHERE discount_id > 0");
  }

  static Future<void> deleteContragent(int id) async {
    final db = await database;
    await db.delete('supplier', where: 'id = ?', whereArgs: [id]);
  }

  static Future<void> setReturnSupplierInvoiceDetails(
      List<SupplierInvoiceDetails> list) async {
    final db = await database;

    for (int i = 0; i < list.length; i++) {
      list[i].returned = 1;
      await db.update('supplier_invoice_details', list[i].toMap(),
          where: "id = ?", whereArgs: [list[i].id]);
    }
  }

  static Future<void> returnProductReceipt(List<ReceiptProduct> list) async {
    final db = await database;

    for (int i = 0; i < list.length; i++) {
      list[i].returned = 1;
      await db.update('product_receipt', list[i].toMapForReceiptTable(),
          where: "id = ?", whereArgs: [list[i].id]);
    }
  }

  static Future<void> justUpdateReceipt(ReceiptProduct list) async {
    final db = await database;

    await db.update("product_receipt", list.toMapForReceiptTable(),
        where: "id = ?", whereArgs: [list.id]);
  }

  static Future<void> justUpdateSupplierInvoiceDetail(
      SupplierInvoiceDetails supplierInvoiceDetails) async {
    final db = await database;

    await db.update('supplier_invoice_details', supplierInvoiceDetails.toMap(),
        where: "id = ?", whereArgs: [supplierInvoiceDetails.id]);
  }

  static Future<void> deleteContactFace(int id) async {
    final db = await database;
    await db
        .delete('contact_face', where: 'contragent_id = ?', whereArgs: [id]);
  }

  static Future<void> deleteContactFaceWithId(int id) async {
    final db = await database;

    await db.delete('contact_face', where: 'id = ?', whereArgs: [id]);
  }

  ///
  ///
  ///
  static Future<void> deleteSupplierInvoice(int id) async {
    final db = await database;
    await db.delete('supplier_invoice', where: "id = ?", whereArgs: [id]);
  }

  static Future<void> deleteSupplierInvoiceDetails(
      List<SupplierInvoiceDetails> list) async {
    final db = await database;

    for (var all in list) {
      await db.delete('supplier_invoice_details',
          where: "id = ?", whereArgs: [all.id]);
    }
  }

  ///
  ///
  ///
  static Future<void> updateReceipt(
      int id, String changeMoney, String total) async {
    final db = await database;

    await db.rawUpdate(
        "UPDATE receipt SET changeMoney = $changeMoney WHERE id == $id");
    await db.rawUpdate("UPDATE receipt SET total = $total WHERE id == $id");
  }

  ///
  ///
  ///
  static Future<void> deleteReceipt(int id) async {
    final db = await database;

    await db.delete('receipt', where: 'id = ?', whereArgs: [id]);
  }

  static Future<void> deleteReceiptProducts(List<ReceiptProduct> list) async {
    final db = await database;

    for (var all in list) {
      await db.delete('product_receipt', where: "id = ?", whereArgs: [all.id]);
    }
  }

  ///
  ///
  ///

  static Future<void> updateSupplierInvoice(int id, double total) async {
    final db = await database;

    await db.rawUpdate(
        "UPDATE supplier_invoice SET total = $total WHERE id == $id");
  }

  static Future<void> setReturnToSupplierInvoice(
      SupplierInvoice supplierInvoice) async {
    final db = await database;
    supplierInvoice.returned = 1;
    await db.update('supplier_invoice', supplierInvoice.toMap(),
        where: "id = ?", whereArgs: [supplierInvoice.id]);
  }

  static Future<void> deleteCategory(int id) async {
    final db = await database;

    await db.delete('category', where: 'id = ?', whereArgs: [id]);
  }

  static Future<List<Category>> getCategoryDb() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('category');
    return List.generate(maps.length, (index) {
      return Category(
          id: maps[index]['id'],
          name: maps[index]['name'],
          picture: maps[index]['picture'],
          createdAt: maps[index]['created_at'],
          updatedAt: maps[index]['updated_at'],
          visible: maps[index]['visible'],
          colorInt: maps[index]['colorInt']);
    });
  }

  static Future<List<Receipt>> getReceipt() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('receipt');
    final List<Map<String, dynamic>> mapsForClients = await db.query('clients');
    final List<Map<String, dynamic>> mapsForReceiptProduct =
        await db.query('product_receipt');

    var listOfProductReceipt =
        List.generate(mapsForReceiptProduct.length, (index) {
      return ReceiptProduct(
          id: mapsForReceiptProduct[index]['id'],
          receiptId: mapsForReceiptProduct[index]['receipt_id'],
          naimenovanie: mapsForReceiptProduct[index]['naimenovanie'],
          quantity: mapsForReceiptProduct[index]['quantity'],
          kolvoUpakovke: mapsForReceiptProduct[index]['kolvoUpakovke'],
          prodazhiPrice: mapsForReceiptProduct[index]['prodazhiPrice'],
          returned: mapsForReceiptProduct[index]['returned']);
    });

    var listOfClients = List.generate(mapsForClients.length, (index) {
      return Client(
          id: mapsForClients[index]['id'],
          name: mapsForClients[index]['name'],
          email: mapsForClients[index]['email'],
          phoneNumber: mapsForClients[index]['phoneNumber'],
          address: mapsForClients[index]['address'],
          city: mapsForClients[index]['city'],
          region: mapsForClients[index]['region'],
          mailIndex: mapsForClients[index]['mailIndex'],
          country: mapsForClients[index]['country'],
          clientCode: mapsForClients[index]['clientCode'],
          clientNote: mapsForClients[index]['clientNote'],
          lastVisit: mapsForClients[index]['lastVisit'],
          numberOfVisits: mapsForClients[index]['numberOfVisits']);
    });

    return List.generate(maps.length, (index) {
      return Receipt(
          id: maps[index]['id'],
          total: maps[index]['total'],
          cashier: maps[index]['cashier'],
          kassa: maps[index]['kassa'],
          cash: maps[index]['cash'],
          dateTime: maps[index]['dateTime'],
          changeMoney: maps[index]['changeMoney'],
          client: listOfClients
                  .every((element) => element.id != maps[index]['client_id'])
              ? null
              : listOfClients.firstWhere(
                  (element) => element.id == maps[index]['client_id']),
          listReceiptProduct: listOfProductReceipt
              .where((element) => element.receiptId == maps[index]['id'])
              .toList());
    });
  }

  static Future<List<ReceiptProduct>> getReceiptProductId() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('product_receipt');
    return List.generate(maps.length, (index) {
      return ReceiptProduct(
          id: maps[index]['id'],
          receiptId: maps[index]['receipt_id'],
          naimenovanie: maps[index]['naimenovanie'],
          quantity: maps[index]['quantity'],
          kolvoUpakovke: maps[index]['kolvoUpakovke'],
          prodazhiPrice: maps[index]['prodazhiPrice'],
          returned: maps[index]['returned']);
    });
  }

  static Future<List<ProductMaker>> getProduct() async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('products');

    final List<Map<String, dynamic>> mapsFromDiscount =
        await db.query('discount');

    var listOfDiscount = List.generate(mapsFromDiscount.length, (index) {
      return Discount(
        id: mapsFromDiscount[index]['id'],
        name: mapsFromDiscount[index]['name'],
        value: mapsFromDiscount[index]['value'],
        percentOrSumma: mapsFromDiscount[index]['percentOrSumma'],
      );
    });

    return List.generate(maps.length, (index) {
      return ProductMaker(
          id: maps[index]['id'],
          image: File(maps[index]['image']),
          archiv: maps[index]['archiv'],
          naimenovanie: maps[index]['naimenovanie'],
          groupe: maps[index]['groupe'],
          code: maps[index]['code'],
          artikul: maps[index]['artikul'],
          minimalPrice: maps[index]['minimalPrice'],
          minimalCurrency: maps[index]['minimalCurrency'],
          zakupkiPrice: maps[index]['zakupkiPrice'],
          zakupkiCurrency: maps[index]['zakupkiCurrency'],
          prodazhiPrice: maps[index]['prodazhiPrice'],
          prodazhiCurrency: maps[index]['prodazhiCurrency'],
          opisanie: maps[index]['opisanie'],
          strana: maps[index]['strana'],
          ves: maps[index]['ves'],
          obyom: maps[index]['obyom'],
          edIzmereniy: maps[index]['edIzmereniy'],
          nds: maps[index]['nds'],
          neSnizhemiyOstatok: maps[index]['neSnizhemiyOstatok'],
          postavshik: maps[index]['postavshik'],
          postavshikId: maps[index]['postavshikId'],
          shtrihkod: maps[index]['shtrihkod'],
          uchetPoSerNomeram: maps[index]['uchetPoSerNomeram'],
          vesTovar: maps[index]['vesTovar'],
          alhogolProduct: maps[index]['alhogolProduct'],
          aksizMark: maps[index]['aksizMark'],
          kodVidaProduct: maps[index]['kodVidaProduct'],
          emkost: maps[index]['emkost'],
          krepost: maps[index]['krepost'],
          upakovka: maps[index]['upakovka'],
          kolvoUpakovka: maps[index]['kolvoUpakovka'],
          vladelesSot: maps[index]['vladelesSot'],
          vladalesOtdel: maps[index]['vladalesOtdel'],
          discount: listOfDiscount
                  .every((element) => element.id != maps[index]['discount_id'])
              ? null
              : listOfDiscount.firstWhere(
                  (element) => element.id == maps[index]['discount_id']),
          quantity: maps[index]['quantity'],
          categoryId: maps[index]['category_id'].toString());
    });
  }

  static Future<List<Contragent>> getContragentDb() async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('supplier');

    return List.generate(maps.length, (index) {
      return Contragent(
          id: maps[index]['id'],
          archiv: maps[index]['archiv'],
          status: maps[index]['status'],
          naimenovanie: maps[index]['naimenovanie'],
          code: maps[index]['code'],
          email: maps[index]['email'],
          phoneNumber: maps[index]['phoneNumber'],
          factAdress: maps[index]['factAdress'],
          comment: maps[index]['comment'],
          typeContragent: maps[index]['typeContragent'],
          polNaimenovanie: maps[index]['polNaimenovanie'],
          yurAdress: maps[index]['yurAdress'],
          vladelesSot: maps[index]['vladelesSot'],
          vladalesOtdel: maps[index]['vladalesOtdel']);
    });
  }

  static Future<List<ContactPersonWidget>> getContactFace() async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('contact_face');

    return List.generate(maps.length, (index) {
      return ContactPersonWidget(
        id: maps[index]['id'],
        contragent_id: maps[index]['contragent_id'],
        nname: maps[index]['name'],
        ddolzhnost: maps[index]['dolzhnost'],
        ttelephone: maps[index]['phoneNumber'],
        eemail: maps[index]['email'],
        ccomment: maps[index]['comment'],
      );
    });
  }

  static Future<List<SupplierInvoiceDetails>>
      getSupplierInvoiceDetails() async {
    final db = await database;
    final List<Map<String, dynamic>> maps =
        await db.query("supplier_invoice_details");

    return List.generate(maps.length, (index) {
      return SupplierInvoiceDetails(
          id: maps[index]['id'],
          supplierInvoiceId: maps[index]['supplier_invoice_id'],
          img: File(maps[index]['img']),
          name: maps[index]['name'],
          productId: maps[index]['product_id'],
          quantity: maps[index]['quantity'],
          price: maps[index]['price'],
          barcode: maps[index]['barcode'],
          kolvoUpakovka: maps[index]['kolvoUpakovka'],
          returned: maps[index]['returned']);
    });
  }

  static Future<List<SupplierInvoice>> getSupplierInvoice() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('supplier_invoice');
    return List.generate(maps.length, (index) {
      return SupplierInvoice(
        id: maps[index]['id'],
        supplierId: maps[index]['supplier_id'],
        datatime: maps[index]['datatime'],
        total: maps[index]['total'],
        quantity: maps[index]['quantity'],
        returned: maps[index]['returned'],
        changeMoney: maps[index]['changeMoney'],
      );
    });
  }

  static Future<List<Discount>> getDiscount() async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('discount');

    return List.generate(maps.length, (index) {
      return Discount(
        id: maps[index]['id'],
        name: maps[index]['name'],
        value: maps[index]['value'],
        percentOrSumma: maps[index]['percentOrSumma'],
      );
    });
  }

  static Future<List<Client>> getClients() async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('clients');
    final List<Map<String, dynamic>> mapsForReceiptProduct =
        await db.query('product_receipt');
    final List<Map<String, dynamic>> mapsForReceipt =
    await db.query('receipt');

    var listOfProductReceipt =
        List.generate(mapsForReceiptProduct.length, (index) {
      return ReceiptProduct(
          id: mapsForReceiptProduct[index]['id'],
          receiptId: mapsForReceiptProduct[index]['receipt_id'],
          naimenovanie: mapsForReceiptProduct[index]['naimenovanie'],
          quantity: mapsForReceiptProduct[index]['quantity'],
          kolvoUpakovke: mapsForReceiptProduct[index]['kolvoUpakovke'],
          prodazhiPrice: mapsForReceiptProduct[index]['prodazhiPrice'],
          returned: mapsForReceiptProduct[index]['returned']);
    });

   var listOfReceipt = List.generate(mapsForReceipt.length, (index) {
      return Receipt(
          id: mapsForReceipt[index]['id'],
          total: mapsForReceipt[index]['total'],
          cashier: mapsForReceipt[index]['cashier'],
          kassa: mapsForReceipt[index]['kassa'],
          cash: mapsForReceipt[index]['cash'],
          dateTime: mapsForReceipt[index]['dateTime'],
          changeMoney: mapsForReceipt[index]['changeMoney'],
          clientId: mapsForReceipt[index]['client_id'],
          listReceiptProduct: listOfProductReceipt
              .where((element) => element.receiptId == mapsForReceipt[index]['id'])
              .toList());
    });
    return List.generate(maps.length, (index) {
      return Client(
        id: maps[index]['id'],
        name: maps[index]['name'],
        email: maps[index]['email'],
        phoneNumber: maps[index]['phoneNumber'],
        address: maps[index]['address'],
        city: maps[index]['city'],
        region: maps[index]['region'],
        mailIndex: maps[index]['mailIndex'],
        country: maps[index]['country'],
        clientCode: maps[index]['clientCode'],
        clientNote: maps[index]['clientNote'],
        lastVisit: maps[index]['lastVisit'],
        numberOfVisits: maps[index]['numberOfVisits'],
        receipt: listOfReceipt.where((element) => element.clientId == maps[index]['id']).toList()
      );
    });
  }
}
