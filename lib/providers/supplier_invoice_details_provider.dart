import 'package:flutter/material.dart';

import '../database/db/product_maker_db.dart';
import '../models/supplier_invoice_details_model.dart';

class SupplierInvoiceDetailsProvider extends ChangeNotifier {
  List<SupplierInvoiceDetails> _listSupplierInvoiceDetails = [];

  SupplierInvoiceDetailsProvider() {
    fetchData();
  }

  void returnedProductReceipt(List<SupplierInvoiceDetails> list) {
    for (int i = 0; i < list.length; i++) {
      _listSupplierInvoiceDetails
          .where((element) => element.id == list[i].id)
          .first
          .returned = 1;
    }
    notifyListeners();
  }

  void deleteSupplierInvoiceDetails(List<SupplierInvoiceDetails> list) {
    for (var all in list) {
      _listSupplierInvoiceDetails
          .removeWhere((element) => element.id == all.id);
    }
    notifyListeners();
  }

  void addToListOfSupplierInvoiceDetails(
      SupplierInvoiceDetails supplierInvoiceDetails) {
    _listSupplierInvoiceDetails.add(SupplierInvoiceDetails(
      id: supplierInvoiceDetails.id,
      supplierInvoiceId: supplierInvoiceDetails.supplierInvoiceId,
      img: supplierInvoiceDetails.img,
      name: supplierInvoiceDetails.name,
      productId: supplierInvoiceDetails.productId,
      quantity: supplierInvoiceDetails.quantity,
      price: supplierInvoiceDetails.price,
      barcode: supplierInvoiceDetails.barcode,
      kolvoUpakovka: supplierInvoiceDetails.kolvoUpakovka,
      returned: supplierInvoiceDetails.returned,
    ));

    for (int i = 0; i < _listSupplierInvoiceDetails.length; i++) {
      print("Hastay ${_listSupplierInvoiceDetails[i].supplierInvoiceId}");
    }

    notifyListeners();
  }

  void notify() {
    notifyListeners();
  }

  Future<void> fetchDataSupplierInvoiceDetails() async {
    final data = await ProductMakerDb.getSupplierInvoiceDetails();
    if (data.isNotEmpty) {
      _listSupplierInvoiceDetails = data.map((e) {
        return SupplierInvoiceDetails(
            id: e.id,
            supplierInvoiceId: e.supplierInvoiceId,
            img: e.img,
            name: e.name,
            productId: e.productId,
            quantity: e.quantity,
            price: e.price,
            barcode: e.barcode,
            kolvoUpakovka: e.kolvoUpakovka,
            returned: e.returned);
      }).toList();
    }
    notifyListeners();
  }

  void fetchData() async {
    await fetchDataSupplierInvoiceDetails();
  }

  List<SupplierInvoiceDetails> get listSupplierInvoiceDetails =>
      _listSupplierInvoiceDetails;
}
