import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/discount_maker_model.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/providers/discount_provider.dart';
import 'package:provider/provider.dart';

class ProductMakerProvider with ChangeNotifier {
  ProductMakerProvider() {
    fetch();
  }

  late List<ProductMaker> _productsmaker = [];

  List<ProductMaker> get productsmaker => _productsmaker;

  Future<void> fetch() async {
    await fetchAndSetData();
    _productsmaker;
    notifyListeners();
  }

  Future<List<ProductMaker>> fetchAndSetData() async {
    final data = await ProductMakerDb.getProduct();
    print(data.length);
    if (data.isNotEmpty) {
      _productsmaker = data.map((e) {
        return ProductMaker(
            id: e.id,
            image: e.image,
            archiv: e.archiv,
            naimenovanie: e.naimenovanie,
            groupe: e.groupe,
            code: e.code,
            artikul: e.artikul,
            minimalPrice: e.minimalPrice,
            minimalCurrency: e.minimalCurrency,
            zakupkiPrice: e.zakupkiPrice,
            zakupkiCurrency: e.zakupkiCurrency,
            prodazhiPrice: e.prodazhiPrice,
            prodazhiCurrency: e.prodazhiCurrency,
            opisanie: e.opisanie,
            strana: e.strana,
            ves: e.ves,
            obyom: e.obyom,
            edIzmereniy: e.edIzmereniy,
            nds: e.nds,
            neSnizhemiyOstatok: e.neSnizhemiyOstatok,
            postavshik: e.postavshik,
            postavshikId: e.postavshikId,
            shtrihkod: e.shtrihkod,
            uchetPoSerNomeram: e.uchetPoSerNomeram,
            vesTovar: e.vesTovar,
            alhogolProduct: e.alhogolProduct,
            aksizMark: e.aksizMark,
            kodVidaProduct: e.kodVidaProduct,
            emkost: e.emkost,
            krepost: e.krepost,
            upakovka: e.upakovka,
            kolvoUpakovka: e.kolvoUpakovka,
            vladelesSot: e.vladelesSot,
            vladalesOtdel: e.vladalesOtdel,
            discount: e.discount,
            quantity: e.quantity,
            categoryId: e.categoryId);
      }).toList();
    }
    return _productsmaker;
  }

  void set(ProductMaker productMaker) {
    _productsmaker.add(productMaker);
    notifyListeners();
  }

  bool checkProductMaker(ProductMaker productMaker) {
    return _productsmaker.contains(productMaker);
  }

  List<ProductMaker> getWhichisAdded() {
    var notArchive =
        _productsmaker.where((element) => element.archiv == 0).toList();
    return notArchive.where((element) => element.quantity > 0).toList();
  }

  double allQuantityProduct() {
    double getQuantity = 0;
    getWhichisAdded().forEach((element) {
      getQuantity += element.quantity.toInt();
    });
    return getQuantity;
  }

  void setZeroToOneProduct(ProductMaker productMaker) {
    getWhichisAdded()
        .where((element) => element.id == productMaker.id).forEach((element) {
          element.quantity = 0.0;
    });
    notifyListeners();
  }


  List<ProductMaker> getNotArchiveTovar() {
    return _productsmaker.where((element) => element.archiv == 0).toList();
  }

  List<ProductMaker> getArchiveTovar() {
    return _productsmaker.where((element) => element.archiv == 1).toList();
  }

  double allPrice() {
    double res = 0.0;
    getWhichisAdded().forEach((element) {
      res += element.getTotalOfProduct();
    });
    return res;
  }

  double totalOneProduct(ProductMaker productMaker)
  {
    double res = 0.0;
    res = productMaker.getTotalOfProduct();
    return res;
  }

  void notify() {
    _productsmaker;
    notifyListeners();
  }

  void setZeroCart() {
    _productsmaker.forEach((element) {
      element.quantity = 0;
      element.discount = null;
    });
    notifyListeners();
  }

  void deleteProduct(ProductMaker productMaker) {
    _productsmaker.removeWhere((element) => element == productMaker);
    notifyListeners();
  }
}
