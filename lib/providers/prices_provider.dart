import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class Prices with ChangeNotifier {
  List<String> _currency = ['РУБ', 'USD', 'TJS', 'EUR'];
  List<String> get currency => _currency;
  late TextEditingController _minimalCurrencyControllerTovar =
      TextEditingController(text: _currency[0]);

  late TextEditingController _senaZakupkiControllerTovar =
      TextEditingController(text: _currency[0]);

  late TextEditingController _senaProdazhiControllerTovar =
      TextEditingController(text: _currency[0]);

  void setMinimal(TextEditingController set) {
    _minimalCurrencyControllerTovar = set;
    // notifyListeners();
  }

  void setsenaZakupki(TextEditingController set) {
    _senaZakupkiControllerTovar = set;
    // notifyListeners();
  }

  void setSenaProdazhi(TextEditingController set) {
    _senaProdazhiControllerTovar = set;
    // notifyListeners();
  }

  void clearMinimal() {
    _minimalCurrencyControllerTovar = TextEditingController(text: '');
    notifyListeners();
  }

  void clearSenaZakupki() {
    _senaZakupkiControllerTovar = TextEditingController(text: '');
    notifyListeners();
  }

  void clearSenaProdazhi() {
    _senaProdazhiControllerTovar = TextEditingController(text: '');
    notifyListeners();
  }

  TextEditingController get minimalCurrencyControllerTovar =>
      _minimalCurrencyControllerTovar;
  TextEditingController get senaZakupkiControllerTovar =>
      _senaZakupkiControllerTovar;
  TextEditingController get senaProdazhiControllerTovar =>
      _senaProdazhiControllerTovar;
}
