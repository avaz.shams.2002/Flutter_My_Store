import 'package:flutter/cupertino.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/widgets/contact_person_widget.dart';

import '../models/contragent_maker_model.dart';

class ContactPersonProvider with ChangeNotifier {
  List<ContactPersonWidget> _listWidget = [];

  bool itemExists(ContactPersonWidget item) {
    return _listWidget.contains(item);
  }

  ContactPersonProvider() {
    fetch();
  }

  void addWidget(
      {int? id,
      int? contragent_id,
      String? fam,
      String? dol,
      String? tel,
      String? email,
      String? commemt}) {
    // var name = _listWidget.where((element) => element.name.text == fam?.text);
    var found = _listWidget.where((element) => element.id == id);
    if (found.isNotEmpty) {
      return;
    }
    _listWidget.add(ContactPersonWidget(
      id: id,
      contragent_id: contragent_id,
      nname: fam,
      ddolzhnost: dol,
      ttelephone: tel,
      eemail: email,
      ccomment: commemt,
    ));
  }

  void fetch() async {
    await fetchAndSetData();
    _listWidget;
    notifyListeners();
  }

  Future<void> fetchAndSetData() async {
    final data = await ProductMakerDb.getContactFace();
    print(data.length);
    if (data.isNotEmpty) {
      _listWidget = data.map((e) {
        return ContactPersonWidget(
          id: e.id,
          contragent_id: e.contragent_id,
          nname: e.nname,
          ddolzhnost: e.ddolzhnost,
          ttelephone: e.ttelephone,
          eemail: e.eemail,
          ccomment: e.ccomment,
        );
      }).toList();
    }
  }

  void notify() {
    notifyListeners();
  }

  void deleteWidget(ContactPersonWidget contactPersonWidget) {
    var found = _listWidget.where((element) => element == contactPersonWidget);
    if (found.isNotEmpty) {
      _listWidget.remove(found.first);
    }
    notifyListeners();
  }

  void deleteAllWithId(int id) {
    _listWidget.removeWhere((element) => element.contragent_id == id);
    notifyListeners();
  }

  void deleteOneItem(ContactPersonWidget contactPersonWidget) {
    _listWidget.remove(contactPersonWidget);
    notifyListeners();
  }

  List<ContactPersonWidget> getContactFace(int id) {
    print(id);
    return _listWidget.where((element) => element.contragent_id == id).toList();
  }

  // List<ContactFace> getContactNames() {
  //   List<ContactFace> contactFace = [];
  //   if (_listWidget.isNotEmpty) {
  //     for (int i = 0; i < _listWidget.length; i++) {
  //       contactFace.add(ContactFace(
  //           name: _listWidget[i].name.text,
  //           dolzhnost: _listWidget[i].dolzhnost.text,
  //           phoneNumber: _listWidget[i].telephone.text,
  //           email: _listWidget[i].email.text,
  //           comment: _listWidget[i].comment.text));
  //     }
  //   }
  //   return contactFace;
  // }

  void clear() {
    _listWidget.clear();
    notifyListeners();
  }

  List<ContactPersonWidget> get listWidget => _listWidget;
}
