import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:my_store/screens/type_code.dart';

class CountriesFromApiProvider with ChangeNotifier {
  final List<dynamic> _names = [];

  Future fetchCountries() async {
    try {
      final response =
          await http.get(Uri.parse('https://restcountries.com/v3.1/all'));
      if (response.statusCode == 200) {
        List<dynamic> map = json.decode(utf8.decode(response.bodyBytes));
        for (int i = 0; i < map.length; i++) {
          _names.add(map[i]['translations']['rus']['common']);
          print(map[i]['translations']['rus']['common']);
        }
        _names.sort();
      }
    } catch (e) {
      print(e);
    }
  }

  CountriesFromApiProvider() {
    fetch();
  }
  void fetch() async {
    await fetchCountries();
    notifyListeners();
  }

  List<dynamic> get names => _names;
}
