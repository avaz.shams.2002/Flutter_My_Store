import 'package:flutter/cupertino.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/category_maker_model.dart';

class CategoryProvider with ChangeNotifier {

  String _categoryId = '';
  List<Category> _categoryList = [];
  void addCategory(Category category) {
    _categoryList.add(category);
    notifyListeners();
  }

  CategoryProvider() {
    fetch();
  }

  void setCategoryId(String set){
    _categoryId= set;
    notifyListeners();
  }

  void notify() {
    notifyListeners();
  }

  void fetch() async {
    await fetchDb();
    _categoryList;
    notifyListeners();
  }

  Future<void> fetchDb() async {
    final data = await ProductMakerDb.getCategoryDb();
    if (data.isNotEmpty) {
      _categoryList = data.map((e) {
        return Category(
            id: e.id,
            name: e.name,
            picture: e.picture,
            createdAt: e.createdAt,
            updatedAt: e.updatedAt,
            visible: e.visible,
          colorInt: e.colorInt
        );
      }).toList();
    }
  }

  void deleteOneCategory(int id) {
    _categoryList.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  List<Category> getCategoryNotArchive() {
    return _categoryList.where((element) => element.visible == 0).toList();
  }

  List<Category> getCategoryArchive() {
    return _categoryList.where((element) => element.visible == 1).toList();
  }

  bool checkCategory(Category category)
  {
    return _categoryList.contains(category);
  }

  List<Category> get categoryList => _categoryList;
  String get categoryId => _categoryId;
}
