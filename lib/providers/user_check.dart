import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_store/pages/login_page.dart';
import 'package:my_store/providers/current_page_provider.dart';
import 'package:my_store/screens/product_screen.dart';
import 'package:provider/provider.dart';

class CheckUser with ChangeNotifier {
  User? _user = FirebaseAuth.instance.currentUser;
  TextEditingController _country = TextEditingController(text: '');

  String? get user => _user?.email;
  TextEditingController get country => _country;
  void setUser({User? user}) {
    _user = user;
    notifyListeners();
  }

  void setCountry(TextEditingController get) {
    _country = get;
    notifyListeners();
  }

  void signOut({context, currentPage}) async {
    FirebaseAuth.instance.currentUser;
    FirebaseAuth.instance.signOut();
    if (context != null) {
      if (currentPage == 2) {
        Navigator.popUntil(context, (route) => route.isFirst);
        return;
      }
      Provider.of<CurrentStateProvider>(context, listen: false)
          .setCurrentPage(2);
      Navigator.popUntil(context, (route) => route.isFirst);
      print("User:${_user?.email}");
    }
    print(FirebaseAuth.instance.currentUser?.email);
  }

  Stream<User?> getter() {
    FirebaseAuth.instance.currentUser;
    return FirebaseAuth.instance.authStateChanges();
  }
}
