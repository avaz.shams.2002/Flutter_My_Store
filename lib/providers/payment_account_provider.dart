import 'package:flutter/cupertino.dart';
import 'package:my_store/pages/payment_account_page.dart';

class PayMentAccountProvider with ChangeNotifier {
  final List<PayMentAccount> _listWidgets = [];

  bool itemExists(PayMentAccount payMentAccount) {
    return _listWidgets.contains(payMentAccount);
  }

  void addWidget(
      {bool? osnovnoySchet,
      String? nomer,
      String? bik,
      String? bank,
      String? address,
      String? korrSchet}) {
    _listWidgets.add(PayMentAccount(
      osnovnoySchet: osnovnoySchet,
      nomerRsScheta: nomer,
      bik: bik,
      bank: bank,
      address: address,
      korrSchet: korrSchet,
    ));
  }

  void notify() {
    notifyListeners();
  }

  void deleteWidget(PayMentAccount payMentAccount) {
    var found = _listWidgets.where((element) => element == payMentAccount);
    if (found.isNotEmpty) {
      _listWidgets.remove(found.first);
    }
    notifyListeners();
  }

  void clear() {
    _listWidgets.clear();
    notifyListeners();
  }

  List<PayMentAccount> get listWidgets => _listWidgets;
}
