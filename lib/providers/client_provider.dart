import 'package:flutter/cupertino.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/client_model.dart';

class ClientProvider extends ChangeNotifier {
  List<Client> _listOfClient = [];
  List<Client> _addedClient = [];

  void addClient(Client client) {
    _listOfClient.add(client);
    notifyListeners();
  }

  ClientProvider() {
    fetchAll();
  }

  void addedClientFunc(Client client) {
    _addedClient.add(client);
    notifyListeners();
  }

  void deleteaddedClient(Client client)
  {
    _addedClient.removeWhere((element) => element.id == client.id);
    notifyListeners();
  }

  void clearList()
  {
    _addedClient.clear();
    notifyListeners();
  }

  bool checkClientInList(Client client) {
    return _listOfClient.contains(client);
  }

  void notify() {
    notifyListeners();
  }

  Future<void> fetchClient() async {
    final data = await ProductMakerDb.getClients();
    if (data.isNotEmpty) {
      _listOfClient = data.map((e) {
        return Client(
            id: e.id,
            name: e.name,
            email: e.email,
            phoneNumber: e.phoneNumber,
            address: e.address,
            city: e.city,
            region: e.region,
            mailIndex: e.mailIndex,
            country: e.country,
            clientCode: e.clientCode,
            clientNote: e.clientNote,
            lastVisit: e.lastVisit,
            numberOfVisits: e.numberOfVisits,
            receipt: e.receipt
        );
      }).toList();
    }
    notifyListeners();
  }

  void fetchAll() async {
    await fetchClient();
  }

  List<Client> get listOfClient => _listOfClient;

  List<Client> get addedClient => _addedClient;
}
