import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_store/widgets/barcode_widget.dart';

import '../screens/type_code.dart';

class Barcode with ChangeNotifier {
  Barcode() {
    // _controller;
    notifyListeners();
  }
  final List<BarcodeWidget> _widgets = [];
  List<BarcodeWidget> get widgets => _widgets;

  void addWidget({String? code}) {
    _widgets.add(BarcodeWidget(
      code: code,
    ));
    notifyListeners();
  }

  void clearWidget() {
    _widgets.clear();
    notifyListeners();
  }

  List<String> getCode() {
    List<String> list = [];
    for (int i = 0; i < _widgets.length; i++) {
      list.add(_widgets[i].code!);
    }
    return list;
  }

  void clearSelectedWidget(BarcodeWidget barcode) {
    var found = _widgets.where((element) => element == barcode);
    if (found.isNotEmpty) {
      _widgets.remove(found.first);
    }
    notifyListeners();
  }

  void setController(TextEditingController set) {
    //   _controller = set;
    //   addWidget(controllerType: _controller);
    //   print(_controller.text);
    //   notifyListeners();
    // }

    void setCodeController(TextEditingController set) {
      // _controllerCode = set;
      // addWidget(controllerCode: _controllerCode);
      // notifyListeners();
    }
  }
}
