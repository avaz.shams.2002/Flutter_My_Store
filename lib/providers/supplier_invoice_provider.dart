import 'package:flutter/cupertino.dart';

import '../database/db/product_maker_db.dart';
import '../models/supplier_invoice_details_model.dart';
import '../models/supplier_invoice_model.dart';

class SupplierAllProvider extends ChangeNotifier {
  SupplierAllProvider() {
    fetchData();
  }

  List<SupplierInvoice> _listOfSupplierInvoice = [];

  void addToListOfSupplierInvoice(SupplierInvoice supplierInvoice) {
    _listOfSupplierInvoice.add(supplierInvoice);
  }

  Future<void> fetchDataSupplierInvoice() async {
    final data = await ProductMakerDb.getSupplierInvoice();
    print(data.length);
    if (data.isNotEmpty) {
      _listOfSupplierInvoice = data.map((e) {
        return SupplierInvoice(
            id: e.id,
            supplierId: e.supplierId,
            datatime: e.datatime,
            total: e.total,
            quantity: e.quantity,
            returned: e.returned,
            changeMoney: e.changeMoney,);
      }).toList();
    }
  }

  void fetchData() async {
    await fetchDataSupplierInvoice();
    notifyListeners();
  }

  List<SupplierInvoice> listOfReturned() {
    return _listOfSupplierInvoice
        .where((element) => element.returned == 1)
        .toList();
  }

  void deleteSupplierInvoice(int id)
  {
    _listOfSupplierInvoice.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  void updateSupplier(int id, double total) {
    _listOfSupplierInvoice.where((element) => element.id == id).first.total =
        total;
    notifyListeners();
  }

  double getTotal(int id) {
    double total = 0.0;
    _listOfSupplierInvoice
        .where((element) => element.supplierId == id)
        .where((element) => element.returned == 0)
        .forEach((element) {
      total += element.total!;
    });
    return total;
  }

  List<SupplierInvoice> get listOfSupplierInvoice => _listOfSupplierInvoice;
}
