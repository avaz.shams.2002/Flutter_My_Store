import 'package:flutter/cupertino.dart';

class CurrentStateProvider extends ChangeNotifier {
  int _currentPage = 2;

  int get currentPage => _currentPage;

  void setCurrentPage(int get) {
    _currentPage = get;
    notifyListeners();
  }
}
