import 'package:flutter/cupertino.dart';
import 'package:my_store/models/product_make_model.dart';

class ProductCartProvider with ChangeNotifier {
  final List<ProductMaker> _cartList = [];
  void addToCart(ProductMaker productMaker) {
    var found = _cartList.where((element) => element.id == productMaker.id);
    if (found.isNotEmpty) {
      return;
    }

    _cartList.add(productMaker);
    notifyListeners();
  }

  List<ProductMaker> get cartList => _cartList;
}
