import 'package:flutter/cupertino.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/discount_maker_model.dart';

class DiscountProvier with ChangeNotifier {
  DiscountProvier() {
    fetchDb();
  }

  List<Discount> _listDiscount = [];

  void addDiscount(Discount discount) {
    _listDiscount.add(discount);
    notifyListeners();
  }


  Future<void> fetchDiscountFromDb() async {
    final data = await ProductMakerDb.getDiscount();
    if (data.isNotEmpty) {
      _listDiscount = data.map((e) {
        return Discount(
            id: e.id,
            name: e.name,
            value: e.value,
            percentOrSumma: e.percentOrSumma);
      }).toList();
    }
    notifyListeners();
  }


  void fetchDb() async {
    await fetchDiscountFromDb();
    notifyListeners();
  }

  void notify() {
    notifyListeners();
  }

  List<Discount> get listDiscount => _listDiscount;

}
