import 'package:flutter/cupertino.dart';
import 'package:my_store/widgets/shtrihcode_widget.dart';

class ShtrihCodePRovider with ChangeNotifier {
  final List<ShtrihCodeWidget> _shtrihWidget = [];

  void addShtrih() {
    if (_shtrihWidget.isEmpty) {
      _shtrihWidget.add(ShtrihCodeWidget());
    } else {
      return;
    }
    notifyListeners();
  }

  String getCode() {
    String get = '';
    if (_shtrihWidget.isNotEmpty) {
      get = _shtrihWidget.first.typeController.text;
    }
    return get;
  }

  void clear() {
    _shtrihWidget.clear();
    notifyListeners();
  }

  List<ShtrihCodeWidget> get shtrihWidget => _shtrihWidget;
}
