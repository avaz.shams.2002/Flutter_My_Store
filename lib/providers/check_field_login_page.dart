import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class CheckFieldLoginPage extends ChangeNotifier {
  bool _checkInternet = true;
  String _checkLogin = '_';
  String _checkPassword = '_';
  String get checkLogin => _checkLogin;
  String get checkPassword => _checkPassword;
  bool get checkInternet => _checkInternet;
  void checkLoginFunction(String checker) {
    _checkLogin = checker;
    notifyListeners();
  }

  void checkPasswordFunction(String checker) {
    _checkPassword = checker;
    notifyListeners();
  }

  void checkInet(bool get) {
    _checkInternet = get;
    notifyListeners();
  }
}
