import 'package:flutter/cupertino.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/contragent_maker_model.dart';

class ContragentProvider with ChangeNotifier {
  late List<Contragent> _list = [];

  void addContragent(Contragent contragent) {
    _list.add(contragent);
    notifyListeners();
  }

  ContragentProvider() {
    fetch();
  }

  bool checkContragent(Contragent contragent) {
    return _list.contains(contragent);
  }

  void addContragentArchiv() {}

  void notify() {
    notifyListeners();
  }

  void fetch() async {
    await fetchAndSetData();
    _list;
    notifyListeners();
  }

  Future<List<Contragent>> fetchAndSetData() async {
    final data = await ProductMakerDb.getContragentDb();
    print(data.length);
    if (data.isNotEmpty) {
      _list = data.map((e) {
        return Contragent(
            id: e.id,
            archiv: e.archiv,
            status: e.status,
            naimenovanie: e.naimenovanie,
            code: e.code,
            email: e.email,
            phoneNumber: e.phoneNumber,
            factAdress: e.factAdress,
            comment: e.comment,
            typeContragent: e.typeContragent,
            polNaimenovanie: e.polNaimenovanie,
            yurAdress: e.yurAdress,
            vladelesSot: e.vladelesSot,
            vladalesOtdel: e.vladalesOtdel);
      }).toList();
    }
    return _list;
  }

  List<Contragent> listOfArchiveContragent() {
    return _list.where((element) => element.archiv == 1).toList();
  }

  List<Contragent> listOfNotArchiveContragent() {
    return _list.where((element) => element.archiv == 0).toList();
  }

  void removeWithId(int id) {
    _list.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  List<Contragent> get list => _list;
}
