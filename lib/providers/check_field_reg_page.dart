import 'package:flutter/cupertino.dart';

class CheckFieldRegPage with ChangeNotifier {
  String _checkReg = '_';
  String _checkPassword = '_';
  String get checkLogin => _checkReg;
  String get checkPassword => _checkPassword;
  void checkRegFunction(String checker) {
    _checkReg = checker;
    notifyListeners();
  }

  void checkPasswordFunction(String checker) {
    _checkPassword = checker;
    notifyListeners();
  }
}
