import 'package:flutter/material.dart';
import 'package:my_store/models/receipt_product_model.dart';

import '../database/db/product_maker_db.dart';
import '../models/product_make_model.dart';
import '../models/receipt_model.dart';

class ReceipAndTovarProvider extends ChangeNotifier {
  List<Receipt> _listReceipt = [];

  List<ReceiptProduct> _listProductMaker = [];

  ReceipAndTovarProvider() {
    fetchAll();
  }

  void addReceipt(Receipt receipt) {
    _listReceipt.add(receipt);
    notifyListeners();
  }

  void addReceiptProduct(List<ReceiptProduct> list) {
    for (int i = 0; i < list.length; i++) {
      _listProductMaker.add(list[i]);
    }
    notifyListeners();
  }


  Future<void> fetchReceiptProductId() async {
    final data = await ProductMakerDb.getReceiptProductId();
    print(data.length);
    if (data.isNotEmpty) {
      _listProductMaker = data.map((e) {
        return ReceiptProduct(
          id: e.id,
            receiptId: e.receiptId,
            naimenovanie: e.naimenovanie,
            quantity: e.quantity,
            kolvoUpakovke: e.kolvoUpakovke,
            prodazhiPrice: e.prodazhiPrice,
            returned: e.returned
        );
      }).toList();
    }
  }

  Future<void> fetchReceipt() async {
    final data = await ProductMakerDb.getReceipt();
    print(data.length);
    if (data.isNotEmpty) {
      _listReceipt = data.map((e) {
        return Receipt(
            id: e.id,
            total: e.total,
            cashier: e.cashier,
            kassa: e.kassa,
            cash: e.cash,
            dateTime: e.dateTime,
            changeMoney: e.changeMoney,
            client: e.client,
          listReceiptProduct: e.listReceiptProduct
        );
      }).toList();
    }
  }

  void fetchAll() async {
    await fetchReceipt();
    await fetchReceiptProductId();
    _listReceipt;
    _listProductMaker;
    notifyListeners();
  }

  void notify()
  {
    notifyListeners();
  }

  void returnedProductReceipt(List<ReceiptProduct> list)
  {
    for(int i = 0 ; i < list.length; i++)
      {
        _listProductMaker.where((element) => element.id == list[i].id).first.returned = 1;
      }
    notifyListeners();
  }

  void updateReceipt(int id, double total)
  {
    _listReceipt.where((element) => element.id == id).first.total = total.toString();
    notifyListeners();
  }

  void deleteReceipt(int id)
  {
    _listReceipt.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  void deleteReceiptProduct(List<ReceiptProduct> list)
  {
    for(var all in list)
      {
        _listProductMaker.removeWhere((element) => element.id == all.id);
      }
    notifyListeners();
  }

  double getAllPrice()
  {
    double getTotal = 0.0;
    _listReceipt.forEach((element) {
      getTotal += double.parse(element.total!);
    });
    return getTotal;
  }

  List<Receipt> get listReceipt => _listReceipt;

  List<ReceiptProduct> get listProductMaker => _listProductMaker;
}
