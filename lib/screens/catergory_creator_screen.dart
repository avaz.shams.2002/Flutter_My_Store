import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/pages/create_product_page.dart';
import 'package:my_store/providers/category_provider.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:provider/provider.dart';

import '../models/category_maker_model.dart';

class CategoryCreatorScreen extends StatefulWidget {
  bool? fromTovarPage;
  TextEditingController? groupController;
  Map<String, int>? dropDownButtonList;
  Category? category;

  CategoryCreatorScreen(
      {Key? key,
      this.groupController,
      this.dropDownButtonList,
      this.category,
      this.fromTovarPage})
      : super(key: key);

  @override
  State<CategoryCreatorScreen> createState() => _CategoryCreatorScreenState();
}

class _CategoryCreatorScreenState extends State<CategoryCreatorScreen> {
  late int colorContainer = 1;
  int colorCon = 0xFFE0E0E0;
  final _formKeyForCategory = GlobalKey<FormState>();
  late TextEditingController _nameOfCategory = TextEditingController(text: '');
  bool _visibleStatus = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.category != null) {
      _nameOfCategory = TextEditingController(text: widget.category!.name);

      colorContainer = int.parse(widget.category!.picture!);

      _visibleStatus = widget.category!.visible == 0 ? false : true;

      colorCon = widget.category!.colorInt!;
    }
  }

  @override
  Widget build(BuildContext context) {
    var productMakerProvider = Provider.of<ProductMakerProvider>(context);
    var categoryProvider = Provider.of<CategoryProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        actions: [
          TextButton(
              onPressed: () {},
              child: const Text(
                "Check",
                style: TextStyle(color: Colors.white70),
              )),
          TextButton(
              onPressed: () async {
                FocusManager.instance.primaryFocus!.unfocus();
                if (!_formKeyForCategory.currentState!.validate()) {
                  return;
                }
                DateTime today = new DateTime.now();

                widget.category ??= Category(
                  name: _nameOfCategory.text,
                  picture: colorContainer.toString(),
                  colorInt: colorCon,
                  createdAt: today.toString().substring(0, 19),
                  updatedAt: today.toString().substring(0, 19),
                  visible: _visibleStatus == false ? 0 : 1,
                );

                if (!categoryProvider.checkCategory(widget.category!)) {
                  await ProductMakerDb.insertCategory(widget.category!);
                  print("Category_id: ${widget.category!.id}");
                  categoryProvider.addCategory(widget.category!);
                  if (widget.groupController != null) {
                    if (_visibleStatus == false) {
                      widget.groupController!.text = _nameOfCategory.text;
                      widget.dropDownButtonList![_nameOfCategory.text] =
                          widget.category!.id!;

                      categoryProvider
                          .setCategoryId(widget.category!.id.toString());
                      productMakerProvider.notify();
                    }
                  }
                } else {
                  widget.category!.visible = _visibleStatus == false ? 0 : 1;
                  widget.category!.name = _nameOfCategory.text;
                  widget.category!.picture = colorContainer.toString();
                  widget.category!.updatedAt =
                      today.toString().substring(0, 19);
                  widget.category!.colorInt = colorCon;
                  categoryProvider.notify();
                  await ProductMakerDb.updateCategory(widget.category!);
                }

                print(_nameOfCategory.text);
                Navigator.of(context).pop(true);
              },
              child: const Text(
                "Сохранить",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ))
        ],
      ),
      body: Form(
        key: _formKeyForCategory,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              if (widget.fromTovarPage == false)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Архивный"),
                    FlutterSwitch(
                        width: 50,
                        height: 25,
                        value: _visibleStatus,
                        onToggle: (val) {
                          setState(() {
                            _visibleStatus = val;
                          });
                        })
                  ],
                ),
              TextFormField(
                decoration: const InputDecoration(
                    labelText: "Название категории",
                    labelStyle: TextStyle(color: Colors.grey)),
                controller: _nameOfCategory,
                validator: (val) {
                  if (val!.isEmpty) {
                    return "Поле не может быть пустым";
                  }
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  Text(
                    "Цвет категории",
                    style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              GridView.count(
                shrinkWrap: true,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 4,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFFE0E0E0;
                        colorContainer = 1;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFFE0E0E0),
                        ),
                        Center(
                            child: colorContainer == 1
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xfff44336;
                        colorContainer = 2;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xfff44336),
                        ),
                        Center(
                            child: colorContainer == 2
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFFF06292;
                        colorContainer = 3;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFFF06292),
                        ),
                        Center(
                            child: colorContainer == 3
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFFFFB74D;
                        colorContainer = 4;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFFFFB74D),
                        ),
                        Center(
                            child: colorContainer == 4
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFF69F0AE;
                        colorContainer = 5;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFF69F0AE),
                        ),
                        Center(
                            child: colorContainer == 5
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFF4CAF50;
                        colorContainer = 6;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFF4CAF50),
                        ),
                        Center(
                            child: colorContainer == 6
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFF2979FF;
                        colorContainer = 7;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFF2979FF),
                        ),
                        Center(
                            child: colorContainer == 7
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        colorCon = 0xFF9C27B0;
                        colorContainer = 8;
                      });
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 100,
                          color: const Color(0xFF9C27B0),
                        ),
                        Center(
                            child: colorContainer == 8
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 30,
                                  )
                                : const Text(""))
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
