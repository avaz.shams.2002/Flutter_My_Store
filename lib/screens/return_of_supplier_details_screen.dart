import 'package:flutter/material.dart';
import 'package:my_store/models/contragent_maker_model.dart';
import 'package:my_store/models/supplier_invoice_details_model.dart';
import 'package:my_store/models/supplier_invoice_model.dart';
import 'package:my_store/providers/contragent_provider.dart';
import 'package:my_store/providers/supplier_invoice_details_provider.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';
import '../providers/supplier_invoice_provider.dart';

class ReturnOfSupplierDetailsScreen extends StatefulWidget {
  SupplierInvoice? supplierInvoice;
  List<SupplierInvoiceDetails>? list;
  final dynamic setSt;
  final dynamic refreshParent;

  ReturnOfSupplierDetailsScreen(
      {Key? key,
      this.supplierInvoice,
      this.list,
      this.setSt,
      this.refreshParent})
      : super(key: key);

  @override
  State<ReturnOfSupplierDetailsScreen> createState() =>
      _ReturnOfSupplierDetailsScreenState();
}

class _ReturnOfSupplierDetailsScreenState
    extends State<ReturnOfSupplierDetailsScreen> {
  double total = 0.0;
  double changeMoney = 0.0;
  List<SupplierInvoiceDetails> addedReceiptProductChecked = [];

  List<double> _listOfQuantity = [];
  List<int> _kolUpakovka = [];
  List<int> _kolShtuk = [];
  List<bool> _isChecked = [];
  int totalItem = 0;
  int kolvoUpakovka = 0;
  double shtukRes = 0.0;
  int kolvoShtuk = 0;
  List<TextEditingController> _listOfUpakovkaControllers = [];
  List<TextEditingController> _listOfShtukControllers = [];
  var rounded = 0.0;
  var sumOfQuantityofShtuk = 0.0;

  @override
  void initState() {
    _isChecked = List<bool>.filled(widget.list!.length, false);
    for (int i = 0;
        i < widget.list!.where((element) => element.returned == 0).length;
        i++) {
      _listOfUpakovkaControllers.add(TextEditingController(text: '0'));
      _listOfShtukControllers.add(TextEditingController(text: '0'));
      rounded = ((widget.list![i].quantity!));
      //
      sumOfQuantityofShtuk =
          (((rounded - rounded.toInt()) * 100).round() / 100);
      //
      sumOfQuantityofShtuk =
          (sumOfQuantityofShtuk * widget.list![i].kolvoUpakovka!)
              .roundToDouble();
      //
      var totals = (widget.list![i].price! * rounded.toInt()) +
          ((sumOfQuantityofShtuk * widget.list![i].price!) /
              widget.list![i].kolvoUpakovka!);
      //
      _kolUpakovka.add(rounded.toInt());
      //
      _kolShtuk.add(sumOfQuantityofShtuk.toInt());
    }
    for (int i = 0; i < widget.list!.length; i++) {
      _listOfQuantity.add(0.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    var supplierInvoiceDetailsProvider =
        Provider.of<SupplierInvoiceDetailsProvider>(context);

    var supplierProvider = Provider.of<SupplierAllProvider>(context);
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
              onPressed: () {
                for (int i = 0; i < _listOfQuantity.length; i++) {
                  print("List if quantity: ${_listOfQuantity[i]}[${i + 1}]");
                }
                print("");
                for (int i = 0; i < widget.list!.length; i++) {
                  print(
                      "Result : ${widget.list![i].quantity! - _listOfQuantity[i]}");
                }
                print(
                    "Length of checked : ${addedReceiptProductChecked.length}");
              },
              child: const Text(
                "Check",
                style: TextStyle(color: Colors.white),
              ))
        ],
        backgroundColor: Colors.blueAccent,
        title: const Text(
          "Возврат",
          style: TextStyle(color: Colors.white),
        ),
        elevation: 0,
      ),
      body: GestureDetector(
        onTap: () {
          setState(() {
            FocusManager.instance.primaryFocus!.unfocus();
            for (var all in _listOfUpakovkaControllers) {
              if (all.text.isEmpty) {
                all.text = '0';
              }
            }
            for (var all in _listOfShtukControllers) {
              if (all.text.isEmpty) {
                all.text = '0';
              }
            }
          });
        },
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: widget.list!.length,
                  itemBuilder: (context, index) {
                    var rounded = ((widget.list![index].quantity!));
                    //
                    var sumOfQuantityofShtuk =
                        (((rounded - rounded.toInt()) * 100).round() / 100);
                    //
                    sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                            widget.list![index].kolvoUpakovka!)
                        .roundToDouble();
                    //

                    widget.list![index].quantity =
                        (widget.list![index].quantity! * 100).round() / 100;

                    print("each prod : ${widget.list![index].quantity!}");
                    //
                    //
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Checkbox(
                                value: _isChecked[index],
                                onChanged: (bool? value) {
                                  setState(() {
                                    _isChecked[index] = value!;
                                  });

                                  if (_isChecked[index] == true) {
                                    addedReceiptProductChecked
                                        .add(widget.list![index]);
                                    _listOfUpakovkaControllers[index].text =
                                        _kolUpakovka[index].toString();
                                    //
                                    _listOfShtukControllers[index].text =
                                        _kolShtuk[index].toString();

                                    //
                                    //
                                  } else {
                                    //
                                    addedReceiptProductChecked.removeWhere(
                                        (element) =>
                                            element == widget.list![index]);
                                    //
                                    _listOfUpakovkaControllers[index].text =
                                        '0';
                                    _listOfShtukControllers[index].text = '0';
                                    //
                                    return;
                                  }
                                },
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "${widget.list![index].name}",
                                      style:
                                          const TextStyle(color: Colors.black),
                                    ),
                                    Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.end,
                                      children: [
                                        Text(
                                          "(${widget.list![index].quantity!.toInt().toString()}-уп",
                                          style: const TextStyle(
                                              color: Colors.grey, fontSize: 10),
                                        ),
                                        if (sumOfQuantityofShtuk != 0)
                                          Text(
                                            ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                            style: const TextStyle(
                                                color: Colors.grey,
                                                fontSize: 10),
                                          ),
                                        const Text(
                                          ")",
                                          style: TextStyle(
                                              color: Colors.grey, fontSize: 10),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "В упаковке: ${widget.list![index].kolvoUpakovka}",
                                      style:
                                          const TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      "Цена(уп): ${widget.list![index].price}",
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 200,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 60,
                                child: TextField(
                                  onTap: () {
                                    setState(() {
                                      if (_listOfUpakovkaControllers[index]
                                          .text
                                          .isEmpty) {
                                        _isChecked[index] = false;
                                        _listOfQuantity[index] = 0;
                                        if (addedReceiptProductChecked
                                            .contains(widget.list![index])) {
                                          addedReceiptProductChecked
                                              .removeWhere((element) =>
                                                  element ==
                                                  widget.list![index]);
                                        }

                                        return;
                                      }
                                      _listOfUpakovkaControllers[index].text =
                                          '';
                                      _isChecked[index] = false;
                                      if (addedReceiptProductChecked
                                          .contains(widget.list![index])) {
                                        addedReceiptProductChecked.removeWhere(
                                            (element) =>
                                                element == widget.list![index]);
                                      }
                                    });
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      if (val.isEmpty) {
                                        _isChecked[index] = false;
                                        _listOfQuantity[index] = 0;

                                        if (addedReceiptProductChecked
                                            .contains(widget.list![index])) {
                                          addedReceiptProductChecked
                                              .removeWhere((element) =>
                                                  element ==
                                                  widget.list![index]);
                                        }
                                        return;
                                      }
                                      //
                                      var checkQuantity = int.parse(
                                              _listOfUpakovkaControllers[index]
                                                  .text) +
                                          int.parse(
                                                  _listOfShtukControllers[index]
                                                      .text) /
                                              widget
                                                  .list![index].kolvoUpakovka!;
                                      //
                                      checkQuantity =
                                          (checkQuantity * 100).round() / 100;
                                      print("checkQuantity: ${checkQuantity}");
                                      print(widget.list![index].quantity!);
                                      //
                                      if (checkQuantity >
                                          widget.list![index].quantity!) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(const SnackBar(
                                          content: Text("Error"),
                                          duration:
                                              const Duration(milliseconds: 700),
                                        ));
                                        _listOfUpakovkaControllers[index].text =
                                            '0';
                                        FocusManager.instance.primaryFocus!
                                            .unfocus();
                                        return;
                                      }
                                      //
                                      print(widget.list![index].quantity);
                                      //
                                      if (checkQuantity ==
                                          widget.list![index].quantity) {
                                        _isChecked[index] = true;
                                        if (!addedReceiptProductChecked
                                            .contains(widget.list![index])) {
                                          addedReceiptProductChecked
                                              .add(widget.list![index]);
                                        }
                                      }
                                      //
                                      if (checkQuantity <
                                          widget.list![index].quantity!) {
                                        if (addedReceiptProductChecked
                                            .contains(widget.list![index])) {
                                          addedReceiptProductChecked
                                              .removeWhere((element) =>
                                                  element ==
                                                  widget.list![index]);
                                        }
                                        _listOfQuantity[index] = checkQuantity;
                                        _isChecked[index] = false;
                                      }
                                      //
                                      print("CheckQuantity :  $checkQuantity");
                                      //
                                      //
                                    });
                                  },
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          left: 10.0, right: 10, top: 15)),
                                  controller: _listOfUpakovkaControllers[index],
                                ),
                              ),
                              const Text(
                                "уп",
                                style: TextStyle(color: Colors.grey),
                              ),
                              if (widget.list![index].kolvoUpakovka! <= 1)
                                const SizedBox(
                                  width: 75,
                                ),
                              if (widget.list![index].kolvoUpakovka! > 1)
                                SizedBox(
                                  width: 60,
                                  child: TextField(
                                    onTap: () {
                                      setState(() {
                                        if (_listOfShtukControllers[index]
                                            .text
                                            .isEmpty) {
                                          _isChecked[index] = false;
                                          _listOfQuantity[index] = 0;
                                          if (addedReceiptProductChecked
                                              .contains(widget.list![index])) {
                                            addedReceiptProductChecked
                                                .removeWhere((element) =>
                                                    element ==
                                                    widget.list![index]);
                                          }
                                          return;
                                        }
                                        _listOfShtukControllers[index].text =
                                            '';
                                        _isChecked[index] = false;
                                        if (addedReceiptProductChecked
                                            .contains(widget.list![index])) {
                                          addedReceiptProductChecked
                                              .removeWhere((element) =>
                                                  element ==
                                                  widget.list![index]);
                                        }
                                      });
                                    },
                                    onChanged: (val) {
                                      setState(() {
                                        print(val);
                                        if (val.isEmpty) {
                                          _isChecked[index] = false;
                                          _listOfQuantity[index] = 0;

                                          if (addedReceiptProductChecked
                                              .contains(widget.list![index])) {
                                            addedReceiptProductChecked
                                                .removeWhere((element) =>
                                                    element ==
                                                    widget.list![index]);
                                          }
                                          return;
                                        }
                                        //
                                        var checkQuantity = int.parse(
                                                _listOfUpakovkaControllers[
                                                        index]
                                                    .text) +
                                            int.parse(_listOfShtukControllers[
                                                        index]
                                                    .text) /
                                                widget.list![index]
                                                    .kolvoUpakovka!;
                                        //
                                        checkQuantity =
                                            (checkQuantity * 100).round() / 100;
                                        print(
                                            "checkQuantity: ${checkQuantity}");
                                        print(widget.list![index].quantity!);
                                        //
                                        if (checkQuantity >
                                            widget.list![index].quantity!) {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(const SnackBar(
                                            content: Text("Error"),
                                            duration:
                                                Duration(milliseconds: 700),
                                          ));
                                          _listOfShtukControllers[index].text =
                                              '0';
                                          if (addedReceiptProductChecked
                                              .contains(widget.list![index])) {
                                            addedReceiptProductChecked
                                                .removeWhere((element) =>
                                                    element ==
                                                    widget.list![index]);
                                          }
                                          _isChecked[index] = false;
                                          FocusManager.instance.primaryFocus!
                                              .unfocus();
                                          return;
                                        }
                                        //
                                        if (checkQuantity ==
                                            widget.list![index].quantity) {
                                          _isChecked[index] = true;
                                          if (!addedReceiptProductChecked
                                              .contains(widget.list![index])) {
                                            addedReceiptProductChecked
                                                .add(widget.list![index]);
                                          }
                                        }
                                        if (checkQuantity <
                                            widget.list![index].quantity!) {
                                          if (addedReceiptProductChecked
                                              .contains(widget.list![index])) {
                                            addedReceiptProductChecked
                                                .removeWhere((element) =>
                                                    element ==
                                                    widget.list![index]);
                                          }
                                          _listOfQuantity[index] =
                                              checkQuantity;
                                          _isChecked[index] = false;
                                        }
                                        print(
                                            "CheckQuantity :  $checkQuantity");
                                      });
                                    },
                                    keyboardType: TextInputType.number,
                                    decoration: const InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                            left: 10.0, right: 10, top: 15)),
                                    controller: _listOfShtukControllers[index],
                                  ),
                                ),
                              if (widget.list![index].kolvoUpakovka! > 1)
                                const Text(
                                  "шт",
                                  style: TextStyle(color: Colors.grey),
                                ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }),
            ),
            InkWell(
              onTap: () async {
                setState(() {});
                FocusManager.instance.primaryFocus!.unfocus();
                for (var all in _listOfShtukControllers) {
                  if (all.text.isEmpty) {
                    all.text = '0';
                  }
                }
                for (var all in _listOfUpakovkaControllers) {
                  if (all.text.isEmpty) {
                    all.text = '0';
                  }
                }
                if (addedReceiptProductChecked.isEmpty &&
                    _listOfQuantity.every((element) => element == 0.0)) {
                  return;
                }

                if (addedReceiptProductChecked.isNotEmpty) {
                  await ProductMakerDb.setReturnSupplierInvoiceDetails(
                      addedReceiptProductChecked);
                  widget.list!
                      .where((element) =>
                          addedReceiptProductChecked.contains(element))
                      .forEach((element) {
                    element.returned = 1;
                    element.quantity = 0.0;
                  });
                  if (widget.list!.every((element) => element.returned == 1)) {
                    await ProductMakerDb.setReturnToSupplierInvoice(
                        widget.supplierInvoice!);
                  }
                }

                for (int i = 0; i < widget.list!.length; i++) {
                  if (widget.list![i].returned == 0) {
                    widget.list![i].quantity = double.parse(
                        (widget.list![i].quantity! - _listOfQuantity[i])
                            .toStringAsFixed(2));

                    var rounded = ((widget.list![i].quantity!));

                    var sumOfQuantityofShtuk =
                        (((rounded - rounded.toInt()) * 100).round() / 100);

                    sumOfQuantityofShtuk =
                        (sumOfQuantityofShtuk * widget.list![i].kolvoUpakovka!)
                            .roundToDouble();
                    total += (widget.list![i].price! * rounded.toInt()) +
                        ((sumOfQuantityofShtuk * widget.list![i].price!) /
                            widget.list![i].kolvoUpakovka!);

                    await ProductMakerDb.justUpdateSupplierInvoiceDetail(
                        widget.list![i]);
                  }
                }

                supplierInvoiceDetailsProvider
                    .returnedProductReceipt(addedReceiptProductChecked);

                print("TOTAL : ${total}");

                print("Total : $total");
                // changeMoney = double.parse(widget.receipt!.cash!) - total;
                await ProductMakerDb.updateSupplierInvoice(
                    widget.supplierInvoice!.id!, total);
                supplierProvider.updateSupplier(
                    widget.supplierInvoice!.id!, total);
                supplierInvoiceDetailsProvider.notify();

                widget.setSt(
                  total,
                  // changeMoney
                );

                widget.refreshParent();

                Navigator.pop(context);
              },
              child: Container(
                color: Colors.green,
                height: 70,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Вернуть",
                        style: TextStyle(
                            color: addedReceiptProductChecked.isNotEmpty
                                ? Colors.white
                                : Colors.white70,
                            fontSize: 20),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
