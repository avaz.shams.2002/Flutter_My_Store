import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/supplier_invoice_details_model.dart';
import 'package:my_store/providers/supplier_invoice_details_provider.dart';
import 'package:my_store/providers/supplier_invoice_provider.dart';
import 'package:my_store/screens/return_of_supplier_details_screen.dart';
import 'package:provider/provider.dart';

import '../models/supplier_invoice_model.dart';

class SupplierEachDetailScrenn extends StatefulWidget {
  List<SupplierInvoiceDetails>? listOfSupplierDetails;
  double? total;
  String? getDateTime;
  SupplierInvoice? supplierInvoice;
  bool? fromReturnedPage;
  dynamic refreshTheParent;

  SupplierEachDetailScrenn(
      {Key? key,
      this.listOfSupplierDetails,
      this.total,
      this.getDateTime,
      this.supplierInvoice,
      this.fromReturnedPage,
      this.refreshTheParent
      })
      : super(key: key);

  @override
  State<SupplierEachDetailScrenn> createState() =>
      _SupplierEachDetailScrennState();
}

class _SupplierEachDetailScrennState extends State<SupplierEachDetailScrenn> {
  void setSt(
    double setTotal,
    // double setChangeMoney
  ) {
    setState(() {
      widget.total = setTotal;
      // changeMoney = setChangeMoney;
    });
  }

  double totalForCahngeMoney = 0.0;
  double changeMoney = 0.0;

  @override
  void initState() {}

  void justRefresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var supplierInvoiceProvider = Provider.of<SupplierAllProvider>(context);
    var supplierInvoiceDetailsProvider =
        Provider.of<SupplierInvoiceDetailsProvider>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 0,
        actions: [
          if (widget.fromReturnedPage == false)
            TextButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ReturnOfSupplierDetailsScreen(
                                supplierInvoice: widget.supplierInvoice,
                                list: widget.listOfSupplierDetails!
                                    .where((element) => element.returned == 0)
                                    .toList(),
                                setSt: setSt,
                                refreshParent: justRefresh,
                              )));
                },
                child: const Text(
                  "Возврат",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ))
        ],
      ),
      body: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    widget.total!.toStringAsFixed(2),
                    style: const TextStyle(color: Colors.black, fontSize: 30),
                  ),
                  const Text("Итого")
                ],
              )
            ],
          ),
          const Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Divider(
              thickness: 1,
              color: Colors.grey,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: widget.listOfSupplierDetails!.length,
              itemBuilder: (context, index) {
                var rounded = ((widget.listOfSupplierDetails![index].quantity));

                var sumOfQuantityofShtuk =
                    (((rounded! - rounded.toInt()) * 100).round() / 100);

                print(
                    "sumOfQuantityofShtuk ______ : ${(sumOfQuantityofShtuk * widget.listOfSupplierDetails![index].kolvoUpakovka!).round()}");

                sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                        widget.listOfSupplierDetails![index].kolvoUpakovka!)
                    .roundToDouble();

                //
                var total = (widget.listOfSupplierDetails![index].price! *
                        rounded.toInt()) +
                    ((sumOfQuantityofShtuk *
                            widget.listOfSupplierDetails![index].price!) /
                        widget.listOfSupplierDetails![index].kolvoUpakovka!);

                return Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${widget.listOfSupplierDetails![index].name}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: widget.listOfSupplierDetails![index]
                                            .returned ==
                                        0
                                    ? Colors.black
                                    : Colors.grey),
                          ),
                          if (widget.listOfSupplierDetails![index]
                                  .kolvoUpakovka! >
                              0)
                            Text(
                              "В упаковке: ${widget.listOfSupplierDetails![index].kolvoUpakovka}",
                              style: const TextStyle(color: Colors.grey),
                            ),
                          Text(
                            "Цена уп- ${widget.listOfSupplierDetails![index].price}",
                            style: const TextStyle(color: Colors.grey),
                          ),
                          if (widget.listOfSupplierDetails![index].returned ==
                              0)
                            Row(
                              children: [
                                Text(
                                  "(${widget.listOfSupplierDetails![index].quantity!.toInt().toString()}-уп",
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                                if (sumOfQuantityofShtuk != 0)
                                  Text(
                                    ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                    style: const TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                const Text(
                                  ")",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 17),
                                ),
                              ],
                            ),
                          const SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (widget.listOfSupplierDetails![index].returned ==
                              1)
                            const Text(
                              "Возвращено",
                              style: TextStyle(color: Colors.redAccent),
                            )
                          else
                            const Text("-")
                        ],
                      ),
                    ],
                  ),
                );
              }),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Divider(
              thickness: 1,
              color: Colors.grey,
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Итого"),
                      Text(widget.total!.toStringAsFixed(2))
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Наличные"),
                      Text(double.parse(widget.total.toString())
                          .toStringAsFixed(2))
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  if (changeMoney != 0.0)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Сдачи"),
                        Text(changeMoney.toStringAsFixed(2))
                      ],
                    ),
                ],
              )),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Divider(
              thickness: 1,
              color: Colors.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                Row(
                  children: [
                    const Text(
                      "Дата поступления: ",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Text("${widget.getDateTime?.substring(0, 11)}")
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    const Text(
                      "Время поступления: ",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Text("${widget.getDateTime?.substring(11, 19)}")
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 100,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  AlertDialog newDialog = AlertDialog(
                    title: const Text("Вы точно хотите удалить?"),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text("Нет")),
                      TextButton(
                          onPressed: () async {
                            supplierInvoiceDetailsProvider
                                .deleteSupplierInvoiceDetails(
                                    widget.listOfSupplierDetails!);
                            supplierInvoiceProvider.deleteSupplierInvoice(
                                widget.supplierInvoice!.id!);

                            await ProductMakerDb.deleteSupplierInvoiceDetails(
                                widget.listOfSupplierDetails!);
                            await ProductMakerDb.deleteSupplierInvoice(
                                widget.supplierInvoice!.id!);

                            widget.refreshTheParent(widget.supplierInvoice!);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: const Text("Да"))
                    ],
                  );
                  showDialog(context: context, builder: (context) => newDialog);
                },
                child: const Text("Удалить"),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.green)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
