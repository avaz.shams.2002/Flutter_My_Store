import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/contragent_maker_model.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/models/supplier_invoice_model.dart';
import 'package:my_store/pages/archive_page.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:my_store/providers/supplier_invoice_details_provider.dart';
import 'package:my_store/providers/supplier_invoice_provider.dart';
import 'package:my_store/screens/side_bar.dart';
import 'package:my_store/screens/tovar_about.dart';
import 'package:my_store/widgets/producf_search_widget.dart';
import 'package:provider/provider.dart';

import '../pages/create_product_page.dart';
import '../providers/category_provider.dart';

class ProductScreen extends StatefulWidget {
  const ProductScreen({Key? key}) : super(key: key);

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  int changeSearch = 0;

  var focus = FocusNode();

  late final TextEditingController _searchController =
      TextEditingController(text: '');

  List<ProductMaker> listForSearch = [];

  @override
  Widget build(BuildContext context) {
    var _textStyle = const TextStyle(color: Colors.black, fontSize: 20);
    var productCart = Provider.of<ProductMakerProvider>(context);
    var categoryProvider = Provider.of<CategoryProvider>(context);

    var supplierInvoiceDetailsProvider =
        Provider.of<SupplierInvoiceDetailsProvider>(context);

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.pink,
            onPressed: () {
              showModalBottomSheet(
                  isScrollControlled: true,
                  context: context,
                  builder: (context) {
                    return Container(
                      height:
                          MediaQuery.of(context).copyWith().size.height * 0.30,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const [
                                Icon(Icons.document_scanner),
                                Text("Создать",
                                    style: TextStyle(
                                      color: Colors.black,
                                    ))
                              ]),
                          const SizedBox(
                            height: 20,
                          ),
                          TextButton(
                              onPressed: () {
                                setState(() {});
                                changeSearch = 0;
                                listForSearch.clear();
                                FocusManager.instance.primaryFocus!.unfocus();
                                Navigator.pop(context);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => CreateProductPage(
                                          image_: null,
                                          category: categoryProvider
                                                  .categoryList.isEmpty
                                              ? []
                                              : categoryProvider
                                                  .getCategoryNotArchive(),
                                          newCategory: true,
                                        )));
                              },
                              child: Text(
                                "Товар",
                                style: _textStyle,
                              )),
                          TextButton(
                              onPressed: () {},
                              child: Text(
                                "Комплект",
                                style: _textStyle,
                              )),
                          TextButton(
                              onPressed: () {},
                              child: Text(
                                "Услуга",
                                style: _textStyle,
                              )),
                        ],
                      ),
                    );
                  });
            },
            child: const Text(
              "+",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          drawer: const SideBar(),
          appBar: AppBar(
            iconTheme: IconThemeData(
                color: changeSearch == 0 ? Colors.white : Colors.black),
            backgroundColor:
                changeSearch == 0 ? Colors.blueAccent : Colors.white,
            elevation: 0,
            title: changeSearch == 0
                ? const Text("Товары")
                : Stack(
                    children: [
                      TextField(
                        focusNode: focus,
                        decoration: const InputDecoration(
                            hintText: "Введите имя товара"),
                        autofocus: true,
                        controller: _searchController,
                        onChanged: (v) {
                          setState(() {});
                          listForSearch.clear();
                          for (var all in productCart.getNotArchiveTovar()) {
                            if (all.naimenovanie!.toUpperCase().contains(
                                _searchController.text.trim().toUpperCase())) {
                              listForSearch.add(all);
                            }
                          }
                          if (v.isEmpty) {
                            listForSearch.clear();
                          }
                        },
                      ),
                      Positioned(
                          right: 0,
                          bottom: 0,
                          child: IconButton(
                              onPressed: () {
                                setState(() {});
                                changeSearch = 0;
                                listForSearch.clear();
                                FocusManager.instance.primaryFocus!.unfocus();
                              },
                              icon: const Icon(
                                Icons.close,
                                color: Colors.black,
                              )))
                    ],
                  ),
            actions: [
              if (changeSearch == 0)
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ArchivePage()));
                        },
                        icon: const Icon(Icons.archive_outlined)),
                    IconButton(
                        onPressed: () {
                          setState(() {});
                          changeSearch = 1;
                          _searchController.text = '';
                          Future.delayed(const Duration(milliseconds: 5), () {
                            FocusScope.of(context).requestFocus(focus);
                          });
                          // showSearch(
                          //     context: context,
                          //     delegate: ProductSearchWidget(
                          //         listProduct: productCart.getNotArchiveTovar()));
                        },
                        icon: const Icon(Icons.search))
                  ],
                )
            ],
          ),
          body: ListView(
            children: [
              if (changeSearch == 1)
                if (listForSearch.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: ListView.separated(
                      shrinkWrap: true,
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(
                        color: Colors.black,
                        thickness: 1,
                      ),
                      itemCount: listForSearch.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            goToEditorProduct(supplierInvoiceDetailsProvider, index,
                                productCart);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    if (listForSearch[index].image.toString() ==
                                            "File: 'null'" ||
                                        listForSearch[index].image == null)
                                      Container(
                                        width: 100,
                                        height: 50,
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/1.jpg"),
                                            fit: BoxFit.fitWidth,
                                            alignment: Alignment.bottomLeft,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "${listForSearch[index].naimenovanie![0]}"
                                            "${listForSearch[index].naimenovanie![productCart.getNotArchiveTovar()[index].naimenovanie!.length - 1]}",
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 20),
                                          ),
                                        ),
                                      )
                                    else
                                      Container(
                                          width: 100,
                                          height: 50,
                                          child: Image.file(
                                              listForSearch[index].image!)),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(left: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            if (listForSearch[index]
                                                .naimenovanie!
                                                .isNotEmpty)
                                              Text(
                                                  "${listForSearch[index].code} • ${listForSearch[index].naimenovanie}"),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                const Icon(
                                                  Icons
                                                      .shopping_cart_checkout_outlined,
                                                  color: Colors.pink,
                                                ),
                                                Text(listForSearch[index]
                                                    .quantity
                                                    .toString())
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    IconButton(
                                        onPressed: () {
                                          ProductMakerDb.deleteProduct(
                                              listForSearch[index].id!);
                                          productCart.deleteProduct(
                                              listForSearch[index]);
                                        },
                                        icon: const Icon(
                                          Icons.delete,
                                          color: Colors.blueGrey,
                                        ))
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  )
                else
                  productsListView(productCart, supplierInvoiceDetailsProvider)
              else
                productsListView(productCart, supplierInvoiceDetailsProvider)
            ],
          )),
    );
  }

  void goToEditorProduct(supplierInvoiceDetailsProvider, index, productCart) {
    setState(() {});
    changeSearch = 0;
    listForSearch.clear();
    FocusManager.instance.primaryFocus!.unfocus();
    var get = supplierInvoiceDetailsProvider.listSupplierInvoiceDetails
        .where(
          (element) =>
              element.productId == productCart.getNotArchiveTovar()[index].id,
        )
        .toList();

    var getTotal = 0.0;
    get.forEach((element) {
      if (element.returned == 0) {
        getTotal += element.quantity!;
      }
    });

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TovarAbout(
                productMaker: productCart.getNotArchiveTovar()[index],
                getKolvo: getTotal)));
  }

  Widget productsListView(productCart, supplierInvoiceDetailsProvider) {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: Colors.black,
          thickness: 1,
        ),
        itemCount: productCart.getNotArchiveTovar().length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              goToEditorProduct(supplierInvoiceDetailsProvider, index, productCart);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      if (productCart
                                  .getNotArchiveTovar()[index]
                                  .image
                                  .toString() ==
                              "File: 'null'" ||
                          productCart.getNotArchiveTovar()[index].image == null)
                        Container(
                          width: 100,
                          height: 50,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage("assets/images/1.jpg"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.bottomLeft,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "${productCart.getNotArchiveTovar()[index].naimenovanie![0]}"
                              "${productCart.getNotArchiveTovar()[index].naimenovanie![productCart.getNotArchiveTovar()[index].naimenovanie!.length - 1]}",
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 20),
                            ),
                          ),
                        )
                      else
                        Container(
                            width: 100,
                            height: 50,
                            child: Image.file(productCart
                                .getNotArchiveTovar()[index]
                                .image!)),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              if (productCart
                                  .getNotArchiveTovar()[index]
                                  .naimenovanie!
                                  .isNotEmpty)
                                Text(
                                    "${productCart.getNotArchiveTovar()[index].code} • ${productCart.getNotArchiveTovar()[index].naimenovanie}"),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Icon(
                                    Icons.shopping_cart_checkout_outlined,
                                    color: Colors.pink,
                                  ),
                                  Text(productCart
                                      .getNotArchiveTovar()[index]
                                      .quantity
                                      .toString())
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      IconButton(
                          onPressed: () {
                            ProductMakerDb.deleteProduct(
                                productCart.getNotArchiveTovar()[index].id!);
                            productCart.deleteProduct(
                                productCart.getNotArchiveTovar()[index]);
                          },
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.blueGrey,
                          ))
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
