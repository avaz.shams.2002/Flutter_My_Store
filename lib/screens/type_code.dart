import 'package:flutter/material.dart';
import 'package:my_store/providers/barcode_provider.dart';
import 'package:provider/provider.dart';

class TypeCode extends StatelessWidget {
  const TypeCode({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> codeMass = ["EAN8", "EAN13", "UPC", "GTIN", "Code128"];
    var setBarcode = Provider.of<Barcode>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        title: const Text(
          "Тип кода",
          style: TextStyle(color: Colors.grey),
        ),
      ),
      body: ListView.separated(
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: Colors.black,
        ),
        itemCount: codeMass.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              // setBarcode
              //     .setController(TextEditingController(text: codeMass[index]));
              // Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(codeMass[index]),
                  const SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
