import 'package:flutter/material.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';
import '../providers/discount_provider.dart';
import '../providers/product_maker_provider.dart';
import 'change_price_screen.dart';

class CategoryProductScreen extends StatelessWidget {
  List<ProductMaker>? productMakerList;

  CategoryProductScreen({Key? key, this.productMakerList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var productMaker = Provider.of<ProductMakerProvider>(context);
    var discountProvider = Provider.of<DiscountProvier>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 0,
        title: const Text("Категории тоавров"),
        actions: [],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: ListView.separated(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          separatorBuilder: (BuildContext context, int index) => const Divider(
            color: Colors.black,
            thickness: 1,
          ),
          itemCount: productMakerList!.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () async {
                await ProductMakerDb.addItemToDbProduct(
                    productMakerList![index]);
                productMaker.notify();
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        if (productMakerList![index].image.toString() ==
                                "File: 'null'" ||
                            productMakerList![index].image == null)
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ChangePriceScreen(
                                            productMaker:
                                                productMakerList![index],
                                            discount:
                                                discountProvider.listDiscount,
                                          )));
                            },
                            child: Container(
                              width: 100,
                              height: 50,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/images/1.jpg"),
                                  fit: BoxFit.fitWidth,
                                  alignment: Alignment.bottomLeft,
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  "${productMakerList![index].naimenovanie![0]}"
                                  "${productMakerList![index].naimenovanie![productMakerList![index].naimenovanie!.length - 1]}",
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ),
                            ),
                          )
                        else
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ChangePriceScreen(
                                            productMaker:
                                                productMakerList![index],
                                            discount:
                                                discountProvider.listDiscount,
                                          )));
                            },
                            child: Container(
                                width: 100,
                                height: 50,
                                child: Image.file(
                                    productMakerList![index].image!)),
                          ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                if (productMakerList![index]
                                    .naimenovanie!
                                    .isNotEmpty)
                                  Text(
                                      "${productMakerList![index].code} • ${productMakerList![index].naimenovanie}"),
                                const SizedBox(
                                  height: 10,
                                ),
                                productMakerList![index].quantity == 0
                                    ? const Text(
                                        "Нажмите чтобы добавить",
                                        style: TextStyle(color: Colors.pink),
                                      )
                                    : Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          const Icon(
                                            Icons
                                                .shopping_cart_checkout_outlined,
                                            color: Colors.pink,
                                          ),
                                          Text(productMakerList![index]
                                              .quantity
                                              .toString()),
                                        ],
                                      )
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChangePriceScreen(
                                          productMaker:
                                              productMakerList![index],
                                          discount:
                                              discountProvider.listDiscount
                                        )));
                          },
                          child: Text(
                            "${productMakerList![index].quantity * productMakerList![index].prodazhiPrice!}",
                            style: const TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 17),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
