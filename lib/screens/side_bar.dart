import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_store/pages/CategoryPage.dart';
import 'package:my_store/pages/discount_page.dart';
import 'package:my_store/pages/home_page.dart';
import 'package:my_store/pages/kontragent_page.dart';
import 'package:my_store/pages/login_page.dart';
import 'package:my_store/pages/receipt_page.dart';
import 'package:my_store/pages/sales_page.dart';
import 'package:my_store/providers/check_field_login_page.dart';
import 'package:my_store/providers/current_page_provider.dart';
import 'package:my_store/providers/user_check.dart';
import 'package:my_store/screens/product_screen.dart';
import 'package:provider/provider.dart';

class SideBar extends StatefulWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  State<SideBar> createState() => _SideBarState();
}


class _SideBarState extends State<SideBar> {
  @override
  Widget build(BuildContext context) {
    var getCurrentState = Provider.of<CurrentStateProvider>(context);
    var checkUser = Provider.of<CheckUser>(context, listen: false);

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.blueAccent),
            accountName: const Text(
              "Администратор",
              style: TextStyle(color: Colors.white),
            ),
            accountEmail: Text(
              checkUser.user ?? "${checkUser.user}",
              style: const TextStyle(color: Colors.white),
            ),
          ),
          ListTile(
            title: Text("Уведомление"),
            onTap: () {},
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 2
                ? Colors.blueAccent
                : Colors.white,
            title: Text("Показатели",
                style: TextStyle(
                    color: getCurrentState.currentPage == 2
                        ? Colors.white
                        : Colors.black)),
            onTap: () {
              int state = 2;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const HomePage()));
              getCurrentState.setCurrentPage(state);
            },
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 3
                ? Colors.blueAccent
                : Colors.white,
            title: Text(
              "Товары",
              style: TextStyle(
                  color: getCurrentState.currentPage == 3
                      ? Colors.white
                      : Colors.black),
            ),
            onTap: () {
              int state = 3;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ProductScreen()));
              getCurrentState.setCurrentPage(state);
            },
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 4
                ? Colors.blueAccent
                : Colors.white,
            title: Text(
              "Поставщики",
              style: TextStyle(
                  color: getCurrentState.currentPage == 4
                      ? Colors.white
                      : Colors.black),
            ),
            onTap: () {
              int state = 4;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const KontragentPage()));
              getCurrentState.setCurrentPage(state);
            },
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 5
                ? Colors.blueAccent
                : Colors.white,
            title: Text("Продажи",
                style: TextStyle(
                    color: getCurrentState.currentPage == 5
                        ? Colors.white
                        : Colors.black)),
            onTap: () {
              int state = 5;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SalesPage()));
              getCurrentState.setCurrentPage(state);
              SystemChannels.textInput.invokeListMethod('TextInput.hide');
            },
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 6
                ? Colors.blueAccent
                : Colors.white,
            title: Text("Категории",
                style: TextStyle(
                    color: getCurrentState.currentPage == 6
                        ? Colors.white
                        : Colors.black)),
            onTap: () {
              int state = 6;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const CategoryPage()));
              getCurrentState.setCurrentPage(state);
            },
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 7
                ? Colors.blueAccent
                : Colors.white,
            title: Text("Скидки",
                style: TextStyle(
                    color: getCurrentState.currentPage == 7
                        ? Colors.white
                        : Colors.black)),
            onTap: () {
              int state = 7;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const DiscountPage()));
              getCurrentState.setCurrentPage(state);
            },
          ),
          ListTile(
            tileColor: getCurrentState.currentPage == 8
                ? Colors.blueAccent
                : Colors.white,
            title: Text("Все продажи",
                style: TextStyle(
                    color: getCurrentState.currentPage == 8
                        ? Colors.white
                        : Colors.black)),
            onTap: () {
              int state = 8;
              if (getCurrentState.currentPage == state) {
                return;
              }
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ReceiptPage()));
              getCurrentState.setCurrentPage(state);
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TextButton(
                  onPressed: () async {
                    if (checkUser.user == null) {
                      Navigator.pop(context);
                      return;
                    }

                    checkUser.signOut(
                        context: context,
                        currentPage: getCurrentState.currentPage);
                    
                        
                  },
                  child: Text("Sign out")),
            ],
          )
        ],
      ),
    );
  }
}
