import 'package:flutter/material.dart';
import 'package:my_store/models/client_model.dart';
import 'package:my_store/pages/receipt_page_about.dart';

import '../models/receipt_model.dart';

class ClientHistory extends StatefulWidget {
  Client? client;

  ClientHistory({Key? key, this.client}) : super(key: key);

  @override
  State<ClientHistory> createState() => _ClientHistoryState();
}

class _ClientHistoryState extends State<ClientHistory> {
  late final TextEditingController _searchController =
      TextEditingController(text: '');

  List<Receipt> listForSearch = [];

  int changeSearch = 0;

  var focus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        title: const Text(
          "История покупок",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Column(
        children: [
          const Divider(
            thickness: 1,
            color: Colors.grey,
          ),
          InkWell(
            onTap: () {
              setState(() {});
              changeSearch = 1;
              Future.delayed(const Duration(milliseconds: 5), () {
                FocusScope.of(context).requestFocus(focus);
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Row(
                children: [
                  const Icon(
                    Icons.search,
                    size: 30,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  if (changeSearch == 0)
                    const Text(
                      "Поиск",
                      style: TextStyle(fontSize: 20),
                    ),
                  if (changeSearch == 1)
                    Expanded(
                      child: Stack(
                        children: [
                          TextField(
                            focusNode: focus,
                            controller: _searchController,
                            decoration: const InputDecoration(
                                hintText: "Введите имя чека"),
                            onChanged: (v) {
                              setState((){});
                              listForSearch.clear();
                              for (var all in widget.client!.receipt!) {
                                if (all.id
                                    .toString()
                                    .toUpperCase()
                                    .contains(_searchController.text.trim().toUpperCase())) {
                                  listForSearch.add(all);
                                }
                              }
                            },
                          ),
                          Positioned(
                              bottom: 0,
                              right: 0,
                              child: IconButton(
                                onPressed: () {
                                  setState(() {});
                                  changeSearch = 0;
                                  _searchController.text = '';
                                  listForSearch.clear();
                                },
                                icon: const Icon(
                                  Icons.close,
                                  color: Colors.black,
                                ),
                              ))
                        ],
                      ),
                    )
                ],
              ),
            ),
          ),
          const Divider(
            thickness: 1,
            color: Colors.grey,
          ),
          if (changeSearch == 1)
            if (listForSearch.isNotEmpty)
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: listForSearch.length,
                    itemBuilder: (contxet, index) {
                      var returnedTovar = listForSearch[index]
                          .listReceiptProduct!
                          .where((element) =>
                              element.receiptId == listForSearch[index].id)
                          .toList();
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ReceiptPageAbout(
                                        receipt: listForSearch[index],
                                        productMaker: listForSearch[index]
                                            .listReceiptProduct,
                                      )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  const Icon((Icons.money_outlined)),
                                  const SizedBox(
                                    width: 30,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(listForSearch[index].id.toString()),
                                      Text(
                                          "Время: ${listForSearch[index].dateTime!.substring(0, 11)} в "
                                          "${listForSearch[index].dateTime!.substring(11, 16)}")
                                    ],
                                  )
                                ],
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(double.parse(listForSearch[index].total!)
                                        .toStringAsFixed(2)),
                                    if (returnedTovar.every(
                                        (element) => element.returned == 1))
                                      const Text(
                                        "Возвращено",
                                        style: TextStyle(color: Colors.redAccent),
                                      )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            else
              receiptListView()
          else
            receiptListView()
        ],
      ),
    );
  }

  Widget receiptListView() {
    if (widget.client != null) {
      if (widget.client!.receipt != null) {
        return Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.client!.receipt!.length,
              itemBuilder: (contxet, index) {
                var returnedTovar = widget
                    .client!.receipt![index].listReceiptProduct!
                    .where((element) =>
                        element.receiptId == widget.client!.receipt![index].id)
                    .toList();
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ReceiptPageAbout(
                                  receipt: widget.client!.receipt![index],
                                  productMaker: widget
                                      .client!.receipt![index].listReceiptProduct,
                                )));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const Icon((Icons.money_outlined)),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    widget.client!.receipt![index].id.toString()),
                                Text(
                                    "Время: ${widget.client!.receipt![index].dateTime!.substring(0, 11)} в "
                                    "${widget.client!.receipt![index].dateTime!.substring(11, 16)}")
                              ],
                            )
                          ],
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(double.parse(
                                      widget.client!.receipt![index].total!)
                                  .toStringAsFixed(2)),
                              if (returnedTovar
                                  .every((element) => element.returned == 1))
                                const Text(
                                  "Возвращено",
                                  style: TextStyle(color: Colors.redAccent),
                                )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
        );
      }
    }
    return Text("");
  }
}
