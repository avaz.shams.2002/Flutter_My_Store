import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/discount_maker_model.dart';
import 'package:my_store/providers/discount_provider.dart';
import 'package:provider/provider.dart';

class DiscountCreatorScreen extends StatefulWidget {
  Discount? discount;

  DiscountCreatorScreen({Key? key, this.discount}) : super(key: key);

  @override
  State<DiscountCreatorScreen> createState() => _DiscountCreatorScreenState();
}

class _DiscountCreatorScreenState extends State<DiscountCreatorScreen> {
  late TextEditingController _nameController = TextEditingController(text: '');
  late TextEditingController _valueController = TextEditingController(text: '');
  int typeOfDiscount = 1;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    if (widget.discount != null) {
      _nameController.text = widget.discount!.name!;
      _valueController.text = widget.discount!.value!.toString();
      typeOfDiscount = widget.discount!.percentOrSumma!;
    }
  }

  @override
  Widget build(BuildContext context) {
    var discountProvider = Provider.of<DiscountProvier>(context);
    return InkWell(
      onTap: () {
        FocusManager.instance.primaryFocus!.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          elevation: 0,
          title: const Text("Создание скидки"),
          actions: [
            TextButton(
                onPressed: () async {
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }

                  if (widget.discount == null) {
                    Discount newDiscount = Discount(
                        name: _nameController.text,
                        value: double.parse(_valueController.text.trim()),
                        percentOrSumma: typeOfDiscount);

                    await ProductMakerDb.insertIntoDiscount(newDiscount);

                    discountProvider.addDiscount(newDiscount);
                    Navigator.pop(context);
                    return;
                  }

                  widget.discount!.name = _nameController.text;
                  widget.discount!.value = double.parse(_valueController.text);
                  widget.discount!.percentOrSumma = typeOfDiscount;

                  await ProductMakerDb.updateDiscount(widget.discount!);

                  discountProvider.notify();
                  Navigator.pop(context);
                },
                child: const Text(
                  "СОХРАНИТЬ",
                  style: const TextStyle(color: Colors.white, fontSize: 16),
                ))
          ],
        ),
        body: InkWell(
          onTap: () {
            FocusManager.instance.primaryFocus!.unfocus();
          },
          child: Container(
            height: 300,
            child: Card(
              elevation: 3,
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          labelText: "Название",
                          labelStyle:
                              TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        controller: _nameController,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Поле не может быть пустым";
                          }
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: 180,
                            child: TextFormField(
                              onTap: (){
                                setState((){});
                                _valueController.text = '';
                              },
                              decoration: const InputDecoration(
                                labelText: "Значение",
                                labelStyle:
                                    TextStyle(color: Colors.grey, fontSize: 14),
                              ),
                              controller: _valueController,
                              keyboardType: TextInputType.number,
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Поле не может быть пустым";
                                }
                              },
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {});
                              typeOfDiscount = 1;
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: typeOfDiscount == 1
                                      ? Colors.green[50]
                                      : Colors.white70,
                                  border: Border.all(color: Colors.grey)),
                              width: 70,
                              height: 50,
                              child: const Center(child: Icon(Icons.percent)),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {});
                              typeOfDiscount = 2;
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: typeOfDiscount == 2
                                      ? Colors.green[50]
                                      : Colors.white70,
                                  border: Border.all(color: Colors.grey)),
                              width: 70,
                              height: 50,
                              child: const Center(
                                child: Text(
                                  "∑",
                                  style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
