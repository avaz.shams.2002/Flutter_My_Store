import 'package:flutter/material.dart';
import 'package:my_store/models/client_model.dart';
import 'package:my_store/providers/client_provider.dart';
import 'package:my_store/screens/client_creator_screen.dart';
import 'package:my_store/screens/client_history_screen.dart';
import 'package:provider/provider.dart';

class ClientProfileScreen extends StatefulWidget {
  Client? client;

  ClientProfileScreen({Key? key, this.client}) : super(key: key);

  @override
  State<ClientProfileScreen> createState() => _ClientProfileScreenState();
}

class _ClientProfileScreenState extends State<ClientProfileScreen> {
  void refreshParent(Client client) {
    setState(() {});
    widget.client = client;
  }

  List<Widget> buttonValues = [
    Row(
      children: const [
        Icon(Icons.delete, color: Colors.grey),
        SizedBox(
          width: 10,
        ),
        Text("Удалить из чека"),
      ],
    ),
  ];

  String value = 'Удалить из чека';

  @override
  Widget build(BuildContext context) {
    var clientProvider = Provider.of<ClientProvider>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        title: const Text(
          "Профиль клиента",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0,
        actions: [
          if (clientProvider.addedClient.isEmpty)
            TextButton(
                onPressed: () {
                  clientProvider.addedClientFunc(widget.client!);
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: const Text("ДОБАВИТЬ В ЧЕК")),
          if (clientProvider.addedClient.isNotEmpty)
            DropdownButtonHideUnderline(
              child: DropdownButton(
                  icon: const Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.more_vert,
                    ),
                  ),
                  items: buttonValues
                      .map((e) => DropdownMenuItem(
                            child: e,
                            value: e,
                          ))
                      .toList(),
                  onChanged: (val) async {
                    if (val == buttonValues[0]) {
                      clientProvider.deleteaddedClient(widget.client!);
                      Navigator.pop(context);
                    }
                  }),
            )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Divider(
            thickness: 1,
            color: Colors.grey,
          ),
          Center(
            child: Column(
              children: [
                const Center(
                    child: Icon(
                  Icons.account_circle_rounded,
                  color: Colors.grey,
                  size: 80,
                )),
                Text(
                  "${widget.client!.name}",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 17),
                ),
                const Divider(
                  color: Colors.grey,
                  thickness: 1,
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Row(
              children: [
                const Icon(
                  Icons.star,
                  size: 30,
                ),
                const SizedBox(
                  width: 30,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "0.0",
                      style: TextStyle(fontSize: 17),
                    ),
                    Text("Баллы",
                        style: TextStyle(fontSize: 17, color: Colors.grey))
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Row(
              children: [
                const Icon(
                  Icons.shopping_basket,
                  size: 30,
                ),
                const SizedBox(
                  width: 30,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${widget.client!.numberOfVisits!}",
                      style: TextStyle(fontSize: 17),
                    ),
                    const Text("Посещений",
                        style:
                            const TextStyle(fontSize: 17, color: Colors.grey))
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Row(
              children: [
                const Icon(
                  Icons.event_note,
                  size: 30,
                ),
                const SizedBox(
                  width: 30,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.client!.lastVisit == ''
                          ? '-'
                          : "${widget.client!.lastVisit!.substring(0, 11)} в "
                              "${widget.client!.lastVisit!.substring(11, 19)}",
                      style: const TextStyle(fontSize: 17),
                    ),
                    const Text("Последнее посещение",
                        style: TextStyle(fontSize: 17, color: Colors.grey))
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: TextButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ClientCreatorScreen(
                                client: widget.client!,
                                refreshParent: refreshParent,
                              )));
                },
                child: const Text("РЕДАКТИРОВАТЬ ПРОФИЛЬ")),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: TextButton(
                onPressed: () {}, child: const Text("СПИСАТЬ БАЛЛЫ")),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: TextButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ClientHistory(
                    client: widget.client!,
                  )));
                }, child: const Text("ПОСМОТРЕТЬ ПОКУПКИ")),
          )
        ],
      ),
    );
  }
}
