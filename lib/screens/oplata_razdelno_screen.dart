import 'package:flutter/material.dart';

import '../models/product_make_model.dart';

class OplataRazdelnoScreen extends StatelessWidget {
  double? allPrice;
  List<ProductMaker>? productMaker;

  OplataRazdelnoScreen({Key? key, this.allPrice, this.productMaker})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        title: Text("Осталось $allPrice"),
        actions: [
          TextButton(
              onPressed: () {},
              child: const Text(
                "Готово",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );
  }
}
