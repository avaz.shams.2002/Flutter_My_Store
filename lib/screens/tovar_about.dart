import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/models/supplier_invoice_details_model.dart';
import 'package:my_store/pages/create_product_page.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:provider/provider.dart';

import '../providers/category_provider.dart';

class TovarAbout extends StatefulWidget {
  ProductMaker? productMaker;
  double? getKolvo;

  TovarAbout({Key? key, this.productMaker, this.getKolvo}) : super(key: key);

  @override
  State<TovarAbout> createState() => _TovarAboutState();
}

class _TovarAboutState extends State<TovarAbout> {
  var sumOfQuantityofShtuk = 0.0;
  var quantity = 0.0;

  @override
  void initState() {
    if (widget.getKolvo != null) {
      quantity = widget.getKolvo!;

      sumOfQuantityofShtuk =
          (((quantity - quantity.toInt()) * 100).round() / 100);

      sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
              int.parse(widget.productMaker!.kolvoUpakovka!))
          .roundToDouble();

      if (sumOfQuantityofShtuk ==
          int.parse(widget.productMaker!.kolvoUpakovka!)) {
        quantity = widget.getKolvo!.toInt() + 1;
        sumOfQuantityofShtuk = 0.0;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> buttonValues = ['Редактировать', 'Удалить'];
    var productCart = Provider.of<ProductMakerProvider>(context);
    var categoryProvider = Provider.of<CategoryProvider>(context);
    var _textStyleForInfo =
        const TextStyle(color: Colors.blueAccent, fontSize: 13);
    var _textStyleForZaglav =
        const TextStyle(color: Colors.blueAccent, fontSize: 14);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        iconTheme: const IconThemeData(color: Colors.white),
        elevation: 0,
        title: const Text(
          "Товар",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 5, top: 5),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                  icon: const Icon(
                    Icons.more_vert,
                    color: Colors.white,
                  ),
                  items: buttonValues
                      .map((e) => DropdownMenuItem(
                            child: Text(e),
                            value: e,
                          ))
                      .toList(),
                  onChanged: (val) {
                    if (val == buttonValues[0]) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CreateProductPage(
                                    productMaker: widget.productMaker!,
                                    image_: widget.productMaker!.image,
                                    category:
                                        categoryProvider.categoryList.isEmpty
                                            ? []
                                            : categoryProvider
                                                .getCategoryNotArchive(),
                                    newCategory: false,
                                  )));
                    }
                    if (val == buttonValues[1]) {
                      ProductMakerDb.deleteProduct(widget.productMaker!.id!);
                      productCart.deleteProduct(widget.productMaker!);
                      Navigator.pop(context);
                    }
                  }),
            ),
          )
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: Card(
              shadowColor: Colors.black,
              elevation: 2,
              child: Column(
                children: [
                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                            "${widget.productMaker!.code} • ${widget.productMaker!.naimenovanie}"),
                      ],
                    ),
                  ),
                  if (widget.productMaker!.image.toString() == "File: 'null'" ||
                      widget.productMaker!.image.toString() == "null" ||
                      widget.productMaker!.image == null)
                    Center(
                      child: Container(
                        width: 200,
                        height: 100,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage("assets/images/1.jpg"),
                            fit: BoxFit.fitWidth,
                            alignment: Alignment.bottomLeft,
                          ),
                        ),
                        child: Center(
                          child: Text(
                            "${widget.productMaker!.naimenovanie![0]}"
                            "${widget.productMaker!.naimenovanie![widget.productMaker!.naimenovanie!.length - 1]}",
                            style: const TextStyle(
                                color: Colors.white, fontSize: 25),
                          ),
                        ),
                      ),
                    )
                  else
                    SizedBox(
                        width: 200,
                        height: 100,
                        child: Image.file(widget.productMaker!.image!)),
                  const SizedBox(
                    height: 15,
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Card(
              shadowColor: Colors.black,
              elevation: 2,
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.window,
                        color: Colors.blueAccent,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Остатки",
                        style: _textStyleForZaglav,
                      )
                    ],
                  ),
                ),
                const Divider(
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        "Доступно",
                        style: TextStyle(color: Colors.grey),
                      ),
                      Text("Остаток", style: TextStyle(color: Colors.grey)),
                      Text("Резерв", style: TextStyle(color: Colors.grey)),
                      Text("Ожидание", style: TextStyle(color: Colors.grey)),
                    ],
                  ),
                ),
                const Divider(
                  thickness: 1,
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15, left: 15),
                  child: Column(
                    children: [
                      Row(
                        children: const [Text("Всего")],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: const [
                          Text("0"),
                          Text("0"),
                          Text("0"),
                          Text("0"),
                        ],
                      ),
                    ],
                  ),
                ),
                const Divider(
                  color: Colors.grey,
                  thickness: 1,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15, left: 15),
                  child: Column(
                    children: [
                      Row(
                        children: const [Text("Основной склад")],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: const [
                          Text("0"),
                          Text("0"),
                          Text("0"),
                          Text("0"),
                        ],
                      ),
                    ],
                  ),
                ),
                const Divider(
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Себестоимость",
                            style: TextStyle(color: Colors.grey),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                              "${widget.productMaker!.prodazhiPrice!}${widget.productMaker!.prodazhiCurrency!.toLowerCase()}")
                        ],
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Card(
              shadowColor: Colors.black,
              elevation: 2,
              child: Column(
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.wallet_membership,
                              color: Colors.blueAccent,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Цены",
                              style: _textStyleForZaglav,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  const Divider(
                    thickness: 1,
                    color: Colors.grey,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Минимальная"),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                            "${widget.productMaker!.minimalPrice} ${widget.productMaker!.minimalCurrency}"),
                        const Divider(
                          thickness: 1,
                          color: Colors.grey,
                        ),
                        const Text("Цена закупки"),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                            "${widget.productMaker!.zakupkiPrice}${widget.productMaker!.zakupkiCurrency!.toLowerCase()}"),
                        const Divider(
                          thickness: 1,
                          color: Colors.grey,
                        ),
                        const Text("Цена продажи"),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                            "${widget.productMaker!.prodazhiPrice}${widget.productMaker!.prodazhiCurrency!.toLowerCase()}"),
                        const SizedBox(
                          height: 15,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Card(
              shadowColor: Colors.black,
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.info,
                          color: Colors.blueAccent,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Информация",
                          style: _textStyleForZaglav,
                        )
                      ],
                    ),
                    const Divider(
                      color: Colors.grey,
                      thickness: 1,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    if (widget.productMaker!.opisanie!.isNotEmpty)
                      Text("Описание: ${widget.productMaker!.opisanie}",
                          style: _textStyleForInfo),
                    const SizedBox(
                      height: 15,
                    ),
                    if (widget.productMaker!.strana!.isNotEmpty)
                      Text(
                        "Страна: ${widget.productMaker!.strana}",
                        style: _textStyleForInfo,
                      ),
                    if (widget.productMaker!.ves!.isNotEmpty)
                      Text(
                        "Вес: ${widget.productMaker!.ves}",
                        style: _textStyleForInfo,
                      ),
                    if (widget.productMaker!.obyom!.isNotEmpty)
                      Text(
                        "Объем: ${widget.productMaker!.obyom}",
                        style: _textStyleForInfo,
                      ),
                    if (widget.productMaker!.groupe!.isNotEmpty)
                      Text(
                        "Категория: ${widget.productMaker!.groupe}",
                        style: _textStyleForInfo,
                      ),
                    // if (productMaker!.nds!.isNotEmpty)
                    //   Text("${productMaker!.nds}"),
                    if (widget.productMaker!.neSnizhemiyOstatok!.isNotEmpty)
                      Text(
                        "Неснижаемый остаток: ${widget.productMaker!.neSnizhemiyOstatok}",
                        style: _textStyleForInfo,
                      ),
                    if (widget.productMaker!.shtrihkod != null)
                      Text(
                        "Штрихкод : ${widget.productMaker!.shtrihkod}",
                        style: _textStyleForInfo,
                      ),
                    if (widget.productMaker!.kolvoUpakovka!.isNotEmpty)
                      Text(
                        "В упаковке: ${widget.productMaker!.kolvoUpakovka!}шт",
                        style: _textStyleForInfo,
                      ),
                    if (quantity != 0.0)
                      Row(children: [
                        Text("Поступление: ", style: _textStyleForInfo),
                        Text(
                          " (${quantity.toInt().toString()}-уп",
                          style: _textStyleForInfo,
                        ),
                        if (sumOfQuantityofShtuk != 0.0)
                          Text(
                            ", ${sumOfQuantityofShtuk.toInt()}-шт",
                            style: _textStyleForInfo,
                          ),
                        Text(
                          ")",
                          style: _textStyleForInfo,
                        )
                      ]),
                    if (widget.productMaker!.postavshik!.isNotEmpty)
                      Text("Поставщик: ${widget.productMaker!.postavshik}",
                          style: _textStyleForInfo),
                    const SizedBox(
                      height: 15,
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
