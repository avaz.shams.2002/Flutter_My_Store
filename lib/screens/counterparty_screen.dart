import 'package:flutter/material.dart';
import 'package:my_store/pages/supplier_creater_page.dart';
import 'package:my_store/screens/side_bar.dart';
import 'package:provider/provider.dart';

import '../models/contragent_maker_model.dart';
import '../providers/contragent_provider.dart';

class CounterPartyScreen extends StatelessWidget {
  Contragent? contragent;
  CounterPartyScreen({Key? key, this.contragent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var contragentProvider = Provider.of<ContragentProvider>(context);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: const Text("+",
              style: TextStyle(color: Colors.white, fontSize: 20)),
          backgroundColor: Colors.pink,
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SupplierCreatorPage(
                          contragent: Contragent(),
                          contactWidget: [],
                        )));
          }),
      drawer: const SideBar(),
      body: ListView.separated(
          separatorBuilder: (BuildContext context, int index) => const Divider(
                color: Colors.grey,
                thickness: 1,
              ),
          itemCount: contragentProvider.list.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                // contragentProvider.addContragentController(
                //     TextEditingController(
                //         text: contragentProvider.list[index].naimenovanie));
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10, top: 10),
                    child: Column(
                      children: [
                        Text(contragentProvider.list[index].naimenovanie!),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10, top: 10),
                    child: Text(contragentProvider.list[index].status!),
                  )
                ],
              ),
            );
          }),
    );
  }
}
