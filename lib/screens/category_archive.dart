import 'package:flutter/material.dart';
import 'package:my_store/screens/catergory_creator_screen.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';
import '../providers/category_provider.dart';

class CategoryArchive extends StatelessWidget {
  const CategoryArchive({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var categoryProvider = Provider.of<CategoryProvider>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 0,
        title: const Text("Архив категорий"),
      ),
      body: categoryProvider.getCategoryArchive().isEmpty
          ? const Text("")
          : Padding(
              padding: const EdgeInsets.only(top: 15),
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(
                  color: Colors.black,
                  thickness: 1,
                ),
                itemCount: categoryProvider.getCategoryArchive().length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CategoryCreatorScreen(
                                    groupController: null,
                                    dropDownButtonList: {},
                                    category: categoryProvider
                                        .getCategoryArchive()[index],
                                    fromTovarPage: false,
                                  )));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '1')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.grey),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '2')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.red),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '3')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.pink),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '4')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.orange),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '5')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.greenAccent),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '6')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.green),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '7')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.blue),
                                      width: 100,
                                      height: 50,
                                    ),
                                  if (categoryProvider
                                          .getCategoryArchive()[index]
                                          .picture ==
                                      '8')
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.purple),
                                      width: 100,
                                      height: 50,
                                    ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(categoryProvider
                                          .getCategoryArchive()[index]
                                          .name!),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Text(categoryProvider
                                          .getCategoryArchive()[index]
                                          .createdAt
                                          .toString()),
                                    ],
                                  ),
                                ],
                              ),
                              IconButton(
                                  onPressed: () {
                                    ProductMakerDb.deleteCategory(
                                        categoryProvider
                                            .getCategoryArchive()[index]
                                            .id!);
                                    categoryProvider.deleteOneCategory(
                                        categoryProvider
                                            .getCategoryArchive()[index]
                                            .id!);
                                  },
                                  icon: const Icon(
                                    Icons.delete,
                                    color: Colors.blueGrey,
                                  ))
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
    );
  }
}
