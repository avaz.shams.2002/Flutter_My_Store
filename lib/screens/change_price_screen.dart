import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/discount_maker_model.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/providers/discount_provider.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:provider/provider.dart';

class ChangePriceScreen extends StatefulWidget {
  ProductMaker? productMaker;
  List<Discount>? discount;

  ChangePriceScreen({
    Key? key,
    this.productMaker,
    this.discount,
  }) : super(key: key);

  @override
  State<ChangePriceScreen> createState() => _ChangePriceScreenState();
}

class _ChangePriceScreenState extends State<ChangePriceScreen> {
  int counter = 0;
  int upakovkaProductMaker = 0;
  int shtuckCounter = 0;
  double totalDiscount = 0.0;
  int typeOfDiscount = 0;
  late TextEditingController priceController = TextEditingController(
      text: widget.productMaker!.prodazhiPrice.toString());

  late TextEditingController quantityController = TextEditingController(
      text: widget.productMaker!.quantity.toInt().toString());

  late TextEditingController shtukController = TextEditingController(text: '');

  List<bool> listOfBool = [];

  List<Discount> listOfDiscount = [];

  List<Color> listOfColors = [];

  @override
  void initState() {
    if (widget.discount != null) {
      listOfBool = List<bool>.filled(widget.discount!.length, false);
      listOfColors =
          List<Color>.filled(widget.discount!.length, Colors.grey[300]!);
    }
    if (widget.productMaker!.discount != null) {
      listOfDiscount.add(widget.productMaker!.discount!);
      for (int i = 0; i < widget.discount!.length; i++) {
        if (widget.discount![i].id == widget.productMaker!.discount!.id!) {
          listOfBool[i] = true;
          listOfColors[i] = Colors.orange;
        }
      }
    }

    var rounded = widget.productMaker!.quantity;
    var ostatok = (((rounded - rounded.toInt()) * 100).round() / 100);
    ostatok = (ostatok * int.parse(widget.productMaker!.kolvoUpakovka!))
        .roundToDouble();
    counter = widget.productMaker!.quantity.toInt();
    upakovkaProductMaker = int.parse(widget.productMaker!.kolvoUpakovka!);
    shtuckCounter = ostatok.toInt();
    shtukController.text = shtuckCounter.toString();
  }

  @override
  Widget build(BuildContext context) {
    var productProvider = Provider.of<ProductMakerProvider>(context);
    var discountProvider = Provider.of<DiscountProvier>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
            Future.delayed(Duration(milliseconds: 1), () {
              SystemChannels.textInput.invokeMethod('TextInput.hide');
            });
          },
          icon: Icon(Icons.arrow_back),
        ),
        actions: [
          TextButton(
              onPressed: () {
                save(productProvider);
              },
              child: const Text(
                "СОХРАНИТЬ",
                style: TextStyle(color: Colors.black, fontSize: 16),
              )),
          TextButton(
              onPressed: () {
                print(listOfDiscount.length);
                if (listOfDiscount.isNotEmpty) {
                  print(listOfDiscount.first.value);
                }
              },
              child: const Text(
                "CHECK",
                style: TextStyle(color: Colors.black),
              ))
        ],
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Row(
          children: [
            Expanded(
              child: Text(
                widget.productMaker!.naimenovanie!,
                style: const TextStyle(color: Colors.black),
              ),
            )
          ],
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus!.unfocus();
        },
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.productMaker!.naimenovanie!,
                    style: const TextStyle(color: Colors.black, fontSize: 18),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "В упаковке: ${widget.productMaker!.kolvoUpakovka}",
                        style: const TextStyle(fontSize: 18),
                      ),
                      const Text(
                        "шт",
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Text(
                    "Цена",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    controller: priceController,
                    onChanged: (val) {
                      setState(() {});
                    },
                    onTap: () {
                      setState(() {
                        priceController.text = '';
                      });
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  if (widget.productMaker!.discount != null)
                    Row(
                      children: [
                        const Text(
                          "Цена со скидкой: ",
                          style: TextStyle(color: Colors.blueAccent),
                        ),
                        Text(
                          widget.productMaker!
                              .totalOneProductWithDiscount()
                              .toStringAsFixed(2),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  const SizedBox(
                    height: 40,
                  ),
                  const Text(
                    "Количество упаковки",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  if (counter == 0) {
                                    return;
                                  }
                                  if (priceController.text.isEmpty) {
                                    priceController.text = '0.0';
                                  }
                                  counter--;
                                  quantityController.text = counter.toString();
                                });
                              },
                              icon: const Icon(Icons.remove)),
                          SizedBox(
                            width: 50,
                            child: TextField(
                              onTap: () {
                                setState(() {
                                  quantityController.text = ''.trim();
                                });
                              },
                              textAlign: TextAlign.center,
                              controller: quantityController,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  if (priceController.text.isEmpty) {
                                    priceController.text = '0.0';
                                  }
                                  // if (counter > upakovkaProductMaker) {
                                  //   counter = 1;
                                  //   upakovkaForController++;
                                  //   shtukController.text =
                                  //       upakovkaForController.toString();
                                  //   quantityController.text = counter.toString();
                                  //   sum = (double.parse(priceController.text) *
                                  //           int.parse(shtukController.text)) +
                                  //       counter;
                                  // }
                                  counter++;

                                  quantityController.text = counter.toString();
                                });
                              },
                              icon: const Icon(Icons.add)),
                        ],
                      ),
                      if (upakovkaProductMaker > 1)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Количество штук",
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (shtuckCounter < 1) {
                                          return;
                                        }
                                        shtuckCounter--;
                                        shtukController.text =
                                            shtuckCounter.toString();
                                      });
                                    },
                                    icon: const Icon(Icons.remove)),
                                SizedBox(
                                  width: 50,
                                  child: TextField(
                                    onChanged: (val) {
                                      shtuckCounter = int.parse(val);
                                    },
                                    onTap: () {
                                      setState(() {
                                        shtukController.text = ''.trim();
                                      });
                                    },
                                    textAlign: TextAlign.center,
                                    controller: shtukController,
                                    keyboardType: TextInputType.number,
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (shtuckCounter >=
                                            upakovkaProductMaker - 1) {
                                          shtuckCounter = -1;
                                          counter++;
                                          quantityController.text =
                                              counter.toString();
                                        }
                                        shtuckCounter++;
                                        shtukController.text =
                                            shtuckCounter.toString();
                                      });
                                    },
                                    icon: const Icon(Icons.add)),
                              ],
                            ),
                          ],
                        )
                    ],
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.green)),
                          onPressed: () async {
                            AlertDialog alertdialog = AlertDialog(
                              title: const Text("Вы уверены?"),
                              actions: [
                                TextButton(
                                    onPressed: () async {
                                      if (priceController.text.isEmpty) {
                                        priceController.text = '0.0';
                                      }

                                      setState(() {
                                        counter = 0;
                                        shtuckCounter = 0;
                                        quantityController.text =
                                            counter.toString();
                                      });
                                      deleteProductFromCart(productProvider);

                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    },
                                    child: const Text("Да")),
                                TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: const Text("Нет")),
                              ],
                            );
                            showDialog(
                                context: context,
                                builder: (context) => alertdialog);
                          },
                          child: const Text("Удалить"))
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            if (discountProvider.listDiscount.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: const [
                        Text(
                          "Скидки",
                          style: TextStyle(color: Colors.blueAccent),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            const SizedBox(
              height: 15,
            ),
            if (discountProvider.listDiscount.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisSpacing: 5,
                      crossAxisCount: 5,
                      mainAxisSpacing: 5,
                    ),
                    itemCount: discountProvider.listDiscount.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          setState(() {});
                          addDiscount(index, discountProvider);
                        },
                        child: Container(
                          width: 50,
                          height: 100,
                          color: listOfColors[index],
                          child: Center(
                            child: Text(
                              "${discountProvider.listDiscount[index].value}"
                              "${discountProvider.listDiscount[index].percentOrSumma == 1 ? "%" : "с"}",
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      );
                    }),
              )
          ],
        ),
      ),
    );
  }

  void addDiscount(index, discountProvider) {
    if (listOfBool[index] == false) {
      for (int i = 0; i < listOfColors.length; i++) {
        listOfColors[i] = Colors.grey[300]!;
      }
      for (int i = 0; i < listOfBool.length; i++) {
        listOfBool[i] = false;
      }
      listOfBool[index] = true;
      listOfColors[index] = Colors.orange;
      listOfDiscount.clear();
      listOfDiscount.add(discountProvider.listDiscount[index]);
    } else {
      listOfColors[index] = Colors.grey[300]!;
      listOfBool[index] = false;
      listOfDiscount.clear();
    }
  }

  void deleteProductFromCart(productProvider) async {
    widget.productMaker!.prodazhiPrice = double.parse(priceController.text);
    widget.productMaker!.quantity = double.parse(quantityController.text);
    widget.productMaker!.discount = null;
    productProvider.notify();
    await ProductMakerDb.updateQuaintityAndPrice(widget.productMaker!);
  }

  void save(productProvider) async {
    FocusManager.instance.primaryFocus!.unfocus();
    if (priceController.text.isEmpty) {
      priceController.text = widget.productMaker!.prodazhiPrice!.toString();
    }
    if (quantityController.text.isEmpty) {
      quantityController.text =
          widget.productMaker!.quantity.toInt().toString();
    }
    counter = int.parse(quantityController.text.trim());

    AlertDialog newAlert =
        AlertDialog(title: const Text("Вы уверены ?"), actions: [
      TextButton(
        onPressed: () async {
          if (priceController.text.isEmpty) {
            priceController.text = '0.0';
          }
          if (quantityController.text.isEmpty) {
            quantityController.text = '0.0';
          }
          if (shtukController.text.isEmpty) {
            shtukController.text = '0';
          }
          deleteProductFromCart(productProvider);
          Navigator.pop(context);
          Navigator.pop(context);
        },
        child: const Text("Да"),
      ),
      TextButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: const Text("Нет"),
      ),
    ]);

    if (counter == 0 && shtuckCounter == 0) {
      showDialog(context: context, builder: (context) => newAlert);
      return;
    }

    if (shtuckCounter >= int.parse(widget.productMaker!.kolvoUpakovka!)) {
      widget.productMaker!.prodazhiPrice = double.parse(priceController.text);
      widget.productMaker!.quantity = double.parse(
          ((int.parse(quantityController.text) *
                          int.parse(widget.productMaker!.kolvoUpakovka!) +
                      int.parse(shtukController.text)) /
                  int.parse(widget.productMaker!.kolvoUpakovka!))
              .toStringAsFixed(2));
      widget.productMaker!.discount = null;
      if (listOfDiscount.isNotEmpty) {
        widget.productMaker!.discount = listOfDiscount.first;
      }
      productProvider.notify();
      await ProductMakerDb.updateQuaintityAndPrice(widget.productMaker!);
      Navigator.pop(context);
      return;
    }
    widget.productMaker!.prodazhiPrice = double.parse(priceController.text);
    widget.productMaker!
        .quantity = double.parse((int.parse(quantityController.text) +
            int.parse(
                    shtukController.text.isEmpty ? '0' : shtukController.text) /
                int.parse(widget.productMaker!.kolvoUpakovka!))
        .toStringAsFixed(2));
    widget.productMaker!.discount = null;
    if (listOfDiscount.isNotEmpty) {
      widget.productMaker!.discount = listOfDiscount.first;
    }
    productProvider.notify();
    await ProductMakerDb.updateQuaintityAndPrice(widget.productMaker!);

    print("Total discount: ${totalDiscount}");
    Navigator.pop(context);
  }
}
