import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/supplier_invoice_details_model.dart';
import '../providers/product_maker_provider.dart';

class ChangePriceSupplierScreen extends StatefulWidget {
  SupplierInvoiceDetails? supplierInvoiceDetails;
  dynamic refreshParent;

  ChangePriceSupplierScreen(
      {Key? key, this.supplierInvoiceDetails, this.refreshParent})
      : super(key: key);

  @override
  State<ChangePriceSupplierScreen> createState() =>
      _ChangePriceSupplierScreenState();
}

class _ChangePriceSupplierScreenState extends State<ChangePriceSupplierScreen> {
  double sum = 0.0;
  int counter = 0;
  int upakovkaProductMaker = 0;
  int shtuckCounter = 0;

  late TextEditingController priceController = TextEditingController(
      text: widget.supplierInvoiceDetails!.price.toString());

  late TextEditingController quantityController = TextEditingController(
      text: widget.supplierInvoiceDetails!.quantity!.toInt().toString());

  late TextEditingController shtukController = TextEditingController(text: '');

  @override
  void initState() {
    var rounded = widget.supplierInvoiceDetails!.quantity;
    var ostatok = (((rounded! - rounded.toInt()) * 100).round() / 100);

    ostatok = (ostatok * widget.supplierInvoiceDetails!.kolvoUpakovka!)
        .roundToDouble();

    sum = (widget.supplierInvoiceDetails!.price! * rounded.toInt()) +
        ((ostatok * widget.supplierInvoiceDetails!.price!) /
            widget.supplierInvoiceDetails!.kolvoUpakovka!);

    counter = widget.supplierInvoiceDetails!.quantity!.toInt();
    upakovkaProductMaker = widget.supplierInvoiceDetails!.kolvoUpakovka!;
    shtuckCounter = ostatok.toInt();
    shtukController.text = shtuckCounter.toString();
    print(upakovkaProductMaker);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: [
          TextButton(
              onPressed: () async {
                setState(() {});
                FocusManager.instance.primaryFocus!.unfocus();
                if (priceController.text.isEmpty) {
                  priceController.text =
                      widget.supplierInvoiceDetails!.price!.toString();
                }
                if (quantityController.text.isEmpty) {
                  quantityController.text = widget
                      .supplierInvoiceDetails!.quantity!
                      .toInt()
                      .toString();
                }
                // if (shtukController.text.isEmpty) {
                //   shtukController.text =
                //       widget.productMaker!.kolvoUpaVibr!.toString();
                // }

                counter = int.parse(quantityController.text.trim());

                AlertDialog newAlert =
                    AlertDialog(title: const Text("Вы уверены ?"), actions: [
                  TextButton(
                    onPressed: () async {
                      if (priceController.text.isEmpty) {
                        priceController.text = '0.0';
                      }
                      if (quantityController.text.isEmpty) {
                        quantityController.text = '0.0';
                      }
                      if (shtukController.text.isEmpty) {
                        shtukController.text = '0';
                      }
                      widget.supplierInvoiceDetails!.price =
                          double.parse(priceController.text);
                      widget.supplierInvoiceDetails!.quantity =
                          double.parse(quantityController.text);

                      widget.refreshParent(
                          widget.supplierInvoiceDetails!.quantity,
                          widget.supplierInvoiceDetails!);

                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: const Text("Да"),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text("Нет"),
                  ),
                ]);

                if (counter == 0 && shtuckCounter == 0) {
                  showDialog(context: context, builder: (context) => newAlert);
                  return;
                }

                if (shtuckCounter >=
                    widget.supplierInvoiceDetails!.kolvoUpakovka!) {
                  widget.supplierInvoiceDetails!.price =
                      double.parse(priceController.text);
                  widget.supplierInvoiceDetails!.quantity = double.parse(
                      ((int.parse(quantityController.text) *
                                      widget.supplierInvoiceDetails!
                                          .kolvoUpakovka! +
                                  int.parse(shtukController.text)) /
                              widget.supplierInvoiceDetails!.kolvoUpakovka!)
                          .toStringAsFixed(2));
                  widget.refreshParent(widget.supplierInvoiceDetails!.quantity,
                      widget.supplierInvoiceDetails!);
                  Navigator.pop(context);
                  return;
                }
                widget.supplierInvoiceDetails!.price =
                    double.parse(priceController.text);

                // var rounded = ((element.quantity * 10).round()) / 10;
                // print("Rounded : ${rounded}" );
                //
                // var ostatok =
                // (double.parse((rounded - rounded.toInt()).toStringAsFixed(1)) * 16)
                //     .toInt();
                // print("Ostatok : $ostatok");

                widget.supplierInvoiceDetails!.quantity = double.parse(
                    (int.parse(quantityController.text) +
                            int.parse(shtukController.text.isEmpty
                                    ? '0'
                                    : shtukController.text) /
                                widget.supplierInvoiceDetails!.kolvoUpakovka!)
                        .toStringAsFixed(2));

                widget.refreshParent(widget.supplierInvoiceDetails!.quantity,
                    widget.supplierInvoiceDetails!);

                Navigator.pop(context);
              },
              child: const Text(
                "СОХРАНИТЬ",
                style: TextStyle(color: Colors.black, fontSize: 16),
              ))
        ],
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Row(
          children: [
            Expanded(
              child: Text(
                sum.toStringAsFixed(2),
                style: const TextStyle(color: Colors.black),
              ),
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.supplierInvoiceDetails!.name!,
              style: const TextStyle(color: Colors.black, fontSize: 18),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "В упаковке: ${widget.supplierInvoiceDetails!.kolvoUpakovka}",
                  style: const TextStyle(fontSize: 18),
                ),
                const Text(
                  "шт",
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              "Цена закупки",
              style: TextStyle(color: Colors.blueAccent),
            ),
            const SizedBox(
              height: 10,
            ),
            TextField(
              keyboardType: TextInputType.number,
              controller: priceController,
              onTap: () {
                setState(() {
                  priceController.text = '';
                });
              },
            ),
            const SizedBox(
              height: 40,
            ),
            const Text(
              "Количество упаковки",
              style: TextStyle(color: Colors.blueAccent),
            ),
            const SizedBox(
              height: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            if (counter == 0) {
                              return;
                            }
                            if (priceController.text.isEmpty) {
                              priceController.text = '0.0';
                            }
                            counter--;
                            quantityController.text =
                                counter.toInt().toString();
                            sum = (double.parse(priceController.text) *
                                    int.parse(quantityController.text)) +
                                ((shtuckCounter *
                                        double.parse(priceController.text)) /
                                    upakovkaProductMaker);
                          });
                        },
                        icon: const Icon(Icons.remove)),
                    SizedBox(
                      width: 50,
                      child: TextField(
                        onTap: () {
                          setState(() {
                            quantityController.text = ''.trim();
                          });
                        },
                        textAlign: TextAlign.center,
                        controller: quantityController,
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            if (priceController.text.isEmpty) {
                              priceController.text = '0.0';
                            }
                            // if (counter > upakovkaProductMaker) {
                            //   counter = 1;
                            //   upakovkaForController++;
                            //   shtukController.text =
                            //       upakovkaForController.toString();
                            //   quantityController.text = counter.toString();
                            //   sum = (double.parse(priceController.text) *
                            //           int.parse(shtukController.text)) +
                            //       counter;
                            // }
                            counter++;

                            quantityController.text =
                                counter.toInt().toString();
                            sum = (double.parse(priceController.text) *
                                    int.parse(quantityController.text)) +
                                ((shtuckCounter *
                                        double.parse(priceController.text)) /
                                    upakovkaProductMaker);
                          });
                        },
                        icon: const Icon(Icons.add)),
                  ],
                ),
                if (upakovkaProductMaker > 1)
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Количество штук",
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  if (shtuckCounter < 1) {
                                    return;
                                  }
                                  shtuckCounter--;
                                  shtukController.text =
                                      shtuckCounter.toString();
                                  sum = (double.parse(priceController.text) *
                                          int.parse(quantityController.text)) +
                                      ((shtuckCounter *
                                              double.parse(
                                                  priceController.text)) /
                                          upakovkaProductMaker);
                                });
                              },
                              icon: const Icon(Icons.remove)),
                          SizedBox(
                            width: 50,
                            child: TextField(
                              onChanged: (val) {
                                shtuckCounter = int.parse(val);
                              },
                              onTap: () {
                                setState(() {
                                  shtukController.text = ''.trim();
                                });
                              },
                              textAlign: TextAlign.center,
                              controller: shtukController,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  if (shtuckCounter >=
                                      upakovkaProductMaker - 1) {
                                    shtuckCounter = -1;
                                    counter++;
                                    quantityController.text =
                                        counter.toString();
                                    sum = (double.parse(priceController.text) *
                                        int.parse(quantityController.text));
                                  }
                                  shtuckCounter++;
                                  shtukController.text =
                                      shtuckCounter.toString();
                                  sum = (double.parse(priceController.text) *
                                          int.parse(quantityController.text)) +
                                      ((shtuckCounter *
                                              double.parse(
                                                  priceController.text)) /
                                          upakovkaProductMaker);
                                });
                              },
                              icon: const Icon(Icons.add)),
                        ],
                      ),
                    ],
                  )
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.green)),
                    onPressed: () async {
                      AlertDialog alertdialog = AlertDialog(
                        title: const Text("Вы уверены?"),
                        actions: [
                          TextButton(
                              onPressed: () async {
                                if (priceController.text.isEmpty) {
                                  priceController.text = '0.0';
                                }

                                setState(() {
                                  counter = 0;
                                  shtuckCounter = 0;
                                  quantityController.text = counter.toString();
                                  sum = double.parse(priceController.text) *
                                      int.parse(quantityController.text);
                                  shtukController.text =
                                      shtuckCounter.toString();
                                });
                                widget.supplierInvoiceDetails!.price =
                                    double.parse(priceController.text);
                                widget.supplierInvoiceDetails!.quantity =
                                    double.parse(quantityController.text);

                                widget.refreshParent(
                                    widget.supplierInvoiceDetails!.quantity,
                                    widget.supplierInvoiceDetails!);

                                Navigator.pop(context);
                                Navigator.pop(context);
                              },
                              child: const Text("Да")),
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text("Нет")),
                        ],
                      );
                      showDialog(
                          context: context, builder: (context) => alertdialog);
                    },
                    child: const Text("Удалить"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
