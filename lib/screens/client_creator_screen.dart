import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/client_model.dart';
import 'package:my_store/providers/client_provider.dart';
import 'package:provider/provider.dart';

class ClientCreatorScreen extends StatefulWidget {
  Client? client;
  dynamic refreshParent;
  dynamic refreshClientPage;

  ClientCreatorScreen({Key? key, this.client, this.refreshParent, this.refreshClientPage})
      : super(key: key);

  @override
  State<ClientCreatorScreen> createState() => _ClientCreatorScreenState();
}

class _ClientCreatorScreenState extends State<ClientCreatorScreen> {
  var _formKey = GlobalKey<FormState>();

  late TextEditingController _clientNameController =
      TextEditingController(text: '');
  late TextEditingController _clientEmailController =
      TextEditingController(text: '');
  late TextEditingController _clientPhoneNumberController =
      TextEditingController(text: '');
  late TextEditingController _clientAddressController =
      TextEditingController(text: '');
  late TextEditingController _clientCityController =
      TextEditingController(text: '');
  late TextEditingController _clientRegionController =
      TextEditingController(text: '');
  late TextEditingController _clientMailIndexController =
      TextEditingController(text: '');
  late TextEditingController _clientCountryController =
      TextEditingController(text: '');
  late TextEditingController _clientCodeController =
      TextEditingController(text: '');
  late TextEditingController _clientNoteontroller =
      TextEditingController(text: '');

  @override
  void initState() {
    if (widget.client != null) {
      _clientNameController.text = widget.client!.name!;
      _clientEmailController.text = widget.client!.email!;
      _clientPhoneNumberController.text = widget.client!.phoneNumber!;
      _clientAddressController.text = widget.client!.address!;
      _clientCityController.text = widget.client!.city!;
      _clientRegionController.text = widget.client!.region!;
      _clientMailIndexController.text = widget.client!.mailIndex!;
      _clientCountryController.text = widget.client!.country!;
      _clientCodeController.text = widget.client!.clientCode!;
      _clientNoteontroller.text = widget.client!.clientNote!;
    }
  }

  @override
  Widget build(BuildContext context) {
    var clientProvider = Provider.of<ClientProvider>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: const Text(
          "Создание клиента",
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          TextButton(
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                ///
                /// if client is null we create new client
                ///
                widget.client ??= Client(
                    name: _clientNameController.text,
                    email: _clientEmailController.text,
                    phoneNumber: _clientPhoneNumberController.text,
                    address: _clientAddressController.text,
                    city: _clientCityController.text,
                    region: _clientRegionController.text,
                    mailIndex: _clientMailIndexController.text,
                    country: _clientCountryController.text,
                    clientCode: _clientCodeController.text,
                    clientNote: _clientNoteontroller.text,
                    lastVisit: "",
                    numberOfVisits: 0);

                if (!clientProvider.checkClientInList(widget.client!)) {
                  await ProductMakerDb.insertClient(widget.client!);
                  clientProvider.addClient(widget.client!);
                  FocusManager.instance.primaryFocus!.unfocus();
                  widget.refreshClientPage(0);
                  Navigator.pop(context);
                  return;
                }

                ///
                /// else we just update our client
                ///

                widget.client!.name = _clientNameController.text;
                widget.client!.email = _clientEmailController.text;
                widget.client!.phoneNumber = _clientPhoneNumberController.text;
                widget.client!.address = _clientAddressController.text;
                widget.client!.city = _clientCityController.text;
                widget.client!.region = _clientRegionController.text;
                widget.client!.mailIndex = _clientMailIndexController.text;
                widget.client!.country = _clientCountryController.text;
                widget.client!.clientCode = _clientCodeController.text;
                widget.client!.clientNote = _clientNoteontroller.text;
                await ProductMakerDb.updateClient(widget.client!);
                widget.refreshParent(widget.client!);
                clientProvider.notify();
                Navigator.pop(context);
              },
              child: const Text(
                "СОХРАНИТЬ",
                style: TextStyle(color: Colors.black, fontSize: 16),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Form(
          key: _formKey,
          child: ListView(
            children: [
              Row(
                children: [
                  const Icon(Icons.person),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextFormField(
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Поле не может быть пустым";
                        }
                      },
                      decoration: const InputDecoration(
                          labelText: "Имя",
                          labelStyle: const TextStyle(color: Colors.grey)),
                      controller: _clientNameController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Icon(Icons.email),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientEmailController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Icon(Icons.phone),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Телефон",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientPhoneNumberController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Icon(Icons.location_on),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Адрес",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientAddressController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Icon(Icons.location_city),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Город",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientCityController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 40,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Регион",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientRegionController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 40,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Почтовой индекс",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientMailIndexController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 40,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Страна",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientCountryController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Icon(Icons.code),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Код клиента ",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientCodeController,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Icon(Icons.message),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          labelText: "Заметка",
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: _clientNoteontroller,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
