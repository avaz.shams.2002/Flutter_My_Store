import 'package:flutter/material.dart';
import 'package:my_store/providers/contragent_provider.dart';
import 'package:provider/provider.dart';

import '../pages/supplier_page.dart';
import '../pages/supplier_creater_page.dart';
import '../providers/contact_person_provider.dart';

class ContragentArchiv extends StatelessWidget {
  const ContragentArchiv({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var categoryArchiveProvider = Provider.of<ContragentProvider>(context);
    var check = Provider.of<ContactPersonProvider>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Архив поставщиков"),
        backgroundColor: Colors.blueAccent,
      ),
      body: ListView.separated(
          separatorBuilder: (BuildContext context, int index) => const Divider(
                color: Colors.grey,
                thickness: 1,
              ),
          itemCount: categoryArchiveProvider.listOfArchiveContragent().length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => SupplierPage(
                //               contragent: categoryArchiveProvider
                //                   .listOfArchiveContragent()[index],
                //             )));
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Text(
                          categoryArchiveProvider
                              .listOfArchiveContragent()[index]
                              .naimenovanie!,
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: IconButton(
                            onPressed: () {
                              var get = check.getContactFace(
                                  categoryArchiveProvider
                                      .listOfArchiveContragent()[index]
                                      .id!);

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SupplierCreatorPage(
                                            contragent: categoryArchiveProvider
                                                    .listOfArchiveContragent()[
                                                index],
                                            contactWidget: get,
                                          )));
                            },
                            icon: const Icon(Icons.info_outline)))
                  ],
                ),
              ),
            );
          }),
    );
  }
}
