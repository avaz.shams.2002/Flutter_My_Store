import 'package:flutter/material.dart';
import 'package:my_store/models/contragent_maker_model.dart';
import 'package:my_store/screens/counterparty_screen.dart';

class Postavshik extends StatelessWidget {
  const Postavshik({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Поставщик",
            style: TextStyle(color: Colors.grey),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: const IconThemeData(color: Colors.black),
          bottom: const TabBar(
              // isScrollable: true,
              tabs: [
                Tab(
                  child: Text("КОНТРАГЕНТЫ"),
                ),
                Tab(
                  child: Text("ЮРЛИЦА"),
                )
              ]),
        ),
        body:  TabBarView(children: [
          CounterPartyScreen(contragent: Contragent(),),
          const Center(
            child: Text("Hello"),
          )
        ]),
      ),
    );
  }
}
