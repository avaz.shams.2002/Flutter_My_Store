import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/pages/oplata_page.dart';
import 'package:my_store/screens/change_price_screen.dart';
import 'package:provider/provider.dart';

import '../providers/discount_provider.dart';
import '../providers/product_maker_provider.dart';

class CheckScreen extends StatefulWidget {
  const CheckScreen({Key? key}) : super(key: key);

  @override
  State<CheckScreen> createState() => _CheckScreenState();
}

class _CheckScreenState extends State<CheckScreen> {
  int changeForDelete = 0;

  @override
  Widget build(BuildContext context) {
    var productMaker = Provider.of<ProductMakerProvider>(context);
    var discountProvider = Provider.of<DiscountProvier>(context);
    List<Widget> buttonValues = [
      Row(
        children: const [
          Icon(Icons.delete, color: Colors.grey),
          SizedBox(
            width: 10,
          ),
          Text("Очистить чек"),
        ],
      ),
      Row(
        children: const [
          Icon(Icons.sync, color: Colors.grey),
          SizedBox(
            width: 10,
          ),
          Text("Синхронизировать"),
        ],
      ),
    ];
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor:
            changeForDelete == 1 ? Colors.white : Colors.blueAccent,
        elevation: 0,
        leading: IconButton(
            onPressed: () {
              Future.delayed(Duration(milliseconds: 5), () {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
              });
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back)),
        iconTheme: IconThemeData(
            color: changeForDelete == 1 ? Colors.black : Colors.white),
        actions: [
          Row(
            children: [
              const SizedBox(
                width: 60,
              ),
              InkWell(
                onTap: () {
                  Future.delayed(Duration(milliseconds: 5), () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                  });
                  Navigator.pop(context);
                },
                child: Badge(
                  animationType: BadgeAnimationType.scale,
                  showBadge:
                      productMaker.getWhichisAdded().isEmpty ? false : true,
                  badgeContent: Row(
                    children: [
                      if (productMaker.getWhichisAdded().isNotEmpty)
                        Text(
                          "${productMaker.getWhichisAdded().length}",
                          style: TextStyle(
                              color: changeForDelete == 1
                                  ? Colors.white
                                  : Colors.black),
                        ),
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Text(
                      "Чек",
                      style: TextStyle(
                          color: changeForDelete == 1
                              ? Colors.black
                              : Colors.white,
                          fontSize: 20),
                    ),
                  ),
                  badgeColor:
                      changeForDelete == 1 ? Colors.black : Colors.white,
                  toAnimate: true,
                  shape: BadgeShape.circle,
                ),
              ),
            ],
          ),
          const Expanded(child: Text("")),
          if (changeForDelete == 0)
            Row(
              children: [
                DropdownButtonHideUnderline(
                  child: DropdownButton(
                      icon: const Icon(
                        Icons.more_vert,
                        color: Colors.white,
                      ),
                      items: buttonValues
                          .map((e) => DropdownMenuItem(
                                child: e,
                                value: e,
                              ))
                          .toList(),
                      onChanged: (val) async {
                        if (val == buttonValues[0]) {
                          await ProductMakerDb.setZeroQuantity(0, 0.0);
                          productMaker.setZeroCart();
                        }
                        if (val == buttonValues[1]) {
                          print("Clicked sync");
                        }
                      }),
                ),
              ],
            )
          else
            IconButton(
                onPressed: () {
                  setState(() {});
                  changeForDelete = 0;
                },
                icon: const Icon(
                  Icons.close,
                  color: Colors.black,
                ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: productMaker.getWhichisAdded().length,
                  itemBuilder: (context, index) {
                    var rounded =
                        ((productMaker.getNotArchiveTovar()[index].quantity));

                    var sumOfQuantityofShtuk =
                        (((rounded - rounded.toInt()) * 100).round() / 100);

                    sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                            int.parse(productMaker
                                .getNotArchiveTovar()[index]
                                .kolvoUpakovka!))
                        .roundToDouble();

                    return InkWell(
                      onLongPress: () {
                        setState(() {});
                        changeForDelete = 1;
                      },
                      onTap: () {
                        if (changeForDelete == 0) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChangePriceScreen(
                                        productMaker: productMaker
                                            .getWhichisAdded()[index],
                                        discount: discountProvider.listDiscount,
                                      )));
                        }
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    productMaker
                                        .getWhichisAdded()[index]
                                        .naimenovanie!,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                      "${productMaker.getWhichisAdded()[index].prodazhiPrice}"),
                                  Text(
                                    productMaker
                                        .getWhichisAdded()[index]
                                        .prodazhiCurrency!
                                        .toLowerCase(),
                                    style: const TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  )
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  if (changeForDelete == 0)
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          productMaker
                                              .getWhichisAdded()[index]
                                              .getTotalOfProduct()
                                              .toStringAsFixed(2),
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17),
                                        ),
                                        if (productMaker
                                                .getWhichisAdded()[index]
                                                .discount !=
                                            null)
                                          Row(
                                            children: [
                                              if (productMaker
                                                      .getWhichisAdded()[index]
                                                      .discount!
                                                      .percentOrSumma ==
                                                  1)
                                                Text(
                                                  productMaker
                                                              .productsmaker[
                                                                  index]
                                                              .discount!
                                                              .value! ==
                                                          0.0
                                                      ? ""
                                                      : "-${productMaker.productsmaker[index].discount!.value!}%",
                                                  style: const TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 12),
                                                ),
                                              if (productMaker
                                                      .getWhichisAdded()[index]
                                                      .discount!
                                                      .percentOrSumma ==
                                                  2)
                                                Text(
                                                  "-${((productMaker.productsmaker[index].discount!.value! * productMaker.productsmaker[index].prodazhiPrice!) / 100) == 0.0 ? ""
                                                      "" : ((productMaker.productsmaker[index].discount!.value! * productMaker.productsmaker[index].prodazhiPrice!) / 100).toStringAsFixed(2)}с",
                                                  style: const TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 12),
                                                ),
                                            ],
                                          )
                                      ],
                                    ),
                                  if (changeForDelete == 1)
                                    IconButton(
                                        onPressed: () async {
                                          await ProductMakerDb
                                              .deleteDiscountDetailById(
                                                  productMaker
                                                      .getWhichisAdded()[index]
                                                      .id!);
                                          await ProductMakerDb
                                              .setZeroForOneProduct(productMaker
                                                  .getWhichisAdded()[index]);

                                          productMaker.notify();
                                        },
                                        icon: Icon(Icons.delete))
                                ],
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "(${productMaker.getWhichisAdded()[index].quantity.toInt().toString()}-уп",
                                style: const TextStyle(
                                    color: Colors.grey, fontSize: 14),
                              ),
                              if (sumOfQuantityofShtuk != 0)
                                Text(
                                  ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                  style: const TextStyle(
                                      color: Colors.grey, fontSize: 14),
                                ),
                              const Text(
                                ")",
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 17),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                        ],
                      ),
                    );
                  }),
            ),
            const Divider(
              color: Colors.grey,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Итого",
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    productMaker.allPrice().toStringAsFixed(2),
                    style: const TextStyle(fontSize: 18),
                  )
                ],
              ),
            ),
            InkWell(
              onTap: () {
                if (productMaker.allPrice() == 0.0) {
                  return;
                }
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => OplataPage(
                              allPrice:
                                  productMaker.allPrice().toStringAsFixed(2),
                              listProductMaker: productMaker.getWhichisAdded(),
                            )));
              },
              child: Container(
                color: Colors.green,
                height: 70,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "ОПЛАТИТЬ",
                        style: TextStyle(
                            color: productMaker.allPrice() == 0
                                ? Colors.white70
                                : Colors.white,
                            fontSize: 16),
                      ),
                      Text(
                        productMaker.allPrice().toStringAsFixed(2),
                        style: TextStyle(
                            color: productMaker.allPrice() == 0
                                ? Colors.white70
                                : Colors.white,
                            fontSize: 16),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
