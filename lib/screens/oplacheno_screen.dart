import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/pages/sales_page.dart';
import 'package:my_store/providers/client_provider.dart';
import 'package:provider/provider.dart';

import '../providers/product_maker_provider.dart';

class OplachenoScrenn extends StatelessWidget {
  double? allPrice;
  double? sdachi;

  OplachenoScrenn({Key? key, this.allPrice, this.sdachi}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var productMaker = Provider.of<ProductMakerProvider>(context);
    var clientProvider = Provider.of<ClientProvider>(context);
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(leading: const Text("")),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Text(
                        allPrice!.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 30),
                      ),
                      const Text(
                        "Оплачено",
                        style: const TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        sdachi!.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 30),
                      ),
                      const Text(
                        "Сдача",
                        style: const TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Expanded(child: const Text("")),
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: ElevatedButton(
                  onPressed: () async {
                    await ProductMakerDb.setZeroQuantity(0, 0.0);
                    productMaker.setZeroCart();
                    clientProvider.clearList();
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => SalesPage()));
                  },
                  child: const Text("НОВАЯ ПРОДАЖА")),
            )
          ],
        ),
      ),
    );
  }
}
