import 'package:flutter/material.dart';
import 'package:my_store/pages/create_product_page.dart';
import 'package:my_store/providers/counries_from_api_provider.dart';
import 'package:my_store/providers/user_check.dart';
import 'package:provider/provider.dart';

class Countries extends StatefulWidget {
  Countries({Key? key}) : super(key: key);

  @override
  State<Countries> createState() => _CountriesState();
}

class _CountriesState extends State<Countries> {
  @override
  Widget build(BuildContext context) {
    var setCountry = Provider.of<CheckUser>(context);
    var getCountryApi = Provider.of<CountriesFromApiProvider>(context);
    return Scaffold(
      appBar: AppBar(),
      body: getCountryApi.names.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircularProgressIndicator(
                    color: Colors.blueAccent,
                  ),
                  Text("Loading...")
                ],
              ),
            )
          : Scrollbar(
              child: ListView.separated(
                itemCount: getCountryApi.names.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {
                        setCountry.setCountry(TextEditingController(
                            text: getCountryApi.names[index]));
                        Navigator.pop(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CreateProductPage()));
                      },
                      child: Text(
                        getCountryApi.names[index],
                        style:
                            const TextStyle(color: Colors.black, fontSize: 20),
                      ));
                },
              ),
            ),
    );
  }
}
