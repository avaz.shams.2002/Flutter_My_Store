import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:my_store/models/discount_maker_model.dart';

class ProductMaker {
  int? id;
  File? image;
  int? archiv = 0;
  String? naimenovanie = '';
  String? groupe = '';
  String? code = '';
  String? artikul = '';
  double? minimalPrice = 0.00;
  String? minimalCurrency = '';
  double? zakupkiPrice = 0.00;
  String? zakupkiCurrency = '';
  double? prodazhiPrice = 0.00;
  String? prodazhiCurrency = '';
  String? opisanie = '';
  String? strana = '';
  String? ves = '';
  String? obyom = '';
  String? edIzmereniy = '';
  String? nds = '';
  String? neSnizhemiyOstatok = '';
  String? postavshik = '';
  int? postavshikId;
  String? shtrihkod = '';
  int? uchetPoSerNomeram = 0;
  int? vesTovar = 0;
  int? alhogolProduct = 0;
  int? aksizMark = 0;
  String? kodVidaProduct = '';
  String? emkost = '';
  String? krepost = '';
  String? upakovka = '';
  String? kolvoUpakovka = '';
  String? vladelesSot = '';
  String? vladalesOtdel = '';
  double quantity = 0;
  String? categoryId = '';
  Discount? discount;
  int? receiptId;

  ProductMaker({
    this.id,
    this.image,
    this.archiv = 0,
    this.naimenovanie = '',
    this.groupe = '',
    this.code = '',
    this.artikul = '',
    this.minimalPrice = 0.00,
    this.minimalCurrency = '',
    this.zakupkiPrice = 0.00,
    this.zakupkiCurrency = '',
    this.prodazhiPrice = 0.00,
    this.prodazhiCurrency = '',
    this.opisanie = '',
    this.strana = '',
    this.ves = '',
    this.obyom = '',
    this.edIzmereniy = '',
    this.nds = '',
    this.neSnizhemiyOstatok = '',
    this.postavshik = '',
    this.postavshikId,
    this.shtrihkod = '',
    this.uchetPoSerNomeram = 0,
    this.vesTovar = 0,
    this.alhogolProduct = 0,
    this.aksizMark = 0,
    this.kodVidaProduct = '',
    this.emkost = '',
    this.krepost = '',
    this.upakovka = '',
    this.kolvoUpakovka = '',
    this.vladelesSot = '',
    this.vladalesOtdel = '',
    this.quantity = 0,
    this.categoryId = '',
    this.discount,
    this.receiptId,
  });

  factory ProductMaker.fromJson(Map<String, dynamic> json) {
    return ProductMaker(
        id: json['id'],
        image: json['image'],
        archiv: json['archiv'],
        naimenovanie: json['naimenovanie'],
        groupe: json['groupe'],
        code: json['code'],
        artikul: json['artikul'],
        minimalPrice: json['minimalPrice'],
        minimalCurrency: json['minimalCurrency'],
        zakupkiPrice: json['zakupkiPrice'],
        zakupkiCurrency: json['zakupkiCurrency'],
        prodazhiPrice: json['prodazhiPrice'],
        prodazhiCurrency: json['prodazhiCurrency'],
        opisanie: json['opisanie'],
        strana: json['strana'],
        ves: json['ves'],
        obyom: json['obyom'],
        edIzmereniy: json['edIzmereniy'],
        nds: json['nds'],
        neSnizhemiyOstatok: json['neSnizhemiyOstatok'],
        postavshik: json['postavshik'],
        shtrihkod: json['shtrihkod'],
        upakovka: json['upakovki'],
        kolvoUpakovka: json['kolvoUpakovka'],
        vladelesSot: json['vladelesSot'],
        vladalesOtdel: json['vladalesOtdel']);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'image': image.toString().replaceAll('File: ', '').replaceAll("'", ''),
      'archiv': archiv,
      'naimenovanie': naimenovanie,
      'groupe': groupe,
      'code': code,
      'artikul': artikul,
      'minimalPrice': minimalPrice,
      'minimalCurrency': minimalCurrency,
      'zakupkiPrice': zakupkiPrice,
      'zakupkiCurrency': zakupkiCurrency,
      'prodazhiPrice': prodazhiPrice,
      'prodazhiCurrency': prodazhiCurrency,
      'opisanie': opisanie,
      'strana': strana,
      'ves': ves,
      'obyom': obyom,
      'edIzmereniy': edIzmereniy,
      'nds': nds,
      'neSnizhemiyOstatok': neSnizhemiyOstatok,
      'postavshik': postavshik,
      'postavshikId': postavshikId,
      'shtrihkod': shtrihkod,
      'uchetPoSerNomeram': uchetPoSerNomeram,
      'vesTovar': vesTovar,
      'alhogolProduct': alhogolProduct,
      'aksizMark': aksizMark,
      'kodVidaProduct': kodVidaProduct,
      'emkost': emkost,
      'krepost': krepost,
      'upakovka': upakovka,
      'kolvoUpakovka': kolvoUpakovka,
      'vladelesSot': vladelesSot,
      'vladalesOtdel': vladalesOtdel,
      'discount_id': discount == null ? null : discount!.id,
      'quantity': quantity,
      'category_id': categoryId
    };
  }

  double getTotalOfProduct() {
    var ostatok = (((quantity - quantity.toInt()) * 100).round() / 100);
    ostatok = (ostatok * int.parse(kolvoUpakovka!))
        .roundToDouble();
    var sum = ((prodazhiPrice! * quantity.toInt()) +
        ((ostatok * prodazhiPrice!) /
            int.parse(kolvoUpakovka!)));
    if (discount != null) {
      return discount!.getTotalWidthTypeOfDiscount(sum);
    }
    return sum;
  }

  double totalOneProductWithDiscount()
  {
    switch(discount!.percentOrSumma)
    {
      case 1:
        return (prodazhiPrice! * (1 - discount!.value! / 100));
      default:
        return prodazhiPrice! - discount!.value!;
    }
  }
}
