class ReceiptProduct {
  int? id;
  int? receiptId;
  String? naimenovanie = '';
  double? quantity;
  int? kolvoUpakovke = 0;
  double? prodazhiPrice = 0.00;
  int? returned = 0;

  ReceiptProduct(
      {this.id,
      this.receiptId,
      this.naimenovanie,
      this.quantity,
        this.kolvoUpakovke,
      this.prodazhiPrice,
      this.returned});

  Map<String, dynamic> toMapForReceiptTable() {
    return {
      "id": id,
      "receipt_id": receiptId,
      "naimenovanie": naimenovanie,
      "quantity": quantity,
      "kolvoUpakovke":kolvoUpakovke,
      "prodazhiPrice": prodazhiPrice,
      "returned": returned
    };
  }
}
