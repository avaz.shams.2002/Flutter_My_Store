import 'dart:ui';

class Category {
  int? id;
  String? name = '';
  String? picture = '';
  int? visible = 0;
  String? createdAt;
  String? updatedAt;
  int? colorInt;
  Category(
      {this.id,
      this.name,
      this.picture,
      this.visible,
      this.createdAt,
      this.updatedAt,
      this.colorInt
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'picture': picture,
      'visible': visible,
      'created_at': createdAt,
      'updated_at': updatedAt,
      'colorInt': colorInt
    };
  }
}
