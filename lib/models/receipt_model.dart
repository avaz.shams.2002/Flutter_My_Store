import 'package:my_store/models/client_model.dart';
import 'package:my_store/models/receipt_product_model.dart';

class Receipt {
  int? id;
  String? total;
  String? cashier = '';
  String? kassa = '';
  String? cash;
  String? dateTime;
  String? changeMoney;
  Client? client;
  List<ReceiptProduct>? listReceiptProduct;
  int? clientId;

  Receipt(
      {this.id,
      this.total,
      this.cashier = '',
      this.kassa = '',
      this.cash,
      this.dateTime,
      this.changeMoney,
      this.client,
      this.listReceiptProduct,
      this.clientId,
      });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'total': total,
      'cashier': cashier,
      'kassa': kassa,
      'cash': cash,
      'dateTime': dateTime,
      'changeMoney': changeMoney,
      "client_id": client == null ? null : client!.id,
    };
  }
}
