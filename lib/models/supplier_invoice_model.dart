import 'package:my_store/pages/supplier_page.dart';

class SupplierInvoice {
  int? id;
  int? supplierId;
  String? datatime;
  double? total;
  int? quantity;
  int? returned = 0;
  double? changeMoney = 0.0;

  SupplierInvoice(
      {this.id,
      this.supplierId,
      this.datatime,
      this.total,
      this.quantity,
      this.returned,
      this.changeMoney,
      });

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "supplier_id": supplierId,
      "datatime": datatime,
      "total": total,
      "quantity": quantity,
      "returned": returned,
      "changeMoney":changeMoney,
    };
  }
}
