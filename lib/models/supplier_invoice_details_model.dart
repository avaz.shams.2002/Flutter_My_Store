import 'dart:io';

class SupplierInvoiceDetails {
  int? id;
  int? supplierInvoiceId;
  File? img;
  String? name;
  int? productId;
  double? quantity;
  double? price;
  String? barcode;
  int? kolvoUpakovka;
  int? returned = 0;

  SupplierInvoiceDetails(
      {this.id,
      this.supplierInvoiceId,
      this.img,
      this.name,
      this.productId,
      this.quantity,
      this.price,
      this.barcode,
      this.kolvoUpakovka,
      this.returned});

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "supplier_invoice_id": supplierInvoiceId,
      "img": img.toString().replaceAll('File: ', '').replaceAll("'", ''),
      "name": name,
      "product_id": productId,
      "quantity": quantity,
      "price": price,
      "barcode":barcode,
      "kolvoUpakovka": kolvoUpakovka,
      "returned": returned
    };
  }
}
