import 'package:my_store/models/receipt_model.dart';

class Client {
  int? id;
  String? name;
  String? email;
  String? phoneNumber;
  String? address;
  String? city;
  String? region;
  String? mailIndex;
  String? country;
  String? clientCode;
  String? clientNote;
  String? lastVisit;
  int? numberOfVisits;
  List<Receipt>? receipt;

  Client({
    this.id,
    this.name,
    this.email,
    this.phoneNumber,
    this.address,
    this.city,
    this.region,
    this.mailIndex,
    this.country,
    this.clientCode,
    this.clientNote,
    this.lastVisit,
    this.numberOfVisits,
    this.receipt
  });

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "name": name,
      "email": email,
      "phoneNumber": phoneNumber,
      "address": address,
      "city": city,
      "region": region,
      "mailIndex": mailIndex,
      "country": country,
      "clientCode": clientCode,
      "clientNote": clientNote,
      "lastVisit": lastVisit,
      "numberOfVisits":numberOfVisits
    };
  }
}
