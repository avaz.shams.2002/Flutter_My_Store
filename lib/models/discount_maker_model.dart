class Discount {
  int? id;
  String? name = '';
  double? value = 0.00;
  int? percentOrSumma;

  Discount({this.id, this.name, this.value, this.percentOrSumma});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'value': value,
      "percentOrSumma": percentOrSumma
    };
  }

  double getTotalWidthTypeOfDiscount(double price) {
    switch (percentOrSumma) {
      case 1:
        return price * (1 - value! / 100);
      default:
        return value! > price ? 0 : price - value!;
    }
  }
}
