import 'package:my_store/widgets/contact_person_widget.dart';

class Contragent {
  int? id;
  int? archiv = 0;
  String? status = ' ';
  String? naimenovanie = '';
  String? code = '';
  String? email = '';
  String? phoneNumber = '';
  String? factAdress = '';
  String? comment = '';
  String? typeContragent = '';
  String? polNaimenovanie = '';
  String? yurAdress = '';
  String? vladelesSot = '';
  String? vladalesOtdel = '';

  Contragent(
      {this.id,
      this.archiv = 0,
      this.status = '',
      this.naimenovanie = '',
      this.code = '',
      this.email = '',
      this.phoneNumber = '',
      this.factAdress = '',
      this.comment = '',
      this.typeContragent = '',
      this.polNaimenovanie = '',
      this.yurAdress = '',
      this.vladelesSot = '',
      this.vladalesOtdel = ''});

  factory Contragent.fromJson(Map<String, dynamic> json) {
    return Contragent(
      id: json['id'],
      archiv: json['archiv'],
      status: json['status'],
      naimenovanie: json['naimenovanie'],
      code: json['code'],
      email: json['email'],
      phoneNumber: json['phoneNumber'],
      factAdress: json['factAdress'],
      comment: json['comment'],
      typeContragent: json['typeContragent'],
      polNaimenovanie: json['polNaimenovanie'],
      yurAdress: json['yurAdress'],
      vladelesSot: json['vladelesSot'],
      vladalesOtdel: json['vladalesOtdel'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'archiv': archiv,
      'status': status,
      'naimenovanie': naimenovanie,
      'code': code,
      'email': email,
      'phoneNumber': phoneNumber,
      'factAdress': factAdress,
      'comment': comment,
      'typeContragent': typeContragent,
      'polNaimenovanie': polNaimenovanie,
      'yurAdress': yurAdress,
      'vladelesSot': vladelesSot,
      'vladalesOtdel': vladalesOtdel
    };
  }
}

class ContactFace {
  int? contragent_id;
  String? name;
  String? dolzhnost;
  String? phoneNumber;
  String? email;
  String? comment;
  ContactFace(
      {this.contragent_id,
      this.name,
      this.dolzhnost,
      this.phoneNumber,
      this.email,
      this.comment});

  Map<String, dynamic> toMap() {
    return {
      'id': contragent_id,
      'name': name,
      'dolzhnost': dolzhnost,
      'phoneNumber': phoneNumber,
      'email': email,
      'comment': comment
    };
  }
}
