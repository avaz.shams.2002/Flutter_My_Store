import 'package:flutter/material.dart';
import 'package:my_store/models/receipt_product_model.dart';
import 'package:my_store/pages/receipt_page_about.dart';
import 'package:my_store/screens/side_bar.dart';
import 'package:provider/provider.dart';

import '../models/receipt_model.dart';
import '../providers/receipt_and_tovar_provider.dart';
import '../widgets/search_receipt_page.dart';

class ReceiptPage extends StatefulWidget {
  const ReceiptPage({Key? key}) : super(key: key);

  @override
  State<ReceiptPage> createState() => _ReceiptPageState();
}

class _ReceiptPageState extends State<ReceiptPage> {
  @override
  Widget build(BuildContext context) {
    var receiptProvider = Provider.of<ReceipAndTovarProvider>(context);
    List<ReceiptProduct> listReceipt = [];
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        drawer: const SideBar(),
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          elevation: 0,
          // actions: [
          //   TextButton(
          //       onPressed: () {
          //         print(receiptProvider.listProductMaker.length);
          //       },
          //       child: const Text(
          //         "Check",
          //         style: TextStyle(color: Colors.white),
          //       ))
          // ],
          title: const Text("Все продажи"),
        ),
        body: Column(
          children: [
            InkWell(
              onTap: () {
                showSearch(
                    context: context,
                    delegate: SearchReceiptPage(
                        listReceipt: receiptProvider.listReceipt,
                        listReceiptProduct: receiptProvider.listProductMaker));
              },
              child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Icon(
                            Icons.search,
                            size: 25,
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Text(
                            "Поиск",
                            style: TextStyle(fontSize: 20),
                          )
                        ],
                      ),
                      Text(
                        "Итог: ${receiptProvider.getAllPrice() == 0.0 ? "" : receiptProvider.getAllPrice().toStringAsFixed(2)}",
                        style:
                            const TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ],
                  )),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: receiptProvider.listReceipt.length,
                  itemBuilder: (context, index) {
                    var returnedTovar = receiptProvider.listProductMaker
                        .where((element) =>
                            element.receiptId ==
                            receiptProvider.listReceipt[index].id)
                        .toList();
                    return InkWell(
                        onTap: () {
                          listReceipt = [];
                          listReceipt = receiptProvider.listProductMaker
                              .where((element) =>
                                  element.receiptId ==
                                  receiptProvider.listReceipt[index].id)
                              .toList();
                          // for (int i = 0; i < listReceipt.length; i++) {
                          //   print(listReceipt[i].quantity);
                          //   print("receipt id: ${listReceipt[i].receiptId}");
                          //   print(listReceipt[i].prodazhiPrice);
                          // }
                          //
                          // print("List length: ${listReceipt.length}");
                          // print(receiptProvider.listProductMaker.length);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ReceiptPageAbout(
                                        productMaker: listReceipt,
                                        receipt:
                                            receiptProvider.listReceipt[index],
                                      )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  const Icon((Icons.money_outlined)),
                                  const SizedBox(
                                    width: 30,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(receiptProvider.listReceipt[index].id
                                          .toString()),
                                      Text(
                                          "Время: ${receiptProvider.listReceipt[index].dateTime!.substring(0, 11)} в "
                                          "${receiptProvider.listReceipt[index].dateTime!.substring(11, 16)}")
                                    ],
                                  )
                                ],
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(double.parse(receiptProvider
                                            .listReceipt[index].total!)
                                        .toStringAsFixed(2)),
                                    if (returnedTovar.every(
                                        (element) => element.returned == 1))
                                      const Text(
                                        "Возвращено",
                                        style:
                                            TextStyle(color: Colors.redAccent),
                                      )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ));
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
