import 'package:flutter/material.dart';
import 'package:my_store/models/category_maker_model.dart';
import 'package:my_store/providers/category_provider.dart';
import 'package:my_store/screens/category_archive.dart';
import 'package:my_store/screens/catergory_creator_screen.dart';
import 'package:my_store/screens/side_bar.dart';
import 'package:my_store/widgets/categoryPage_search_widget.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var categoryProvider = Provider.of<CategoryProvider>(context);
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.pink,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CategoryCreatorScreen(
                            groupController: null,
                            dropDownButtonList: {},
                            fromTovarPage: false,
                          )));
            },
            child: const Text("+",
                style: TextStyle(color: Colors.white, fontSize: 20))),
        drawer: const SideBar(),
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          elevation: 0,
          title: const Text("Категории"),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const CategoryArchive()));
                },
                icon: const Icon(Icons.archive_outlined)),
            IconButton(
                onPressed: () {
                  showSearch(
                      context: context,
                      delegate: CategorySearchPageWidget(
                          categoryProvider.getCategoryNotArchive()));
                },
                icon: Icon(Icons.search))
          ],
        ),
        body: categoryProvider.getCategoryNotArchive().isEmpty
            ? const Text("")
            : Padding(
                padding: const EdgeInsets.only(top: 15),
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                    color: Colors.black,
                    thickness: 1,
                  ),
                  itemCount: categoryProvider.getCategoryNotArchive().length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CategoryCreatorScreen(
                                      groupController: null,
                                      dropDownButtonList: {},
                                      category: categoryProvider
                                          .getCategoryNotArchive()[index],
                                      fromTovarPage: false,
                                    )));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Color(categoryProvider
                                              .getCategoryNotArchive()[index]
                                              .colorInt!)),
                                      width: 100,
                                      height: 50,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(categoryProvider
                                            .getCategoryNotArchive()[index]
                                            .name!),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(categoryProvider
                                            .getCategoryNotArchive()[index]
                                            .createdAt
                                            .toString()),
                                      ],
                                    ),
                                  ],
                                ),
                                IconButton(
                                    onPressed: () {
                                      ProductMakerDb.deleteCategory(
                                          categoryProvider
                                              .getCategoryNotArchive()[index]
                                              .id!);
                                      categoryProvider.deleteOneCategory(
                                          categoryProvider
                                              .getCategoryNotArchive()[index]
                                              .id!);
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      color: Colors.blueGrey,
                                    ))
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
      ),
    );
  }
}
