import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_store/providers/check_field_login_page.dart';
import 'package:my_store/providers/check_field_reg_page.dart';
import 'package:provider/provider.dart';

import '../providers/user_check.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  late TextEditingController _userLoginFirled;
  late TextEditingController _passwordUserFiels;

  late final _key = GlobalKey<FormState>();
  @override
  // ignore: must_call_super
  void initState() {
    _userLoginFirled = TextEditingController(text: '');
    _passwordUserFiels = TextEditingController(text: '');
  }

  @override
  Widget build(BuildContext context) {
    var getUserProvider = Provider.of<CheckUser>(context);
    var getCheckRegFieldProvider = Provider.of<CheckFieldRegPage>(context);
    var internet = Provider.of<CheckFieldLoginPage>(context);
    Future<void> checkRegFields() async {
      FocusManager.instance.primaryFocus?.unfocus();
      try {
        if (!_key.currentState!.validate()) {
          // if (_userLoginFirled.text.isEmpty || _passwordUserFiels.text.isEmpty) {
          getCheckRegFieldProvider
              .checkRegFunction(_userLoginFirled.text.trim());
          getCheckRegFieldProvider
              .checkPasswordFunction(_passwordUserFiels.text.trim());
          print("If is working");
          return;
        }

        getCheckRegFieldProvider.checkRegFunction(_userLoginFirled.text.trim());
        getCheckRegFieldProvider
            .checkPasswordFunction(_passwordUserFiels.text.trim());
        if (internet.checkInternet == false) {
          AlertDialog alert = AlertDialog(
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text("Ok"))
            ],
            content: const Text("No internet connetion"),
            title: const Text(
              "Error",
            ),
          );
          showDialog(context: context, builder: (context) => alert);
          return;
        }
        await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: _userLoginFirled.text.trim(),
            password: _passwordUserFiels.text.trim());
        getUserProvider.setUser(user: FirebaseAuth.instance.currentUser);
        Navigator.pop(context);
        print("If is not  working");
      } on FirebaseAuthException catch (e) {
        Fluttertoast.showToast(
            msg: e.message.toString(), gravity: ToastGravity.CENTER);
      }
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 25, right: 25),
        child: Form(
          key: _key,
          child: Column(
            children: [
              Stack(
                children: [
                  TextFormField(
                    validator: (value) {
                      if (!RegExp(
                              "^[a-zA-Z0-9.!#\$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*\$")
                          .hasMatch(value!)) {
                        return 'Please Enter Email Adress';
                      }
                    },
                    controller: _userLoginFirled,
                    style: const TextStyle(color: Colors.white),
                    decoration: const InputDecoration(
                        labelText: "Email",
                        labelStyle: TextStyle(color: Colors.white),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child: getCheckRegFieldProvider.checkLogin.isEmpty
                          ? const Icon(
                              Icons.error_outline,
                              color: Colors.red,
                            )
                          : const Text(""))
                ],
              ),
              Stack(
                children: [
                  TextFormField(
                    controller: _passwordUserFiels,
                    style: const TextStyle(color: Colors.white),
                    decoration: const InputDecoration(
                        labelText: "Password",
                        labelStyle: TextStyle(color: Colors.white),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white))),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child: getCheckRegFieldProvider.checkPassword.isEmpty
                          ? const Icon(
                              Icons.error_outline,
                              color: Colors.red,
                            )
                          : const Text(""))
                ],
              ),
              const Padding(padding: const EdgeInsets.only(top: 15)),

              Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.white)),
                    onPressed: () {
                      checkRegFields();
                    },
                    child: const Text(
                      "ЗАРЕГИСТРИРОВАТЬСЯ",
                      style: TextStyle(color: Colors.blueAccent),
                    )),
              )
              // GestureDetector(
              //   onTap: () {
              //     checkRegFields();
              //     print("Taped");
              //   },
              //   child: Container(
              //     height: 50,
              //     color: Colors.white,
              //     child: const Center(
              //       // ignore: unnecessary_const
              //       child: const Text(
              //         "ЗАРЕГИСТРИРОВАТЬСЯ",
              //         style: TextStyle(color: Colors.blueAccent),
              //       ),
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
