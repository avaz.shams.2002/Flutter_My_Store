import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';
import '../providers/product_maker_provider.dart';
import '../screens/tovar_about.dart';

class ArchivePage extends StatelessWidget {
  const ArchivePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var productCart = Provider.of<ProductMakerProvider>(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          elevation: 0,
          title: Text("Архив товаров"),
        ),
        body: productCart.productsmaker.isEmpty
            ? const Text("")
            : Padding(
                padding: const EdgeInsets.only(top: 15),
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                    color: Colors.black,
                    thickness: 1,
                  ),
                  itemCount: productCart.getArchiveTovar().length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TovarAbout(
                                      productMaker:
                                          productCart.getArchiveTovar()[index],
                                    )));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                if (productCart
                                            .getArchiveTovar()[index]
                                            .image
                                            .toString() ==
                                        "File: 'null'" ||
                                    productCart
                                            .getArchiveTovar()[index]
                                            .image ==
                                        null)
                                  Container(
                                    width: 100,
                                    height: 50,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image:
                                            AssetImage("assets/images/1.jpg"),
                                        fit: BoxFit.fitWidth,
                                        alignment: Alignment.bottomLeft,
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "${productCart.getArchiveTovar()[index].naimenovanie![0]}"
                                        "${productCart.getArchiveTovar()[index].naimenovanie![productCart.getArchiveTovar()[index].naimenovanie!.length - 1]}",
                                        style: const TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ),
                                  )
                                else
                                  Container(
                                      width: 100,
                                      height: 50,
                                      child: Image.file(productCart
                                          .getArchiveTovar()[index]
                                          .image!)),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        if (productCart
                                            .getArchiveTovar()[index]
                                            .naimenovanie!
                                            .isNotEmpty)
                                          Text(
                                              "${productCart.getArchiveTovar()[index].code} • ${productCart.getArchiveTovar()[index].naimenovanie}"),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            const Icon(
                                              Icons
                                                  .shopping_cart_checkout_outlined,
                                              color: Colors.pink,
                                            ),
                                            Text(productCart
                                                .getArchiveTovar()[index]
                                                .quantity
                                                .toString())
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {
                                      ProductMakerDb.deleteProduct(productCart
                                          .getArchiveTovar()[index]
                                          .id!);
                                      productCart.deleteProduct(
                                          productCart.getArchiveTovar()[index]);
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      color: Colors.blueGrey,
                                    ))
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ));
  }
}
