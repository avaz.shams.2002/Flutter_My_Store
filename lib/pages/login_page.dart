import 'dart:async';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:my_store/pages/registor_page.dart';
import 'package:my_store/providers/check_field_login_page.dart';
import 'package:my_store/providers/user_check.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late TextEditingController _loginController;
  late TextEditingController _passwordControllerl;
  final _loginKey = GlobalKey<FormState>();
  // late var _internetConnection = false;
  late bool _hideAndShow = true;
  late final GlobalKey<NavigatorState> navigatorKey;
  @override
  // ignore: must_call_super
  void initState() {
    // ignore: todo
    // TODO: implement initState
    _loginController = TextEditingController(text: 'ava@gmail.com');
    _passwordControllerl = TextEditingController(text: '12345678');

    InternetConnectionChecker().onStatusChange.listen((event) {
      final check = event == InternetConnectionStatus.connected;
      if (mounted) {
        setState(() {
          Provider.of<CheckFieldLoginPage>(context, listen: false)
              .checkInet(check);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var getCheckFieldProvider = Provider.of<CheckFieldLoginPage>(context);
    var getUserProvider = Provider.of<CheckUser>(context);
    Future logInFunc() async {
      await Firebase.initializeApp();
      FocusManager.instance.primaryFocus?.unfocus();
      //form for validation
      //   |
      //  \ /
      //   |
      try {
        if (!_loginKey.currentState!.validate()) {
          // if (_loginController.text.isEmpty || _passwordControllerl.text.isEmpty) {
          getCheckFieldProvider
              .checkLoginFunction(_loginController.text.trim());
          getCheckFieldProvider
              .checkPasswordFunction(_passwordControllerl.text.trim());
          print("Email: ${getCheckFieldProvider.checkLogin}");
          print("Password: ${getCheckFieldProvider.checkPassword}");
          return;
        }
        getCheckFieldProvider.checkLoginFunction(_loginController.text.trim());
        getCheckFieldProvider
            .checkPasswordFunction(_passwordControllerl.text.trim());
        if (getCheckFieldProvider.checkInternet == false) {
          AlertDialog alert = AlertDialog(
            title: const Text("Error"),
            content: const Text("No internet connection"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text("Ok"))
            ],
          );

          showDialog(
              context: context,
              builder: (context) {
                return alert;
              },
              barrierDismissible: false);
          return;
        }
        // showDialog(
        //     context: context,
        //     builder: (context) => const Center(
        //           child: CircularProgressIndicator(),
        //         ));
        // ignore: curly_braces_in_flow_control_structures

        await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: _loginController.text.trim(),
            password: _passwordControllerl.text.trim());

        print("USJNKDSJJN  ${FirebaseAuth.instance.currentUser?.email}");
        getUserProvider.setUser(user: FirebaseAuth.instance.currentUser);

        // Navigator.of(context, rootNavigator: true).pop();
        print("Email: ${getCheckFieldProvider.checkLogin}");
        print("Password: ${getCheckFieldProvider.checkPassword}");
        print("Worked");
      } catch (error) {
        print(error);
        Fluttertoast.showToast(
            msg: "Не действительный пароль или логин",
            gravity: ToastGravity.CENTER);
      }
    }

    return WillPopScope(
      onWillPop: () => exit(0),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.blueAccent,
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.home,
                    size: 40,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "МойСклад",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        fontFamily: "BebasNeue-Regular"),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Form(
                  key: _loginKey,
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          TextFormField(
                            controller: _loginController,
                            keyboardType: TextInputType.emailAddress,
                            decoration: const InputDecoration(
                                labelText: "Логин(user@company) или Email",
                                labelStyle: TextStyle(
                                  color: Colors.white,
                                ),
                                border: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white))

                                // hintText: "Логин(user@company) или Email",
                                // hintStyle: TextStyle(color: Colors.white),

                                ),
                            style: const TextStyle(color: Colors.white),
                            onSaved: (value) {},
                            validator: (value) {
                              if (!RegExp(
                                      "^[a-zA-Z0-9.!#\$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0"
                                      "-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*\$")
                                  .hasMatch(value!)) {
                                return 'There is not an Email address';
                              }
                            },
                          ),
                          // Positioned(
                          //     right: 0,
                          //     bottom: 23,
                          //     child: getCheckFieldProvider.checkLogin.isEmpty
                          //         ? const Icon(
                          //             Icons.error_outline,
                          //             color: Colors.red,
                          //           )
                          //         : const Text(""))
                        ],
                      ),
                      Stack(
                        children: [
                          TextFormField(
                            maxLength: 15,
                            obscureText: _hideAndShow,
                            style: const TextStyle(color: Colors.white),
                            controller: _passwordControllerl,
                            // keyboardType: TextInputType.,
                            decoration: const InputDecoration(
                                counterText: "",
                                labelText: "Пароль",
                                labelStyle: TextStyle(color: Colors.white),
                                border: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white))),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "";
                              }
                            },
                          ),
                          // Positioned(
                          //     right: 0,
                          //     bottom: 0,
                          //     child: getCheckFieldProvider.checkPassword.isEmpty
                          //         ? const Icon(
                          //             Icons.error_outline,
                          //             color: Colors.red,
                          //           )
                          //         : const Text("")),
                          Positioned(
                              right: 0,
                              bottom:
                                  getCheckFieldProvider.checkPassword.isEmpty
                                      ? 20
                                      : 0,
                              child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (_hideAndShow) {
                                      _hideAndShow = false;
                                    } else {
                                      _hideAndShow = true;
                                    }
                                  });
                                },
                                icon: Icon(
                                  Icons.remove_red_eye_outlined,
                                  color: _hideAndShow == false
                                      ? Colors.red
                                      : Colors.black,
                                ),
                                // color: Colors.black,
                              ))
                        ],
                      ),
                      const Padding(padding: const EdgeInsets.only(top: 15)),
                      GestureDetector(
                        onTap: () {
                          logInFunc();
                        },
                        child: Container(
                            height: 50,
                            color: const Color.fromARGB(255, 14, 38, 81),
                            width: MediaQuery.of(context).size.width,
                            child: const Center(
                                child: Text(
                              "ВОЙТИ",
                              style: TextStyle(color: Colors.white),
                            ))),
                      ),
                      const Padding(padding: EdgeInsets.only(top: 25)),
                      TextButton(
                          onPressed: () {
                            FocusManager.instance.primaryFocus?.unfocus();
                            print("Registor");
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    const RegistrationPage()));
                          },
                          child: const Text(
                            "Зарегистрироваться",
                            style: TextStyle(color: Colors.white),
                          ))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
