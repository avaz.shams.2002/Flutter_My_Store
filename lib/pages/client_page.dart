import 'package:flutter/material.dart';
import 'package:my_store/models/client_model.dart';
import 'package:my_store/providers/client_provider.dart';
import 'package:my_store/screens/client_creator_screen.dart';
import 'package:my_store/screens/client_profile_screen.dart';
import 'package:provider/provider.dart';

class ClientPage extends StatefulWidget {
  const ClientPage({Key? key}) : super(key: key);

  @override
  State<ClientPage> createState() => _ClientPageState();
}

class _ClientPageState extends State<ClientPage> {
  int changeSearch = 0;
  late final TextEditingController _searchController =
      TextEditingController(text: '');
  final focus = FocusNode();

  void setChangeSearchZero(int zero) {
    setState(() {});
    changeSearch = 0;
    FocusManager.instance.primaryFocus!.unfocus();
  }

  List<Client> clients = [];

  @override
  Widget build(BuildContext context) {
    var clientProvider = Provider.of<ClientProvider>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
        title: const Text(
          "Добавление клиента к чеку",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0,
        actions: [
          // TextButton(
          //     onPressed: () {
          //       print(clientProvider.listOfClient.length);
          //     },
          //     child: const Text("check"))
        ],
      ),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus!.unfocus();
        },
        child: ListView(
          children: [
            const Divider(
              thickness: 1,
              color: Colors.grey,
            ),
            InkWell(
              onTap: () {
                setState(() {});
                changeSearch = 1;
                _searchController.text = '';
                Future.delayed(const Duration(milliseconds: 200), () {
                  FocusScope.of(context).requestFocus(focus);
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 5, bottom: 5),
                child: Row(
                  children: [
                    const Icon(
                      Icons.search,
                      size: 30,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    if (changeSearch == 0)
                      const Text(
                        "Поиск",
                        style: TextStyle(fontSize: 20),
                      ),
                    if (changeSearch == 1)
                      Expanded(
                        child: Stack(
                          children: [
                            TextField(
                              onChanged: (val) {
                                setState(() {});

                                clients.clear();
                                for (var all in clientProvider.listOfClient) {
                                  if (all.name!.toUpperCase().contains(
                                      _searchController.text.toUpperCase())) {
                                    clients.add(all);
                                  }
                                }
                                if (val.isEmpty) {
                                  clients.clear();
                                }
                              },
                              focusNode: focus,
                              controller: _searchController,
                              decoration: const InputDecoration(
                                  hintText: "Поиск",
                                  hintStyle:  TextStyle(fontSize: 20)),
                            ),
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: IconButton(
                                  onPressed: () {
                                    setState(() {});
                                    clients.clear();
                                    changeSearch = 0;
                                    FocusManager.instance.primaryFocus!
                                        .unfocus();
                                  },
                                  icon: const Icon(Icons.close)),
                            )
                          ],
                        ),
                      )
                  ],
                ),
              ),
            ),
            const Divider(
              thickness: 1,
              color: Colors.grey,
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ClientCreatorScreen(
                                refreshClientPage: setChangeSearchZero,
                              )));
                },
                child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "ДОБАВИТЬ НОВОГО КЛИЕНТА",
                          style: TextStyle(color: Colors.blueAccent),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
            ),
            const Divider(
              thickness: 1,
              color: Colors.grey,
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Row(
                children: [
                  const Text("Последние клиенты"),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            if (changeSearch == 1)
              if (clients.isNotEmpty)
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: clients.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ClientProfileScreen(
                                      client: clients[index])));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 12),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Center(
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey[300]),
                                  child: const Center(
                                      child: Icon(
                                    Icons.account_circle_rounded,
                                    color: Colors.grey,
                                  )),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 20),
                                  child: Column(
                                    children: [
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${clients[index].name}",
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                          ),
                                        ],
                                      ),
                                      const Divider(
                                        color: Colors.grey,
                                        thickness: 1,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    })
              else
                getClientFromProvider(clientProvider)
            else
              getClientFromProvider(clientProvider)
          ],
        ),
      ),
    );
  }

  Widget getClientFromProvider(ClientProvider clientProvider) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: clientProvider.listOfClient.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ClientProfileScreen(
                          client: clientProvider.listOfClient[index])));
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 12),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Center(
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.grey[300]),
                      child: const Center(
                          child: Icon(
                        Icons.account_circle_rounded,
                        color: Colors.grey,
                      )),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 20, left: 20),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${clientProvider.listOfClient[index].name}",
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            ],
                          ),
                          const Divider(
                            color: Colors.grey,
                            thickness: 1,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
