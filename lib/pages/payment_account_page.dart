import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:my_store/providers/payment_account_provider.dart';
import 'package:provider/provider.dart';

class PayMentAccount extends StatelessWidget {
  late String? nomerRsScheta;
  late String? bik;
  late String? bank;
  late String? address;
  late String? korrSchet;
  late bool? osnovnoySchet;
  PayMentAccount(
      {Key? key,
      this.nomerRsScheta = '',
      this.bik = '',
      this.bank = '',
      this.address = '',
      this.korrSchet = '',
      this.osnovnoySchet = false})
      : super(key: key);

  late TextEditingController nomerController =
      TextEditingController(text: nomerRsScheta);

  late TextEditingController bikController = TextEditingController(text: bik);

  late TextEditingController bankConroller = TextEditingController(text: bank);

  late TextEditingController addressController =
      TextEditingController(text: address);

  late TextEditingController korrSchetController =
      TextEditingController(text: korrSchet);

  late bool? osnovnoyController = osnovnoySchet;

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var paymentProvider = Provider.of<PayMentAccountProvider>(context);
    var _textFieldStyle = TextStyle(color: Colors.grey, fontSize: 14);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.close)),
        iconTheme: const IconThemeData(color: Colors.white),
        actions: [
          TextButton(
              onPressed: () {
                FocusManager.instance.primaryFocus!.unfocus();
                if (!_formKey.currentState!.validate() ||
                    nomerController.text.isEmpty) {
                  return;
                }

                if (!paymentProvider.itemExists(this)) {
                  paymentProvider.addWidget(
                      osnovnoySchet: osnovnoyController,
                      nomer: nomerController.text.isEmpty
                          ? ""
                          : nomerController.text,
                      bik: bikController.text.isEmpty ? "" : bikController.text,
                      bank:
                          bankConroller.text.isEmpty ? "" : bankConroller.text,
                      address: addressController.text.isEmpty
                          ? ""
                          : addressController.text,
                      korrSchet: addressController.text.isEmpty
                          ? ""
                          : addressController.text);
                }
                paymentProvider.notify();
                Navigator.pop(context);
              },
              child: const Text("Сохранить",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ))),
          if (paymentProvider.itemExists(this))
            IconButton(
                onPressed: () {
                  paymentProvider.deleteWidget(this);
                  Navigator.pop(context);
                },
                icon: Icon(Icons.delete))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
        child: ListView(
          children: [
            Stack(
              children: [
                const Padding(
                  padding: const EdgeInsets.only(left: 45, right: 45, top: 30),
                  child: Center(
                    child: Text(
                      "Укажите БИК, чтобы заполнить остальные реквизиты автоматически",
                      style: TextStyle(color: Colors.grey, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Positioned(
                    top: 0,
                    right: 0,
                    child: IconButton(
                        onPressed: () {}, icon: const Icon(Icons.close)))
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 45, right: 15),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Основной счет",
                          style: const TextStyle(
                              color: Colors.black, fontSize: 14),
                        ),
                        FlutterSwitch(
                            width: 50,
                            height: 25,
                            value: osnovnoyController!,
                            onToggle: (val) {
                              if (osnovnoyController! == false) {
                                osnovnoyController = val;
                              } else {
                                osnovnoyController = val;
                              }
                            })
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      controller: nomerController,
                      decoration: InputDecoration(
                          hintText: "Номер расчетного счета",
                          hintStyle: _textFieldStyle),
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Поле не может быть пустым";
                        }
                      },
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: bikController,
                      decoration: InputDecoration(
                          hintText: "БИК", hintStyle: _textFieldStyle),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: bankConroller,
                      decoration: InputDecoration(
                          hintText: "Банк", hintStyle: _textFieldStyle),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: addressController,
                      decoration: InputDecoration(
                          hintText: "Адрес", hintStyle: _textFieldStyle),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: korrSchetController,
                      decoration: InputDecoration(
                          hintText: "Корр. счет", hintStyle: _textFieldStyle),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
