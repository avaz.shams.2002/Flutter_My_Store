import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/contragent_maker_model.dart';
import 'package:my_store/pages/payment_account_page.dart';
import 'package:my_store/providers/contragent_provider.dart';
import 'package:my_store/widgets/contact_person_widget.dart';
import 'package:provider/provider.dart';

import '../providers/contact_person_provider.dart';
import '../providers/payment_account_provider.dart';

class SupplierCreatorPage extends StatefulWidget {
  Contragent? contragent;
  List<ContactPersonWidget>? contactWidget = [];

  SupplierCreatorPage({Key? key, this.contragent, this.contactWidget})
      : super(key: key);

  @override
  State<SupplierCreatorPage> createState() => _SupplierCreatorPageState();
}

class _SupplierCreatorPageState extends State<SupplierCreatorPage> {
  final _formKey = GlobalKey<FormState>();
  final _formKeyBottom = GlobalKey<FormState>();

  late bool konrtaArchiv = false;
  late bool dostupSwitcher = false;

  late TextEditingController name = TextEditingController(text: "");
  late TextEditingController dolzhnost = TextEditingController(text: '');
  late TextEditingController telephone = TextEditingController(text: '');
  late TextEditingController email = TextEditingController(text: '');
  late TextEditingController comment = TextEditingController(text: '');
  late TextEditingController _statusController =
      TextEditingController(text: '');
  late TextEditingController _naimenovanie = TextEditingController(text: '');
  late TextEditingController _code = TextEditingController(text: '');
  late TextEditingController _telephone = TextEditingController(text: '');
  late TextEditingController _email = TextEditingController(text: '');
  late TextEditingController _factAddress = TextEditingController(text: '');
  late TextEditingController _comment = TextEditingController(text: '');
  late TextEditingController _typeContragentController =
      TextEditingController(text: '');
  late TextEditingController _polNaimenovanie = TextEditingController(text: '');
  late TextEditingController _urAddress = TextEditingController(text: '');
  late TextEditingController _inn = TextEditingController(text: '');
  late TextEditingController _kpp = TextEditingController(text: '');
  late TextEditingController _okpo = TextEditingController(text: '');
  late TextEditingController _ogrh = TextEditingController(text: '');
  late TextEditingController _vladelesSotController =
      TextEditingController(text: '');
  late TextEditingController _vladelesOtdelController =
      TextEditingController(text: '');
  late List<ContactPersonWidget> contactWidget;

  @override
  void initState() {
    if (widget.contragent != null) {
      if(widget.contragent!.archiv == 1)
        {
          konrtaArchiv = true;
        }
      _statusController =
          TextEditingController(text: widget.contragent!.status);
      _naimenovanie =
          TextEditingController(text: widget.contragent!.naimenovanie);
      _code = TextEditingController(text: widget.contragent!.code);
      _telephone = TextEditingController(text: widget.contragent!.phoneNumber);
      _email = TextEditingController(text: widget.contragent!.email);
      _factAddress = TextEditingController(text: widget.contragent!.factAdress);
      _comment = TextEditingController(text: widget.contragent!.comment);
      _typeContragentController =
          TextEditingController(text: widget.contragent!.typeContragent);
      _polNaimenovanie =
          TextEditingController(text: widget.contragent!.polNaimenovanie);
      _urAddress = TextEditingController(text: widget.contragent!.yurAdress);
      _vladelesSotController =
          TextEditingController(text: widget.contragent!.vladelesSot);
      _vladelesOtdelController =
          TextEditingController(text: widget.contragent!.vladalesOtdel);
    }
    contactWidget = widget.contactWidget!;
  }

  @override
  Widget build(BuildContext context) {
    final List<String> _items = ['Юрлицо', 'ИП', 'Физ.лицо'];
    var _textStle = const TextStyle(color: Colors.grey, fontSize: 14);
    var getContact = Provider.of<ContactPersonProvider>(context);
    var paymentProvider = Provider.of<PayMentAccountProvider>(context);
    var setContragent = Provider.of<ContragentProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        iconTheme: const IconThemeData(color: Colors.white),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            AlertDialog alert = AlertDialog(
              content: const Text("Удалять изменения?"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text("Не удалять")),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: const Text("Удалять")),
              ],
            );
            showDialog(context: context, builder: (context) => alert);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        actions: [
          TextButton(
              onPressed: () async {
                if (!_formKey.currentState!.validate() ||
                    _naimenovanie.text.isEmpty ||
                    _telephone.text.isEmpty) {
                  AlertDialog alert = AlertDialog(
                    title: const Text("Не заполнены обязательные поля:"),
                    content: const Text("Наименование или Телефон"),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text("OK"))
                    ],
                  );
                  showDialog(context: context, builder: (context) => alert);
                  return;
                }
                widget.contragent ??= Contragent(
                    archiv: konrtaArchiv == false ? 0 : 1,
                    status: _statusController.text,
                    naimenovanie: _naimenovanie.text,
                    code: _code.text,
                    email: _email.text,
                    phoneNumber: _telephone.text,
                    factAdress: _factAddress.text,
                    comment: _comment.text,
                    typeContragent: _typeContragentController.text,
                    polNaimenovanie: _polNaimenovanie.text,
                    yurAdress: _urAddress.text,
                    vladelesSot: _vladelesSotController.text,
                    vladalesOtdel: _vladelesOtdelController.text,

                );

                if (!setContragent.checkContragent(widget.contragent!)) {
                  await ProductMakerDb.insertContragent(widget.contragent!);
                  print("Id : ${widget.contragent!.id}");
                  setContragent.addContragent(widget.contragent!);
                  if (contactWidget.isNotEmpty) {
                    await ProductMakerDb.insertToContactFace(
                        widget.contragent!.id!, contactWidget);
                    for (int i = 0; i < contactWidget.length; i++) {
                      getContact.addWidget(
                          id: contactWidget[i].id,
                          contragent_id: widget.contragent!.id,
                          fam: contactWidget[i].nname,
                          dol: contactWidget[i].ddolzhnost,
                          tel: contactWidget[i].ttelephone,
                          email: contactWidget[i].eemail,
                          commemt: contactWidget[i].ccomment);
                    }
                    print("List length: ${getContact.listWidget.length}");
                  }
                  // getContact.clear();
                  Navigator.pop(context);
                  return;
                }
                widget.contragent!.archiv = konrtaArchiv == false ? 0 : 1;
                widget.contragent!.status = _statusController.text;
                widget.contragent!.naimenovanie = _naimenovanie.text;
                widget.contragent!.code = _code.text;
                widget.contragent!.email = _email.text;
                widget.contragent!.phoneNumber = _telephone.text;
                widget.contragent!.factAdress = _factAddress.text;
                widget.contragent!.comment = _comment.text;
                widget.contragent!.typeContragent =
                    _typeContragentController.text;
                widget.contragent!.polNaimenovanie = _polNaimenovanie.text;
                widget.contragent!.yurAdress = _urAddress.text;
                widget.contragent!.vladelesSot = _vladelesSotController.text;
                widget.contragent!.vladalesOtdel =
                    _vladelesOtdelController.text;
                setContragent.notify();

                await ProductMakerDb.updateContragent(widget.contragent!);
                await ProductMakerDb.insertToContactFace(
                    widget.contragent!.id!, contactWidget);
                for (int i = 0; i < contactWidget.length; i++) {
                  getContact.addWidget(
                      id: contactWidget[i].id,
                      contragent_id: widget.contragent!.id,
                      fam: contactWidget[i].name.text,
                      dol: contactWidget[i].ddolzhnost,
                      tel: contactWidget[i].ttelephone,
                      email: contactWidget[i].eemail,
                      commemt: contactWidget[i].ccomment);
                }
                Navigator.pop(context);

                paymentProvider.clear();
                Navigator.of(context);
              },
              child: const Text(
                "Сохранить",
                style: TextStyle(color: Colors.white, fontSize: 20),
              )),
          // TextButton(
          //     onPressed: () {
          //       for (int i = 0; i < contactWidget.length; i++) {
          //         print(contactWidget[i].nname);
          //       }
          //     },
          //     child: const Text(
          //       "Check",
          //       style: TextStyle(color: Colors.white),
          //     )),
          if (widget.contragent != null && widget.contragent!.id != null)
            IconButton(
                onPressed: () async {
                  await ProductMakerDb.deleteContactFace(
                      widget.contragent!.id!);
                  await ProductMakerDb.deleteContragent(widget.contragent!.id!);
                  getContact.deleteAllWithId(widget.contragent!.id!);
                  setContragent.removeWithId(widget.contragent!.id!);
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ))
        ],
      ),
      body: Form(
        key: _formKey,
        child: Scrollbar(
          child: ListView(
            children: [
              const SizedBox(
                height: 10,
              ),
              Card(
                shadowColor: Colors.black,
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.only(left: 60, right: 60),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Контрагент",
                        style:
                            TextStyle(color: Colors.blueAccent, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Архивный",
                            style: TextStyle(color: Colors.black, fontSize: 14),
                          ),
                          FlutterSwitch(
                              width: 50,
                              height: 25,
                              value: konrtaArchiv,
                              onToggle: (val) {
                                setState(() {
                                  if (konrtaArchiv == false) {
                                    konrtaArchiv = val;
                                  }

                                  konrtaArchiv = val;
                                });
                              })
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Stack(
                        children: [
                          TextField(
                            controller: _statusController,
                            readOnly: true,
                            onTap: () {
                              List<String> status = [
                                'Новый',
                                'Выслано предложение',
                                'Переговоры',
                                'Сделка заключена',
                                'Сделка не залключена'
                              ];
                              showModalBottomSheet(
                                  context: context,
                                  isScrollControlled: true,
                                  builder: (context) => Container(
                                        height:
                                            MediaQuery.of(context).size.height,
                                        color: Colors.blueAccent,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 30),
                                          child: Scaffold(
                                            appBar: AppBar(
                                              elevation: 0,
                                              backgroundColor: Colors.white,
                                              iconTheme: const IconThemeData(
                                                  color: Colors.black),
                                            ),
                                            body: ListView.separated(
                                              itemCount: status.length,
                                              separatorBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return const Divider();
                                              },
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      _statusController.text =
                                                          status[index];
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            12.0),
                                                    child: Text(status[index]),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                      ));
                            },
                            decoration: InputDecoration(
                                labelText: "Статус", labelStyle: _textStle),
                          ),
                          if (_statusController.text.isNotEmpty)
                            Positioned(
                                bottom: 0,
                                right: 0,
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _statusController.text = '';
                                      });
                                    },
                                    icon: const Icon(Icons.close)))
                        ],
                      ),
                      TextFormField(
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Поле не может быть пустым";
                          }
                        },
                        controller: _naimenovanie,
                        decoration: InputDecoration(
                            labelText: "Наименование*", labelStyle: _textStle),
                      ),
                      // const SizedBox(
                      //   height: 20,
                      // ),
                      TextField(
                        controller: _code,
                        decoration: InputDecoration(
                            labelText: "Код", labelStyle: _textStle),
                      ),
                      TextFormField(
                        controller: _email,
                        decoration: InputDecoration(
                            labelText: "Email", labelStyle: _textStle),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        controller: _telephone,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Поле не может быть пустым";
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "Телефон", labelStyle: _textStle),
                      ),
                      TextField(
                        controller: _factAddress,
                        decoration: InputDecoration(
                            labelText: "Фактический адресс",
                            labelStyle: _textStle),
                      ),
                      TextField(
                        controller: _comment,
                        decoration: InputDecoration(
                            labelText: "Комментарий", labelStyle: _textStle),
                      ),
                      const SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Card(
                shadowColor: Colors.black,
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.only(left: 60, right: 60),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Контактные лица",
                        style:
                            TextStyle(color: Colors.blueAccent, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      if (contactWidget.isNotEmpty)
                        ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: contactWidget.length,
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    const Divider(
                                      thickness: 2,
                                      color: Color.fromARGB(255, 55, 54, 54),
                                    ),
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () async {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              contactWidget[index]));
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          contactWidget[index].name.text,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14),
                                        ),
                                        if (contactWidget[index]
                                            .telephone
                                            .text
                                            .isNotEmpty)
                                          Text(contactWidget[index]
                                              .telephone
                                              .text),
                                        if (contactWidget[index]
                                            .email
                                            .text
                                            .isNotEmpty)
                                          Text(contactWidget[index].email.text),
                                        if (contactWidget[index]
                                            .dolzhnost
                                            .text
                                            .isNotEmpty)
                                          Text(contactWidget[index]
                                              .dolzhnost
                                              .text)
                                      ],
                                    ),
                                    IconButton(
                                      onPressed: () async {
                                        await ProductMakerDb
                                            .deleteContactFaceWithId(
                                                contactWidget[index].id!);
                                        getContact.deleteOneItem(
                                            contactWidget[index]);
                                        setState(() {
                                          contactWidget
                                              .remove(contactWidget[index]);
                                        });
                                      },
                                      icon: const Icon(Icons.delete,
                                          color: Colors.blueGrey),
                                    )
                                  ],
                                ),
                              );
                            }),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            showModalBottomSheet(
                                isScrollControlled: true,
                                context: context,
                                builder: (context) => Container(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      color: Colors.blueAccent,
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 25),
                                        child: Scaffold(
                                          appBar: AppBar(
                                            backgroundColor: Colors.blueAccent,
                                            leading: IconButton(
                                              onPressed: () {
                                                name = TextEditingController(
                                                    text: "");
                                                dolzhnost =
                                                    TextEditingController(
                                                        text: '');
                                                telephone =
                                                    TextEditingController(
                                                        text: '');
                                                email = TextEditingController(
                                                    text: '');
                                                comment = TextEditingController(
                                                    text: '');
                                                Navigator.pop(context);
                                              },
                                              icon: Icon(
                                                Icons.arrow_back,
                                                color: Colors.white,
                                              ),
                                            ),
                                            iconTheme: const IconThemeData(
                                                color: Colors.white),
                                            actions: [
                                              TextButton(
                                                  onPressed: () {
                                                    // print(name.text);
                                                    FocusManager
                                                        .instance.primaryFocus!
                                                        .unfocus();
                                                    if (!_formKeyBottom
                                                        .currentState!
                                                        .validate()) {
                                                      return;
                                                    }
                                                    setState(() {
                                                      contactWidget.add(
                                                          ContactPersonWidget(
                                                        nname: name.text,
                                                        ddolzhnost:
                                                            dolzhnost.text,
                                                        ttelephone:
                                                            telephone.text,
                                                        eemail: email.text,
                                                        ccomment: comment.text,
                                                      ));
                                                    });

                                                    name =
                                                        TextEditingController(
                                                            text: "");
                                                    dolzhnost =
                                                        TextEditingController(
                                                            text: '');
                                                    telephone =
                                                        TextEditingController(
                                                            text: '');
                                                    email =
                                                        TextEditingController(
                                                            text: '');
                                                    comment =
                                                        TextEditingController(
                                                            text: '');

                                                    Navigator.pop(context);
                                                  },
                                                  child: const Text(
                                                    "СОХРАНИТЬ",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 20),
                                                  )),
                                            ],
                                          ),
                                          body: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 60, right: 60),
                                            child: Form(
                                              key: _formKeyBottom,
                                              child: Column(
                                                children: [
                                                  TextFormField(
                                                    controller: name,
                                                    decoration:
                                                        const InputDecoration(
                                                            hintStyle: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14),
                                                            hintText: "ФИО"),
                                                    validator: (value) {
                                                      if (value!.isEmpty)
                                                        return "Поле не может быть пустым";
                                                    },
                                                  ),
                                                  TextField(
                                                    controller: dolzhnost,
                                                    decoration:
                                                        const InputDecoration(
                                                            hintStyle: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14),
                                                            hintText:
                                                                "Должность"),
                                                  ),
                                                  TextFormField(
                                                    validator: (val) {
                                                      if (val!.isEmpty) {
                                                        return 'Поле не может быть пустым';
                                                      }
                                                    },
                                                    controller: telephone,
                                                    keyboardType:
                                                        TextInputType.number,
                                                    decoration:
                                                        const InputDecoration(
                                                            hintStyle: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14),
                                                            hintText:
                                                                "Телефон"),
                                                  ),
                                                  TextField(
                                                    controller: email,
                                                    decoration:
                                                        const InputDecoration(
                                                            hintStyle:
                                                                const TextStyle(
                                                                    color: Colors
                                                                        .grey,
                                                                    fontSize:
                                                                        14),
                                                            hintText: "Email"),
                                                  ),
                                                  TextField(
                                                    controller: comment,
                                                    decoration:
                                                        const InputDecoration(
                                                            hintStyle: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14),
                                                            hintText:
                                                                "Комментарий"),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ));
                          },
                          child: const Text("ДОБАВИТЬ КОНТАКТНОЕ ЛИЦО")),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Card(
                shadowColor: Colors.black,
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.only(right: 60, left: 60),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Реквизиты",
                          style:
                              TextStyle(color: Colors.blueAccent, fontSize: 14),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Stack(
                          children: [
                            TextField(
                              readOnly: true,
                              controller: _typeContragentController,
                              decoration: InputDecoration(
                                  labelText: "Тип контрагента",
                                  labelStyle: _textStle),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              child: Container(
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                      items: _items
                                          .map((e) => DropdownMenuItem(
                                                child: Text(e),
                                                value: e,
                                              ))
                                          .toList(),
                                      onChanged: (val) {
                                        setState(() {
                                          _typeContragentController.text =
                                              val.toString();
                                        });
                                      }),
                                ),
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _polNaimenovanie,
                          decoration: InputDecoration(
                              hintText: "Полное наименование",
                              hintStyle: _textStle),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _urAddress,
                          decoration: InputDecoration(
                              hintStyle: _textStle,
                              hintText: "Юридический адрес"),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: _inn,
                          decoration: InputDecoration(
                              hintStyle: _textStle, hintText: "ИНН"),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: _kpp,
                          decoration: InputDecoration(
                              hintStyle: _textStle, hintText: "КПП"),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: _ogrh,
                          decoration: InputDecoration(
                              hintStyle: _textStle, hintText: "ОГРН"),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                            keyboardType: TextInputType.number,
                            controller: _okpo,
                            decoration: InputDecoration(
                                hintStyle: _textStle, hintText: "ОКПО")),
                        const SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Card(
                shadowColor: Colors.black,
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.only(left: 60, right: 60),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      const Text(
                        "Расчетные счета",
                        style:
                            TextStyle(color: Colors.blueAccent, fontSize: 14),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Divider(
                        color: Colors.grey,
                        thickness: 1,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      if (paymentProvider.listWidgets.isNotEmpty)
                        ListView.separated(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    const Divider(),
                            itemCount: paymentProvider.listWidgets.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => paymentProvider
                                              .listWidgets[index]));
                                },
                                child: Container(
                                  height: 30,
                                  child: Stack(
                                    children: [
                                      Text(paymentProvider.listWidgets[index]
                                          .nomerController.text),
                                      Positioned(
                                        bottom: 0,
                                        right: 0,
                                        child: IconButton(
                                            onPressed: () {
                                              paymentProvider.deleteWidget(
                                                  paymentProvider
                                                      .listWidgets[index]);
                                            },
                                            icon: const Icon(Icons.close)),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PayMentAccount()));
                          },
                          child: const Text(
                            "ДОБАВИТЬ РАСЧЕТНЫЙ СЧЕТ",
                            style: TextStyle(color: Colors.white),
                          )),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Card(
                  shadowColor: Colors.black,
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 60, left: 60),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        const Text(
                          "Доступ",
                          style:
                              TextStyle(color: Colors.blueAccent, fontSize: 14),
                        ),
                        Stack(
                          children: [
                            TextField(
                              controller: _vladelesSotController,
                              readOnly: true,
                              onTap: () {
                                var vladelesSotrudnik = [
                                  'Администратор',
                                ];
                                showModalBottomSheet(
                                    isScrollControlled: true,
                                    context: context,
                                    builder: (context) => Container(
                                          height: MediaQuery.of(context)
                                              .size
                                              .height,
                                          color: Colors.blueAccent,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 30),
                                            child: Scaffold(
                                              appBar: AppBar(
                                                iconTheme: const IconThemeData(
                                                    color: Colors.black),
                                                backgroundColor: Colors.white,
                                                elevation: 0,
                                                title: const Text(
                                                  "Владелец-сотрудник",
                                                  style: TextStyle(
                                                      color: Colors.grey),
                                                ),
                                              ),
                                              body: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 20),
                                                child: ListView.separated(
                                                  separatorBuilder:
                                                      (BuildContext context,
                                                              int index) =>
                                                          const Divider(),
                                                  itemCount:
                                                      vladelesSotrudnik.length,
                                                  itemBuilder:
                                                      (context, index) {
                                                    return InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          _vladelesSotController
                                                                  .text =
                                                              vladelesSotrudnik[
                                                                  index];
                                                        });
                                                        Navigator.pop(context);
                                                      },
                                                      child: Card(
                                                          color: const Color
                                                                  .fromARGB(255,
                                                              201, 197, 197),
                                                          shadowColor:
                                                              Colors.black,
                                                          elevation: 3,
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  "${index + 1}: ${vladelesSotrudnik[index]}",
                                                                  style: const TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          20),
                                                                ),
                                                              ],
                                                            ),
                                                          )),
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ));
                              },
                              decoration: InputDecoration(
                                  labelText: "Владелец-сотрудник",
                                  labelStyle: _textStle),
                            ),
                            if (_vladelesSotController.text.isNotEmpty)
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _vladelesSotController.text = '';
                                        });
                                      },
                                      icon: const Icon(Icons.close)))
                          ],
                        ),
                        Stack(
                          children: [
                            TextField(
                              controller: _vladelesOtdelController,
                              readOnly: true,
                              onTap: () {
                                var vladelesOtdel = ['Основной'];
                                showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  builder: (context) => Container(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      color: Colors.blueAccent,
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 30),
                                        child: Scaffold(
                                          appBar: AppBar(
                                            backgroundColor: Colors.white,
                                            title: const Text(
                                              "Владелец-отдел",
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                            elevation: 0,
                                            iconTheme: const IconThemeData(
                                                color: Colors.black),
                                          ),
                                          body: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 20),
                                            child: ListView.separated(
                                              separatorBuilder:
                                                  (BuildContext context,
                                                          int index) =>
                                                      const Divider(),
                                              itemCount: vladelesOtdel.length,
                                              itemBuilder: (context, index) {
                                                return InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      _vladelesOtdelController
                                                              .text =
                                                          vladelesOtdel[index];
                                                    });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Card(
                                                      color:
                                                          const Color.fromARGB(
                                                              255,
                                                              201,
                                                              197,
                                                              197),
                                                      shadowColor: Colors.black,
                                                      elevation: 3,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "${index + 1}: ${vladelesOtdel[index]}",
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 20),
                                                            ),
                                                          ],
                                                        ),
                                                      )),
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                      )),
                                );
                              },
                              decoration: InputDecoration(
                                  labelText: "Владелец-отдел",
                                  labelStyle: _textStle),
                            ),
                            if (_vladelesOtdelController.text.isNotEmpty)
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _vladelesOtdelController.text = '';
                                        });
                                      },
                                      icon: const Icon(Icons.close)))
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "Доступ",
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 14),
                            ),
                            FlutterSwitch(
                                width: 50,
                                height: 25,
                                value: dostupSwitcher,
                                onToggle: (val) {
                                  setState(() {
                                    if (dostupSwitcher == false) {
                                      dostupSwitcher = val;
                                    } else {
                                      dostupSwitcher = val;
                                    }
                                  });
                                })
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
