import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/category_maker_model.dart';
import 'package:my_store/models/contragent_maker_model.dart';
import 'package:my_store/providers/category_provider.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:my_store/pages/supplier_creater_page.dart';
import 'package:my_store/providers/barcode_provider.dart';
import 'package:my_store/providers/contact_person_provider.dart';
import 'package:my_store/providers/contragent_provider.dart';
import 'package:my_store/providers/prices_provider.dart';
import 'package:my_store/providers/user_check.dart';
import 'package:my_store/screens/catergory_creator_screen.dart';
import 'package:my_store/screens/countries.dart';
import 'package:my_store/screens/postavshik_tovar.dart';
import 'package:my_store/widgets/contact_person_widget.dart';
import 'package:provider/provider.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:async/async.dart';
import '../models/product_make_model.dart';
import '../providers/counries_from_api_provider.dart';
import '../providers/shtrihcode_provider.dart';
import '../screens/counterparty_screen.dart';

class CreateProductPage extends StatefulWidget {
  ProductMaker? productMaker;
  File? image_;
  List<Category>? category;
  bool? newCategory = false;
  String? shtrihcode;

  CreateProductPage({
    Key? key,
    this.productMaker,
    this.image_,
    this.category,
    this.newCategory,
    this.shtrihcode,
  }) : super(key: key);

  @override
  State<CreateProductPage> createState() => _CreateProductPageState();
}

class _CreateProductPageState extends State<CreateProductPage> {
  final _formKey = GlobalKey<FormState>();

  late TextEditingController _naimenovanie = TextEditingController(text: '');
  late TextEditingController _gruppa = TextEditingController(text: '');
  late TextEditingController _code = TextEditingController(text: '');
  late TextEditingController _artikul = TextEditingController(text: '');
  late TextEditingController _minimal = TextEditingController(text: '0.0');
  late TextEditingController _senaZakupki = TextEditingController(text: '0.0');
  late TextEditingController _senaProdazhi = TextEditingController(text: '0.0');
  late TextEditingController _opisanie = TextEditingController(text: '');
  late TextEditingController _ves = TextEditingController(text: '');
  late TextEditingController _obyom = TextEditingController(text: '');
  late TextEditingController _edinisaIzmereniy =
      TextEditingController(text: '');
  late TextEditingController _nesOstatok = TextEditingController(text: '');
  late TextEditingController _kodVidaProd = TextEditingController(text: '');
  late TextEditingController _emkostTari = TextEditingController(text: '');
  late TextEditingController _krepost = TextEditingController(text: '');
  late TextEditingController _minimalCurrency = TextEditingController(text: '');
  late TextEditingController _senaZakupkiCurrency =
      TextEditingController(text: '');
  late TextEditingController _prodazhCurrency = TextEditingController(text: '');
  late TextEditingController _strana = TextEditingController(text: '');
  late TextEditingController _shtrihcodeController =
      TextEditingController(text: '');
  late TextEditingController _upakovka = TextEditingController(text: '');
  late TextEditingController _kolVoUpakovka = TextEditingController(text: '1');
  late TextEditingController _nds = TextEditingController(text: '');
  late TextEditingController _postavshikController =
      TextEditingController(text: '');
  late TextEditingController _vladelesSot = TextEditingController(text: '');
  late TextEditingController _vladelesOtdel = TextEditingController(text: '');

  bool checkBottomSheet = false;
  FocusNode? focusNode;
  File? image;
  late bool _archivStatus = false;
  late bool _uchetPOoSerNomeram = false;
  late bool _vedovoyTovar = false;
  late bool _alhogolProduct = false;
  late bool _aksiaMark = false;
  late bool _obshiyDostup = true;
  late Map<String, int> categoryDropDown = {};
  late String categoryId = '';

  int? supplierId;
  double procentPribili = 0.0;

  @override
  void initState() {
    focusNode = FocusNode();
    if (widget.productMaker != null) {
      _naimenovanie =
          TextEditingController(text: widget.productMaker?.naimenovanie);
      _gruppa = TextEditingController(text: widget.productMaker?.groupe);
      _code = TextEditingController(text: widget.productMaker?.code);
      _artikul = TextEditingController(text: widget.productMaker?.artikul);
      _minimal = TextEditingController(
          text: widget.productMaker?.minimalPrice.toString());
      _senaZakupki = TextEditingController(
          text: widget.productMaker?.zakupkiPrice.toString());
      _senaProdazhi = TextEditingController(
          text: widget.productMaker?.prodazhiPrice.toString());

      _opisanie = TextEditingController(text: widget.productMaker?.opisanie);
      _ves = TextEditingController(text: widget.productMaker?.ves);
      _obyom = TextEditingController(text: widget.productMaker?.obyom);
      _edinisaIzmereniy =
          TextEditingController(text: widget.productMaker?.edIzmereniy);
      _nesOstatok =
          TextEditingController(text: widget.productMaker?.neSnizhemiyOstatok);
      _minimalCurrency =
          TextEditingController(text: widget.productMaker?.minimalCurrency);
      _senaZakupkiCurrency =
          TextEditingController(text: widget.productMaker?.zakupkiCurrency);
      _prodazhCurrency =
          TextEditingController(text: widget.productMaker?.prodazhiCurrency);
      _strana = TextEditingController(text: widget.productMaker?.strana);
      _kodVidaProd =
          TextEditingController(text: widget.productMaker!.kodVidaProduct);
      _emkostTari = TextEditingController(text: widget.productMaker!.emkost);
      _krepost = TextEditingController(text: widget.productMaker!.krepost);
      _shtrihcodeController =
          TextEditingController(text: widget.productMaker?.shtrihkod);
      _upakovka = TextEditingController(text: widget.productMaker?.upakovka);
      _kolVoUpakovka =
          TextEditingController(text: widget.productMaker?.kolvoUpakovka);
      _nds = TextEditingController(text: widget.productMaker?.nds);
      _postavshikController =
          TextEditingController(text: widget.productMaker?.postavshik);

      _vladelesSot =
          TextEditingController(text: widget.productMaker?.vladelesSot);

      _vladelesOtdel =
          TextEditingController(text: widget.productMaker?.vladalesOtdel);

      _kodVidaProd =
          TextEditingController(text: widget.productMaker?.kodVidaProduct);
      _emkostTari = TextEditingController(text: widget.productMaker?.emkost);
      _krepost = TextEditingController(text: widget.productMaker?.krepost);
      _uchetPOoSerNomeram =
          widget.productMaker!.uchetPoSerNomeram == 0 ? false : true;
      _vedovoyTovar = widget.productMaker!.vesTovar == 0 ? false : true;
      _alhogolProduct = widget.productMaker!.alhogolProduct == 0 ? false : true;
      _aksiaMark = widget.productMaker!.aksizMark == 0 ? false : true;

      categoryId = widget.productMaker!.categoryId!;
    }
    if (widget.shtrihcode != null) {
      _shtrihcodeController.text = widget.shtrihcode!;
    }
    if (widget.productMaker != null) {
      if (widget.productMaker!.archiv == 1) {
        _archivStatus = true;
      }
      if (widget.productMaker!.postavshikId != null) {
        if (widget.productMaker!.postavshikId! > 0) {
          supplierId = widget.productMaker!.postavshikId;
        } else {
          supplierId = -1;
        }
      }

      if (widget.productMaker!.zakupkiPrice != null &&
          widget.productMaker!.prodazhiPrice != null) {
        procentPribili = ((widget.productMaker!.prodazhiPrice! * 100) /
                widget.productMaker!.zakupkiPrice!) -
            100;
        if (procentPribili.isInfinite) {
          procentPribili = 0.0;
        }
        if (procentPribili.isNaN) {
          procentPribili = 0.0;
        }
      }
    }

    100;
    image = widget.image_;

    categoryDropDown["Без категории"] = 0;
    categoryDropDown["Добавить категорию"] = 0;
    for (int i = 0; i < widget.category!.length; i++) {
      categoryDropDown[widget.category![i].name!] = widget.category![i].id!;
    }

    // fetch();
    super.initState();
  }

  Future pickImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      final imageTemp = File(image.path);
      setState(() {
        this.image = imageTemp;
      });
    } on PlatformException catch (e) {
      print("Error picture $e");
    }
  }

  Future deleteImage() async {
    try {
      setState(() {
        image = null;
      });
    } catch (e) {
      print(e);
    }
  }

  void showKeyboard() {
    focusNode?.requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    List<String> edIzmereniyList = [
      'шт',
      'гр',
      'кг',
      'см',
      'м',
      'см2',
      'м2',
      'л',
      'мг',
      'мл',
      'час',
      'мин',
      'пор'
    ];
    List<String> buttonValues = ['РУБ', "USD", 'СОМ', 'EUR'];

    var getCountryApi = Provider.of<CountriesFromApiProvider>(context);
    var getProductMakerProvider = Provider.of<ProductMakerProvider>(context);
    var getCountryProvider = Provider.of<CheckUser>(context);
    var setContragentPRovider = Provider.of<ContragentProvider>(context);
    var categoryid = Provider.of<CategoryProvider>(context);
    var _textStyleForPriceFields =
        const TextStyle(color: Colors.grey, fontSize: 14);
    var _textStyleForInfoFields =
        const TextStyle(color: Colors.grey, fontSize: 12);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blueAccent,
        iconTheme: const IconThemeData(color: Colors.white),
        leading: IconButton(
            onPressed: () {
              if (widget.newCategory == false) {
                Navigator.pop(context);
                return;
              }
              if (checkBottomSheet == true) {
                checkBottomSheet = false;
                Navigator.pop(context);
                return;
              }

              AlertDialog aler = AlertDialog(
                content: const Text("Удалить изменение"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text("Не удалять")),
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: const Text("Удалять")),
                ],
              );
              showDialog(context: context, builder: (context) => aler);
            },
            icon: const Icon(Icons.arrow_back)),
        actions: [
          TextButton(
              onPressed: () {
                showKeyboard();
              },
              child: const Text(
                "12345",
                style: TextStyle(color: Colors.white),
              )),
          TextButton(
              onPressed: () async {
                FocusManager.instance.primaryFocus?.unfocus();
                if (!_formKey.currentState!.validate() ||
                    _naimenovanie.text.trim().isEmpty) {
                  AlertDialog alert = AlertDialog(
                    title: const Text("Не заполнены обязательные поля:"),
                    content: const Text("Наименование"),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OK"),
                      )
                    ],
                  );

                  showDialog(
                      context: context,
                      builder: (context) {
                        return alert;
                      },
                      barrierDismissible: false);
                  return;
                }
                if (_minimal.text.isEmpty) {
                  _minimal = TextEditingController(text: '0.0');
                }
                if (_senaZakupki.text.isEmpty) {
                  _senaZakupki = TextEditingController(text: '0.0');
                }
                if (_senaProdazhi.text.isEmpty) {
                  _senaZakupki = TextEditingController(text: '0.0');
                }

                widget.productMaker ??= ProductMaker(
                    image: image,
                    archiv: _archivStatus == false ? 0 : 1,
                    naimenovanie: _naimenovanie.text,
                    groupe: _gruppa.text,
                    code: _code.text,
                    artikul: _artikul.text,
                    minimalPrice: double.parse(_minimal.text),
                    minimalCurrency: _minimalCurrency.text,
                    zakupkiPrice: double.parse(_senaZakupki.text),
                    zakupkiCurrency: _senaZakupkiCurrency.text,
                    prodazhiPrice: double.parse(_senaProdazhi.text),
                    prodazhiCurrency: _prodazhCurrency.text,
                    opisanie: _opisanie.text,
                    strana: _strana.text,
                    ves: _ves.text,
                    obyom: _obyom.text,
                    edIzmereniy: _edinisaIzmereniy.text,
                    nds: _nds.text,
                    neSnizhemiyOstatok: _nesOstatok.text,
                    postavshik: _postavshikController.text,
                    postavshikId: supplierId ?? -1,
                    shtrihkod: _shtrihcodeController.text.isEmpty
                        ? null
                        : _shtrihcodeController.text,
                    uchetPoSerNomeram: _uchetPOoSerNomeram == false ? 0 : 1,
                    vesTovar: _vedovoyTovar == false ? 0 : 1,
                    alhogolProduct: _alhogolProduct == false ? 0 : 1,
                    aksizMark: _aksiaMark == false ? 0 : 1,
                    kodVidaProduct: _kodVidaProd.text,
                    emkost: _emkostTari.text,
                    krepost: _krepost.text,
                    upakovka: _upakovka.text,
                    kolvoUpakovka: _kolVoUpakovka.text,
                    vladelesSot: _vladelesSot.text,
                    vladalesOtdel: _vladelesOtdel.text,
                    categoryId: categoryId);

                if (!getProductMakerProvider
                    .checkProductMaker(widget.productMaker!)) {
                  if (getProductMakerProvider.getNotArchiveTovar().any(
                      (element) =>
                          element.shtrihkod == _shtrihcodeController.text)) {
                    AlertDialog newDialogForShtrihcodeError = AlertDialog(
                      title: const Text("Есть продукт с этим штрих-кодом"),
                      content:
                          const Text("Измените штрих-код, а затем сохраните"),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text("OK")),
                      ],
                    );

                    showDialog(
                        context: context,
                        builder: (context) => newDialogForShtrihcodeError);
                    return;
                  }
                  await ProductMakerDb.insertTovar(widget.productMaker!);

                  print("ID: ${widget.productMaker!.id}");
                  getProductMakerProvider.set(widget.productMaker!);

                  Navigator.pop(context);
                  return;
                }
                //инамча бояд класса обновить кни
                ///обнова
                //

                if (int.parse(_kolVoUpakovka.text) !=
                    int.parse(widget.productMaker!.kolvoUpakovka!)) {
                  AlertDialog newAlertDialog = AlertDialog(
                    content: const Text("Вы уверены?"),
                    title: const Text("Количество товара будет обнавлен"),
                    actions: [
                      TextButton(
                          onPressed: () async {

                            updateClass();
                            getProductMakerProvider.notify();
                            await ProductMakerDb.updateTovar(
                                widget.productMaker!);

                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: const Text("Да")),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                            return;
                          },
                          child: const Text("Нет"))
                    ],
                  );
                  showDialog(
                      context: context, builder: (context) => newAlertDialog);
                  return;
                }
                var rounded = ((widget.productMaker!.quantity));

                var sumOfQuantityofShtuk =
                    (((rounded - rounded.toInt()) * 100).round() / 100);
                print("shit rounded : $sumOfQuantityofShtuk");
                print(
                    "sumOfQuantityofShtuk ______ : ${(sumOfQuantityofShtuk * int.parse(_kolVoUpakovka.text)).round()}");

                sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                        int.parse(widget.productMaker!.kolvoUpakovka!))
                    .roundToDouble();

                var quantity =
                    (rounded.toInt() * int.parse(_kolVoUpakovka.text) +
                            sumOfQuantityofShtuk) /
                        int.parse(_kolVoUpakovka.text);
                print(
                    "Уп - ${rounded.toInt()} шт - ${sumOfQuantityofShtuk} кол - ${quantity} при ${_kolVoUpakovka.text}");

                widget.productMaker!.image = image;
                widget.productMaker!.archiv = _archivStatus == false ? 0 : 1;
                widget.productMaker!.naimenovanie = _naimenovanie.text;
                widget.productMaker!.groupe = _gruppa.text;
                widget.productMaker!.code = _code.text;
                widget.productMaker!.artikul = _artikul.text;
                widget.productMaker!.minimalPrice = double.parse(_minimal.text);
                widget.productMaker!.minimalCurrency = _minimalCurrency.text;
                widget.productMaker!.zakupkiPrice =
                    double.parse(_senaZakupki.text);
                widget.productMaker!.zakupkiCurrency =
                    _senaZakupkiCurrency.text;
                widget.productMaker!.prodazhiPrice =
                    double.parse(_senaProdazhi.text);
                widget.productMaker!.prodazhiCurrency = _prodazhCurrency.text;
                widget.productMaker!.opisanie = _opisanie.text;
                widget.productMaker!.strana = _strana.text;
                widget.productMaker!.ves = _ves.text;
                widget.productMaker!.obyom = _obyom.text;
                widget.productMaker!.edIzmereniy = _edinisaIzmereniy.text;
                widget.productMaker!.nds = _nds.text;
                widget.productMaker!.neSnizhemiyOstatok = _nesOstatok.text;
                widget.productMaker!.postavshik = _postavshikController.text;
                widget.productMaker!.postavshikId = supplierId ?? -1;
                widget.productMaker!.shtrihkod =
                    _shtrihcodeController.text.isEmpty
                        ? null
                        : _shtrihcodeController.text;
                widget.productMaker!.upakovka = _upakovka.text;
                widget.productMaker!.kolvoUpakovka = _kolVoUpakovka.text;
                widget.productMaker!.vladelesSot = _vladelesSot.text;
                widget.productMaker!.vladalesOtdel = _vladelesOtdel.text;
                widget.productMaker!.quantity = quantity;
                widget.productMaker!.uchetPoSerNomeram =
                    _uchetPOoSerNomeram == false ? 0 : 1;
                widget.productMaker!.vesTovar = _vedovoyTovar == false ? 0 : 1;
                widget.productMaker!.alhogolProduct =
                    _alhogolProduct == false ? 0 : 1;
                widget.productMaker!.aksizMark = _aksiaMark == false ? 0 : 1;
                widget.productMaker!.kodVidaProduct = _kodVidaProd.text;
                widget.productMaker!.emkost = _emkostTari.text;
                widget.productMaker!.krepost = _krepost.text;
                widget.productMaker!.categoryId = categoryId;

                getProductMakerProvider.notify();
                await ProductMakerDb.updateTovar(widget.productMaker!);

                print("Обнова кор кард");
                Navigator.pop(context);
                Navigator.pop(context);
                return;
              },
              child: const Text(
                "Сохранить",
                style: TextStyle(color: Colors.white, fontSize: 20),
              )),
          // TextButton(
          //     onPressed: () {
          //       print(categoryId);
          //     },
          //     child: const Text(
          //       "Check",
          //       style: const TextStyle(color: Colors.white),
          //     ))
          // TextButton(
          //     onPressed: () {
          //       print("${widget.productMaker!.naimenovanie}");
          //       print(getProductMakerProvider
          //           .checkProductMaker(widget.productMaker!));
          //       print("Штрихкод ${_shtrihcodeController.text}");
          //       print("Упаковка ${_upakovka.text}");
          //       print("Кол-во Упаковка ${_kolVoUpakovka.text}");
          //       print("BottomSheet:  ${checkBottomSheet}");
          //       print("BottomSheet: ${widget.productMaker!.edIzmereniy} ");
          //     },
          //     child: const Text(
          //       "Check",
          //       style: TextStyle(color: Colors.white),
          //     ))
        ],
      ),
      body: Builder(
        builder: (BuildContext context) => Form(
          key: _formKey,
          child: Scrollbar(
            child: InkWell(
              onTap: () {
                FocusManager.instance.primaryFocus!.unfocus();
              },
              child: ListView(
                children: [
                  firstCard(categoryid, _textStyleForPriceFields, buttonValues,
                      edIzmereniyList),
                  const SizedBox(
                    height: 20,
                  ),
                  secondCard(_textStyleForPriceFields, buttonValues),
                  const SizedBox(
                    height: 20,
                  ),
                  thirdCard(
                      _textStyleForInfoFields,
                      setContragentPRovider,
                      getCountryProvider,
                      getCountryApi,
                      context,
                      edIzmereniyList),
                  const SizedBox(
                    height: 20,
                  ),
                  fourthCard(),
                  if (_alhogolProduct == true)
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Card(
                        shadowColor: Colors.black,
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 60, right: 60),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text("Алкогольная продукция",
                                    style: TextStyle(
                                        color: Colors.blueAccent,
                                        fontSize: 14)),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    const Expanded(
                                        child:
                                            Text("Содержить акцизную марку")),
                                    FlutterSwitch(
                                        width: 50,
                                        height: 25,
                                        value: _aksiaMark,
                                        onToggle: (val) {
                                          setState(() {
                                            _aksiaMark = val;
                                          });
                                        })
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextField(
                                  controller: _kodVidaProd,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      hintText: "Код вида продукции",
                                      hintStyle: _textStyleForInfoFields),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextField(
                                  controller: _emkostTari,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      hintText: "Емкость тары, л",
                                      hintStyle: _textStyleForInfoFields),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextField(
                                  controller: _krepost,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      hintText: "Крепость, %",
                                      hintStyle: _textStyleForInfoFields),
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                              ]),
                        ),
                      ),
                    ),
                  const SizedBox(
                    height: 20,
                  ),
                  Card(
                      shadowColor: Colors.black,
                      elevation: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 60, right: 60),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 20,
                            ),
                            const Text(
                              "Доступ",
                              style: TextStyle(
                                  color: Colors.blueAccent, fontSize: 14),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Stack(
                              children: [
                                TextField(
                                  controller: _vladelesSot,
                                  readOnly: true,
                                  onTap: () {
                                    var vladelesSotrudnik = ['Администратор'];
                                    showModalBottomSheet(
                                        isScrollControlled: true,
                                        context: context,
                                        builder: (context) => Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 25),
                                              child: Scaffold(
                                                  appBar: AppBar(
                                                    backgroundColor:
                                                        Colors.white,
                                                    iconTheme:
                                                        const IconThemeData(
                                                            color:
                                                                Colors.black),
                                                    elevation: 0,
                                                  ),
                                                  body: ListView.separated(
                                                    itemBuilder:
                                                        (context, index) {
                                                      return InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              _vladelesSot
                                                                      .text =
                                                                  vladelesSotrudnik[
                                                                      index];
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Card(
                                                              color: const Color
                                                                      .fromARGB(
                                                                  255,
                                                                  201,
                                                                  197,
                                                                  197),
                                                              shadowColor:
                                                                  Colors.black,
                                                              elevation: 3,
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .all(
                                                                        8.0),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                      "${index + 1}: ${vladelesSotrudnik[index]}",
                                                                      style: const TextStyle(
                                                                          color: Colors
                                                                              .black,
                                                                          fontSize:
                                                                              20),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )));
                                                    },
                                                    itemCount: vladelesSotrudnik
                                                        .length,
                                                    separatorBuilder:
                                                        (BuildContext context,
                                                                int index) =>
                                                            const Divider(),
                                                  )),
                                            ));
                                  },
                                  decoration: const InputDecoration(
                                      labelText: "Владелец-сотрудник",
                                      labelStyle: TextStyle(
                                          color: Colors.grey, fontSize: 14)),
                                ),
                                if (_vladelesSot.text.isNotEmpty)
                                  Positioned(
                                      bottom: 0,
                                      right: 0,
                                      child: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _vladelesSot.text = '';
                                            });
                                          },
                                          icon: const Icon(Icons.close,
                                              color: Colors.grey)))
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Stack(
                              children: [
                                TextField(
                                    onTap: () {
                                      var vladelesOtdel = ['Основной'];
                                      showModalBottomSheet(
                                          isScrollControlled: true,
                                          context: context,
                                          builder: (context) => Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 25),
                                                child: Scaffold(
                                                    appBar: AppBar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      elevation: 0,
                                                      iconTheme:
                                                          const IconThemeData(
                                                              color:
                                                                  Colors.black),
                                                    ),
                                                    body: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 10),
                                                      child: ListView.separated(
                                                        separatorBuilder:
                                                            (BuildContext
                                                                        context,
                                                                    int index) =>
                                                                const Divider(),
                                                        itemCount: vladelesOtdel
                                                            .length,
                                                        itemBuilder:
                                                            (context, index) {
                                                          return InkWell(
                                                            onTap: () {
                                                              setState(() {
                                                                _vladelesOtdel
                                                                        .text =
                                                                    vladelesOtdel[
                                                                        index];
                                                              });
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: Card(
                                                                color: const Color
                                                                        .fromARGB(
                                                                    255,
                                                                    201,
                                                                    197,
                                                                    197),
                                                                shadowColor:
                                                                    Colors
                                                                        .black,
                                                                elevation: 3,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          8.0),
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        "${index + 1}: ${vladelesOtdel[index]}",
                                                                        style: const TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                            fontSize: 20),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                )),
                                                          );
                                                        },
                                                      ),
                                                    )),
                                              ));
                                    },
                                    controller: _vladelesOtdel,
                                    readOnly: true,
                                    decoration: const InputDecoration(
                                        labelText: "Владелец-отдел",
                                        labelStyle: TextStyle(
                                            color: Colors.grey, fontSize: 14))),
                                if (_vladelesOtdel.text.isNotEmpty)
                                  Positioned(
                                    right: 0,
                                    bottom: 0,
                                    child: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            _vladelesOtdel.text = '';
                                          });
                                        },
                                        icon: const Icon(
                                          Icons.close,
                                          color: Colors.grey,
                                        )),
                                  )
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                const Expanded(child: Text("Общий доступ")),
                                FlutterSwitch(
                                    height: 25,
                                    width: 50,
                                    value: _obshiyDostup,
                                    onToggle: (val) {
                                      setState(() {
                                        _obshiyDostup = val;
                                      });
                                    })
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Card fourthCard() {
    return Card(
      shadowColor: Colors.black,
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.only(left: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            const Text(
              "Тип товара",
              style: TextStyle(color: Colors.blueAccent, fontSize: 14),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(child: Text("Учет по серейным номерам")),
                Expanded(
                  child: FlutterSwitch(
                      width: 50,
                      height: 25,
                      value: _uchetPOoSerNomeram,
                      onToggle: (val) {
                        setState(() {
                          _uchetPOoSerNomeram = val;

                          _vedovoyTovar = false;
                          _alhogolProduct = false;
                        });
                      }),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(child: Text("Весовой товар")),
                Expanded(
                  child: FlutterSwitch(
                      width: 50,
                      height: 25,
                      value: _vedovoyTovar,
                      onToggle: (val) {
                        setState(() {
                          _vedovoyTovar = val;
                          _uchetPOoSerNomeram = false;
                          _alhogolProduct = false;
                        });
                      }),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(child: Text("Алкогольная продукция")),
                Expanded(
                  child: FlutterSwitch(
                      width: 50,
                      height: 25,
                      value: _alhogolProduct,
                      onToggle: (val) {
                        setState(() {
                          _alhogolProduct = val;
                          _vedovoyTovar = false;
                          _uchetPOoSerNomeram = false;
                          _aksiaMark = false;
                          _kodVidaProd = TextEditingController(text: "");
                          _emkostTari = TextEditingController(text: "");
                          _krepost = TextEditingController(text: "");
                        });
                      }),
                )
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  void updateClass() {
    widget.productMaker!.image = image;
    widget.productMaker!.archiv = _archivStatus == false ? 0 : 1;
    widget.productMaker!.naimenovanie = _naimenovanie.text;
    widget.productMaker!.groupe = _gruppa.text;
    widget.productMaker!.code = _code.text;
    widget.productMaker!.artikul = _artikul.text;
    widget.productMaker!.minimalPrice = double.parse(_minimal.text);
    widget.productMaker!.minimalCurrency = _minimalCurrency.text;
    widget.productMaker!.zakupkiPrice = double.parse(_senaZakupki.text);
    widget.productMaker!.zakupkiCurrency = _senaZakupkiCurrency.text;
    widget.productMaker!.prodazhiPrice = double.parse(_senaProdazhi.text);
    widget.productMaker!.prodazhiCurrency = _prodazhCurrency.text;
    widget.productMaker!.opisanie = _opisanie.text;
    widget.productMaker!.strana = _strana.text;
    widget.productMaker!.ves = _ves.text;
    widget.productMaker!.obyom = _obyom.text;
    widget.productMaker!.edIzmereniy = _edinisaIzmereniy.text;
    widget.productMaker!.nds = _nds.text;
    widget.productMaker!.neSnizhemiyOstatok = _nesOstatok.text;
    widget.productMaker!.postavshik = _postavshikController.text;
    widget.productMaker!.postavshikId = supplierId ?? -1;
    widget.productMaker!.shtrihkod =
        _shtrihcodeController.text.isEmpty ? null : _shtrihcodeController.text;
    widget.productMaker!.upakovka = _upakovka.text;
    widget.productMaker!.kolvoUpakovka = _kolVoUpakovka.text;
    widget.productMaker!.vladelesSot = _vladelesSot.text;
    widget.productMaker!.vladalesOtdel = _vladelesOtdel.text;
    widget.productMaker!.quantity = 0;
    widget.productMaker!.uchetPoSerNomeram =
        _uchetPOoSerNomeram == false ? 0 : 1;
    widget.productMaker!.vesTovar = _vedovoyTovar == false ? 0 : 1;
    widget.productMaker!.alhogolProduct = _alhogolProduct == false ? 0 : 1;
    widget.productMaker!.aksizMark = _aksiaMark == false ? 0 : 1;
    widget.productMaker!.kodVidaProduct = _kodVidaProd.text;
    widget.productMaker!.emkost = _emkostTari.text;
    widget.productMaker!.krepost = _krepost.text;
    widget.productMaker!.categoryId = categoryId;
  }

  Card thirdCard(
      TextStyle _textStyleForInfoFields,
      ContragentProvider setContragentPRovider,
      CheckUser getCountryProvider,
      CountriesFromApiProvider getCountryApi,
      BuildContext context,
      List<String> edIzmereniyList) {
    return Card(
      shadowColor: Colors.black,
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.only(left: 60, right: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Информация",
              style: TextStyle(color: Colors.blueAccent, fontSize: 14),
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "Архивный",
                  style: TextStyle(color: Colors.grey),
                ),
                FlutterSwitch(
                    width: 50,
                    height: 25,
                    value: _archivStatus,
                    onToggle: (val) {
                      setState(() {
                        _archivStatus = val;
                      });
                    })
              ],
            ),
            TextField(
              controller: _opisanie,
              decoration: InputDecoration(
                  hintText: "Описание", hintStyle: _textStyleForInfoFields),
            ),
            TextField(
              controller: _strana,
              decoration: InputDecoration(
                  hintText: "Страна/Регион",
                  hintStyle: _textStyleForInfoFields),
              readOnly: true,
              onTap: () {
                // if (getCountryProvider.country.text.isNotEmpty) {
                //   return;
                // }
                checkBottomSheet = true;
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    duration: const Duration(seconds: 2),
                    content: Row(
                      children: [
                        Container(
                          width: 20,
                          height: 20,
                          child: const CircularProgressIndicator(
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 15),
                        const Text("Загрузка...",
                            style: TextStyle(color: Colors.white))
                      ],
                    )));
                Timer(
                    const Duration(seconds: 2),
                    () => Scaffold.of(context).showBottomSheet((context) {
                          return getCountryApi.names.isEmpty
                              ? Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      CircularProgressIndicator(
                                        color: Colors.blueAccent,
                                      ),
                                      Text("Loading...")
                                    ],
                                  ),
                                )
                              : Scrollbar(
                                  child: ListView.separated(
                                    itemCount: getCountryApi.names.length,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                            const Divider(),
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                          onTap: () {
                                            setState(() {
                                              _strana.text =
                                                  getCountryApi.names[index];
                                            });
                                            checkBottomSheet = false;
                                            Navigator.pop(context);
                                          },
                                          child: Text(
                                            getCountryApi.names[index],
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 20),
                                          ));
                                    },
                                  ),
                                );
                        }));
              },
            ),
            if (getCountryProvider.country.text.isNotEmpty)
              Positioned(
                right: 0,
                bottom: 0,
                child: IconButton(
                    onPressed: () {
                      getCountryProvider
                          .setCountry(TextEditingController(text: ''));
                    },
                    icon: const Icon(
                      Icons.close,
                      color: Colors.grey,
                    )),
              ),
            TextField(
              controller: _ves,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Вес",
                hintStyle: _textStyleForInfoFields,
              ),
            ),
            TextField(
              controller: _obyom,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: "Объем", hintStyle: _textStyleForInfoFields),
            ),
            TextField(
              readOnly: true,
              controller: _edinisaIzmereniy,
              onTap: () {
                checkBottomSheet = true;
                Scaffold.of(context).showBottomSheet((context) {
                  return ListView.separated(
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                      itemCount: edIzmereniyList.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(
                              left: 10, top: 10, bottom: 10),
                          child: InkWell(
                              onTap: () {
                                setState(() {
                                  _edinisaIzmereniy.text =
                                      edIzmereniyList[index];
                                });
                                checkBottomSheet = false;
                                Navigator.pop(context);
                              },
                              child: Text(
                                edIzmereniyList[index],
                                style: const TextStyle(fontSize: 20),
                              )),
                        );
                      });
                });
              },
              decoration: InputDecoration(
                  hintText: "Единица измерений",
                  hintStyle: _textStyleForInfoFields),
            ),
            TextField(
              controller: _nds,
              readOnly: true,
              onTap: () {
                checkBottomSheet = true;
                List<String> nds = ['0', 'Без НДС', '10%', '18%', "20%"];
                Scaffold.of(context).showBottomSheet((context) {
                  return ListView.separated(
                      itemCount: nds.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              _nds.text = nds[index];
                            });
                            checkBottomSheet = false;
                            Navigator.pop(context);
                          },
                          child: Text(
                            nds[index],
                            style: const TextStyle(fontSize: 20),
                          ),
                        );
                      });
                });
              },
              decoration: const InputDecoration(
                  labelText: "НДС, %",
                  labelStyle: TextStyle(color: Colors.grey, fontSize: 14)),
            ),
            TextField(
              keyboardType: TextInputType.number,
              controller: _nesOstatok,
              decoration: InputDecoration(
                  hintText: "Не снижаемый остаток",
                  hintStyle: _textStyleForInfoFields),
            ),
            Stack(
              children: [
                TextField(
                  controller: _postavshikController,
                  readOnly: true,
                  onTap: () {
                    showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        builder: (context) => DefaultTabController(
                              length: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 25),
                                child: Scaffold(
                                  floatingActionButton: FloatingActionButton(
                                      child: const Text("+",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20)),
                                      backgroundColor: Colors.pink,
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SupplierCreatorPage(
                                                      contactWidget: [],
                                                    )));
                                      }),
                                  appBar: AppBar(
                                    backgroundColor: Colors.white,
                                    elevation: 0,
                                    iconTheme: const IconThemeData(
                                        color: Colors.black),
                                    bottom: const TabBar(
                                        // isScrollable: true,
                                        tabs: [
                                          Tab(
                                            child: Text("КОНТРАГЕНТЫ"),
                                          ),
                                          Tab(
                                            child: Text("ЮРЛИЦА"),
                                          )
                                        ]),
                                    // preferredSize: Size.fromHeight(kToolbarHeight),
                                  ),
                                  body: TabBarView(children: [
                                    ListView.separated(
                                        separatorBuilder:
                                            (BuildContext context, int index) =>
                                                const Divider(
                                                  color: Colors.grey,
                                                  thickness: 1,
                                                ),
                                        itemCount:
                                            setContragentPRovider.list.length,
                                        itemBuilder: (context, index) {
                                          return InkWell(
                                            onTap: () {
                                              setState(() {
                                                _postavshikController.text =
                                                    setContragentPRovider
                                                        .list[index]
                                                        .naimenovanie!;
                                                supplierId =
                                                    setContragentPRovider
                                                        .list[index].id;

                                                print(supplierId);
                                              });
                                              Navigator.pop(context);
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10, top: 10),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        setContragentPRovider
                                                            .list[index]
                                                            .naimenovanie!,
                                                        style: const TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 10, top: 10),
                                                    child: Text(
                                                      setContragentPRovider
                                                          .list[index].status!,
                                                      style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18),
                                                    ))
                                              ],
                                            ),
                                          );
                                        }),
                                    const Center(
                                      child: Text("Hello"),
                                    )
                                  ]),
                                ),
                              ),
                            ));
                  },
                  decoration: InputDecoration(
                      hintText: "Поставщик",
                      hintStyle: _textStyleForInfoFields),
                ),
                if (supplierId == -1)
                  const Text("")
                else
                  Positioned(
                      right: 0,
                      bottom: 0,
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              _postavshikController.text = '';
                              supplierId = -1;
                            });
                          },
                          icon: const Icon(Icons.close)))
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              decoration: const InputDecoration(
                  hintText: "Артикул",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black))),
              controller: _artikul,
            ),
            const SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }

  Card secondCard(
      TextStyle _textStyleForPriceFields, List<String> buttonValues) {
    return Card(
      shadowColor: Colors.black,
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.only(left: 60, right: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Цены",
              style: TextStyle(color: Colors.blueAccent, fontSize: 14),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 100,
                  child: TextField(
                    controller: _minimal,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelStyle: _textStyleForPriceFields,
                        labelText: "Минимальная"),
                  ),
                ),
                SizedBox(
                  width: 100,
                  child: Stack(
                    children: [
                      TextField(
                        controller: _minimalCurrency,
                        onTap: () {
                          if (_minimalCurrency.text.isNotEmpty) {
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text("Сначала удалите валюту")));
                            return;
                          }
                        },
                        readOnly: true,
                        decoration: InputDecoration(
                            labelStyle: _textStyleForPriceFields,
                            labelText: "Валюта"),
                      ),
                      if (_minimalCurrency.text.isNotEmpty)
                        Positioned(
                            bottom: -5,
                            right: 0,
                            child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    _minimalCurrency.text = '';
                                  });
                                },
                                icon: const Icon(
                                  Icons.close,
                                  size: 15,
                                )))
                      else
                        Positioned(
                            bottom: -5,
                            right: 0,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                  icon: const Icon(
                                    Icons.more_vert,
                                    color: Colors.black,
                                  ),
                                  items: buttonValues
                                      .map((e) => DropdownMenuItem(
                                            child: Text(e),
                                            value: e,
                                          ))
                                      .toList(),
                                  onChanged: (val) {
                                    setState(() {
                                      _minimalCurrency.text = val.toString();
                                    });
                                  }),
                            ))
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }

  Card firstCard(
      CategoryProvider categoryid,
      TextStyle _textStyleForPriceFields,
      List<String> buttonValues,
      List<String> edIzmereniyList) {
    return Card(
      elevation: 2,
      shadowColor: Colors.black,
      borderOnForeground: false,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (image.toString() != "File: 'null'" && image != null)
                Center(
                  child: Column(
                    children: [
                      Container(
                          width: 200, height: 100, child: Image.file(image!)),
                      Row(
                        children: [
                          TextButton(
                              onPressed: () {
                                //
                                //
                                pickImage();
                                //
                                //
                              },
                              child: const Text("Изменить")),
                          TextButton(
                              onPressed: () {
                                //
                                //
                                deleteImage();
                                //
                                //
                              },
                              child: const Text(
                                "Удалить",
                                style: TextStyle(color: Colors.redAccent),
                              )),
                        ],
                      )
                    ],
                  ),
                )
              else
                TextButton(
                    onPressed: () async {
                      //
                      pickImage();
                      //
                    },
                    child: const Text("Добавить фото"))
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 60, right: 60),
            child: Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: [
                    TextField(
                      onTap: () {
                        setState(() {});
                        _shtrihcodeController.text = '';
                      },
                      controller: _shtrihcodeController,
                      focusNode: focusNode,
                      decoration: const InputDecoration(
                          labelText: "Штрихкод",
                          labelStyle:
                              TextStyle(color: Colors.grey, fontSize: 14)),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: IconButton(
                          onPressed: () {
                            FlutterBarcodeScanner.scanBarcode(
                                    "#000000", "Cancel", true, ScanMode.BARCODE)
                                .then((value) =>
                                    _shtrihcodeController.text = value);
                          },
                          icon: const Icon(
                            Icons.camera_alt,
                            color: Colors.blueGrey,
                          )),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // ElevatedButton(
                    //     onPressed: () {
                    //       setWidget.addWidget();
                    //     },
                    //     child: const Text("ДОБАВИТЬ ШТРИХКОД")),
                  ],
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.trim().isEmpty) {
                      return "Поле не может быть пустым";
                    }
                  },
                  decoration: const InputDecoration(
                      labelText: "Наименование",
                      labelStyle: TextStyle(color: Colors.grey, fontSize: 15),
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black))),
                  controller: _naimenovanie,
                ),
                const SizedBox(
                  height: 15,
                ),

                Stack(
                  children: [
                    TextField(
                      decoration: const InputDecoration(
                          labelText: "Категории",
                          labelStyle:
                              TextStyle(color: Colors.grey, fontSize: 15),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                      controller: _gruppa,
                      readOnly: true,
                      onTap: () {},
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          items: categoryDropDown
                              .map((key, value) {
                                return MapEntry(
                                    key,
                                    DropdownMenuItem(
                                      value: key,
                                      onTap: () {
                                        setState(() {
                                          categoryId = value.toString().trim();
                                          print(categoryId);
                                        });
                                      },
                                      child: Text(key),
                                    ));
                              })
                              .values
                              .toList(),
                          onChanged: (val) async {
                            var g = categoryDropDown.entries.toList();
                            if (val.toString() == g[1].key) {
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CategoryCreatorScreen(
                                            // productPage:widget,
                                            groupController: _gruppa,
                                            dropDownButtonList:
                                                categoryDropDown,
                                            fromTovarPage: true,
                                          )));

                              categoryId = categoryid.categoryId;
                              return;
                            }
                            setState(() {
                              _gruppa.text = val.toString();
                            });
                          },
                        ),
                      ),
                    )
                  ],
                ),
                // const SizedBox(
                //   height: 15,
                // ),
                // TextFormField(
                //   validator: (val) {
                //     if (val!.isEmpty) {
                //       return "Полене может быть пустым";
                //     }
                //   },
                //   decoration: const InputDecoration(
                //       hintText: "Код",
                //       hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
                //       border: UnderlineInputBorder(
                //           borderSide: BorderSide(color: Colors.black))),
                //   controller: _code,
                // ),

                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 100,
                      child: TextField(
                        controller: _kolVoUpakovka,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            labelText: "Кол-во",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 14)),
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      child: TextField(
                        readOnly: true,
                        onTap: () {
                          showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (context) => Container(
                                    height: MediaQuery.of(context).size.height,
                                    color: Colors.blueAccent,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 25),
                                      child: Scaffold(
                                          appBar: AppBar(
                                            backgroundColor: Colors.blueAccent,
                                            elevation: 0,
                                            leading: IconButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                icon: const Icon(
                                                    Icons.arrow_back)),
                                            iconTheme: const IconThemeData(
                                                color: Colors.white),
                                          ),
                                          body: ListView.separated(
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10,
                                                    top: 10,
                                                    bottom: 10),
                                                child: InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        _upakovka.text =
                                                            edIzmereniyList[
                                                                index];
                                                      });
                                                      Navigator.pop(context);
                                                    },
                                                    child: Text(
                                                      edIzmereniyList[index],
                                                      style: const TextStyle(
                                                          fontSize: 20),
                                                    )),
                                              );
                                            },
                                            itemCount: edIzmereniyList.length,
                                            separatorBuilder:
                                                (BuildContext context,
                                                        int index) =>
                                                    const Divider(),
                                          )),
                                    ),
                                  ));
                        },
                        controller: _upakovka,
                        decoration: const InputDecoration(
                            labelText: "Упаковка*",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 14)),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 100,
                      child: TextFormField(
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Пустое поле";
                          }
                        },
                        onTap: () {
                          _senaProdazhi.text = '';
                        },
                        onChanged: (val) {
                          setState(() {});
                          procentPribili = ((double.parse(
                                          _senaProdazhi.text.isEmpty
                                              ? '0.0'
                                              : _senaProdazhi.text) *
                                      100) /
                                  double.parse(_senaZakupki.text.isEmpty
                                      ? "0.0"
                                      : _senaZakupki.text)) -
                              100;

                          if (procentPribili.isInfinite) {
                            procentPribili = 0.0;
                          }
                          if (procentPribili.isNaN) {
                            procentPribili = 0.0;
                          }
                        },
                        controller: _senaProdazhi,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelStyle: _textStyleForPriceFields,
                            labelText: "Цена продажи"),
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      child: Stack(
                        children: [
                          TextField(
                            readOnly: true,
                            controller: _prodazhCurrency,
                            onTap: () {
                              if (_prodazhCurrency.text.isNotEmpty) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content:
                                            Text("Сначала удалите валюту")));
                                return;
                              }
                            },
                            decoration: InputDecoration(
                                labelStyle: _textStyleForPriceFields,
                                labelText: "Валюта"),
                          ),
                          if (_prodazhCurrency.text.isNotEmpty)
                            Positioned(
                                bottom: -5,
                                right: 0,
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _prodazhCurrency.text = '';
                                      });
                                    },
                                    icon: const Icon(
                                      Icons.close,
                                      size: 15,
                                    )))
                          else
                            Positioned(
                                bottom: -5,
                                right: 0,
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                      icon: const Icon(
                                        Icons.more_vert,
                                        color: Colors.black,
                                      ),
                                      items: buttonValues
                                          .map((e) => DropdownMenuItem(
                                                child: Text(e),
                                                value: e,
                                              ))
                                          .toList(),
                                      onChanged: (val) {
                                        setState(() {
                                          _prodazhCurrency.text =
                                              val.toString();
                                        });
                                      }),
                                ))
                        ],
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 100,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: _senaZakupki,
                        onTap: () {
                          setState(() {});
                          _senaZakupki.text = '';
                        },
                        onChanged: (val) {
                          setState(() {});
                          setState(() {});
                          procentPribili = ((double.parse(
                                          _senaProdazhi.text.isEmpty
                                              ? '0.0'
                                              : _senaProdazhi.text) *
                                      100) /
                                  double.parse(_senaZakupki.text.isEmpty
                                      ? "0.0"
                                      : _senaZakupki.text)) -
                              100;

                          if (procentPribili.isInfinite) {
                            procentPribili = 0.0;
                          }
                          if (procentPribili.isNaN) {
                            procentPribili = 0.0;
                          }
                        },
                        decoration: InputDecoration(
                            labelStyle: _textStyleForPriceFields,
                            labelText: "Цена закупки"),
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      child: Stack(
                        children: [
                          TextField(
                            readOnly: true,
                            controller: _senaZakupkiCurrency,
                            onTap: () {
                              if (_senaZakupkiCurrency.text.isNotEmpty) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content:
                                            Text("Сначала удалите валюту")));
                                return;
                              }
                            },
                            decoration: InputDecoration(
                                labelStyle: _textStyleForPriceFields,
                                labelText: "Валюта"),
                          ),
                          if (_senaZakupkiCurrency.text.isNotEmpty)
                            Positioned(
                                bottom: -5,
                                right: 0,
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _senaZakupkiCurrency.text = '';
                                      });
                                    },
                                    icon: const Icon(
                                      Icons.close,
                                      size: 15,
                                    )))
                          else
                            Positioned(
                                bottom: -5,
                                right: 0,
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                      icon: const Icon(
                                        Icons.more_vert,
                                        color: Colors.black,
                                      ),
                                      items: buttonValues
                                          .map((e) => DropdownMenuItem(
                                                child: Text(e),
                                                value: e,
                                              ))
                                          .toList(),
                                      onChanged: (val) {
                                        setState(() {
                                          _senaZakupkiCurrency.text =
                                              val.toString();
                                        });
                                      }),
                                ))
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 60),
                child: Text("${procentPribili.toStringAsFixed(2)}%"),
              )),
          const SizedBox(
            height: 25,
          ),
        ],
      ),
    );
  }
}
