import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/supplier_invoice_model.dart';
import 'package:my_store/providers/product_cart_provider.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:my_store/providers/supplier_invoice_details_provider.dart';
import 'package:my_store/screens/supplier_each_detail_screen.dart';
import 'package:my_store/widgets/all_supplier_invoice_details_search.dart';
import 'package:my_store/widgets/supplier_invoice_each_search.dart';
import 'package:my_store/widgets/supplier_returned_search_widget.dart';
import 'package:provider/provider.dart';

import '../models/contragent_maker_model.dart';
import '../models/product_make_model.dart';
import '../models/supplier_invoice_details_model.dart';
import '../providers/supplier_invoice_provider.dart';
import '../screens/change_price_screen.dart';
import '../screens/change_price_supplier_screen.dart';

class SupplierPage extends StatefulWidget {
  Contragent? contragent;
  List<ProductMaker>? listOfProduct;
  List<ProductMaker>? listOfOwnSupplierProduct;
  List<SupplierInvoice>? listOfSupplierInvoice;

  SupplierPage(
      {Key? key,
      this.contragent,
      this.listOfProduct,
      this.listOfSupplierInvoice,
      this.listOfOwnSupplierProduct})
      : super(key: key);

  @override
  State<SupplierPage> createState() => _SupplierPageState();
}

class _SupplierPageState extends State<SupplierPage> {
  List<SupplierInvoiceDetails> list = [];

  List<SupplierInvoiceDetails> supplierInvoiceDetailsList = [];
  List<SupplierInvoiceDetails> listOfProduct = [];

  double totalForNovoePostuplenie = 0.0;

  List<String> listOfDropButton = ['Отчистить'];

  List<SupplierInvoice> listOfInvoice = [];

  double changeMoney = 0.0;

  @override
  void initState() {
    print("");
    setState(() {});

    if (widget.listOfOwnSupplierProduct!.isNotEmpty) {
      for (var all in widget.listOfOwnSupplierProduct!) {
        supplierInvoiceDetailsList.add(SupplierInvoiceDetails(
            id: null,
            supplierInvoiceId: null,
            img: all.image,
            productId: all.id,
            name: all.naimenovanie,
            quantity: 0,
            price: all.zakupkiPrice,
            barcode: all.shtrihkod,
            kolvoUpakovka: int.parse(all.kolvoUpakovka!),
            returned: 0));
      }
    }

    for (var all in widget.listOfProduct!) {
      supplierInvoiceDetailsList.add(SupplierInvoiceDetails(
          id: null,
          supplierInvoiceId: null,
          img: all.image,
          productId: all.id,
          name: all.naimenovanie,
          quantity: 0,
          price: all.zakupkiPrice,
          barcode: all.shtrihkod,
          kolvoUpakovka: int.parse(all.kolvoUpakovka!),
          returned: 0));
    }
  }

  void refreshParentScreen(
    double quantity,
    SupplierInvoiceDetails supplierInvoiceDetails,
  ) {
    setState(() {
      supplierInvoiceDetailsList
          .where((element) => element == supplierInvoiceDetails)
          .first
          .quantity = quantity;

      var total = 0.0;
      supplierInvoiceDetailsList.forEach((element) {
        var rounded = ((element.quantity));

        var sumOfQuantityofShtuk =
            (((rounded! - rounded.toInt()) * 100).round() / 100);

        sumOfQuantityofShtuk =
            (sumOfQuantityofShtuk * element.kolvoUpakovka!).roundToDouble();

        total += element.price! * rounded.toInt() +
            ((sumOfQuantityofShtuk * element.price!) / element.kolvoUpakovka!);
      });

      totalForNovoePostuplenie = total;
    });
  }

  void refreshThelistOfSupplierInvoice(SupplierInvoice supplierInvoice) {
    setState(() {
      widget.listOfSupplierInvoice!
          .removeWhere((element) => element.id == supplierInvoice.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    var supplierInvoiceProvider = Provider.of<SupplierAllProvider>(context);
    var supplierInvoiceDetailsProvider =
        Provider.of<SupplierInvoiceDetailsProvider>(context);

    listOfInvoice = supplierInvoiceProvider.listOfSupplierInvoice
        .where((element) => element.supplierId == widget.contragent!.id)
        .toList()
        .where((element) => element.returned == 1)
        .toList();

    return DefaultTabController(
        length: 3,
        child: Scaffold(
            appBar: AppBar(
              title: Text("${widget.contragent!.naimenovanie}"),
              backgroundColor: Colors.blueAccent,
              elevation: 0,
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                        icon: const Icon(
                          Icons.more_vert_outlined,
                          color: Colors.white,
                        ),
                        items: listOfDropButton.map((e) {
                          return DropdownMenuItem(
                            child: Text(e),
                            value: e,
                          );
                        }).toList(),
                        onChanged: (val) {
                          if (val == listOfDropButton[0]) {
                            setState(() {});
                            supplierInvoiceDetailsList.forEach((element) {
                              element.id = null;
                              element.supplierInvoiceId = null;
                              element.quantity = 0;
                              element.returned = 0;
                            });
                            totalForNovoePostuplenie = 0.0;
                          }
                        }),
                  ),
                )
              ],
              bottom: const TabBar(
                indicatorColor: Colors.white,
                isScrollable: true,
                labelColor: Colors.white,
                tabs: [
                  Tab(
                    text: "Поступления",
                  ),
                  Tab(
                    text: "Новое поступления",
                  ),
                  Tab(
                    text: "Возврат",
                  ),
                ],
              ),
            ),
            body: TabBarView(children: [
              ///
              ///
              ///
              Column(
                children: [
                  if (widget.listOfSupplierInvoice!.isNotEmpty)
                    InkWell(
                      onTap: () {
                        showSearch(
                            context: context,
                            delegate: SupplierInvoceEachSearch(
                                supplierInvoceList:
                                    widget.listOfSupplierInvoice));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: const [
                                Icon(Icons.search),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  "Поиск",
                                  style: TextStyle(fontSize: 20),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Итог: ${supplierInvoiceProvider.getTotal(widget.contragent!.id!).toStringAsFixed(2)}",
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: widget.listOfSupplierInvoice!.length,
                        itemBuilder: (context, index) {
                          var eachSupplierInvoiceDetailes =
                              supplierInvoiceDetailsProvider
                                  .listSupplierInvoiceDetails
                                  .where((element) =>
                                      element.supplierInvoiceId ==
                                      widget.listOfSupplierInvoice![index].id)
                                  .toList();
                          return InkWell(
                            onTap: () {
                              setState(() {});
                              var eachSupplierInvoiceDetailess =
                                  supplierInvoiceDetailsProvider
                                      .listSupplierInvoiceDetails
                                      .where((element) =>
                                          element.supplierInvoiceId ==
                                          widget
                                              .listOfSupplierInvoice![index].id)
                                      .toList();

                              var total = 0.0;

                              eachSupplierInvoiceDetailess.forEach((element) {
                                if (element.returned == 0) {
                                  var rounded = ((element.quantity));

                                  var sumOfQuantityofShtuk =
                                      (((rounded! - rounded.toInt()) * 100)
                                              .round() /
                                          100);

                                  sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                                          element.kolvoUpakovka!)
                                      .roundToDouble();

                                  total += element.price! * rounded.toInt() +
                                      ((sumOfQuantityofShtuk * element.price!) /
                                          element.kolvoUpakovka!);
                                }
                              });

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SupplierEachDetailScrenn(
                                            total: total,
                                            listOfSupplierDetails:
                                                eachSupplierInvoiceDetailes,
                                            getDateTime: widget
                                                .listOfSupplierInvoice![index]
                                                .datatime,
                                            supplierInvoice: widget
                                                .listOfSupplierInvoice![index],
                                            fromReturnedPage: false,
                                            refreshTheParent:
                                                refreshThelistOfSupplierInvoice,
                                          )));
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${widget.listOfSupplierInvoice![index].id}",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Дата: ${widget.listOfSupplierInvoice![index].datatime.toString().substring(0, 11)}",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Время: ${widget.listOfSupplierInvoice![index].datatime.toString().substring(11, 19)}",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  if (eachSupplierInvoiceDetailes.every(
                                      (element) => element.returned == 1))
                                    const Text(
                                      "Возвращено",
                                      style: TextStyle(color: Colors.redAccent),
                                    )
                                  else
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        const Text(
                                          "Общая сумма",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          widget.listOfSupplierInvoice![index]
                                              .total!
                                              .toStringAsFixed(2),
                                          style: const TextStyle(
                                              fontStyle: FontStyle.normal),
                                        ),
                                      ],
                                    )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),

              ///
              ///
              ///
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      showSearch(
                          context: context,
                          delegate: AllSupplierInvoiceDetailsSearch(
                              supplierList: supplierInvoiceDetailsList,
                              refreshParent: refreshParentScreen));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: const [
                              Icon(Icons.search),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                "Поиск",
                                style: TextStyle(fontSize: 20),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Итог: ${totalForNovoePostuplenie.toStringAsFixed(2)}",
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: supplierInvoiceDetailsList.length,
                        itemBuilder: (context, index) {
                          var rounded =
                              ((supplierInvoiceDetailsList[index].quantity));

                          var sumOfQuantityofShtuk =
                              (((rounded! - rounded.toInt()) * 100).round() /
                                  100);

                          sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                                  supplierInvoiceDetailsList[index]
                                      .kolvoUpakovka!)
                              .roundToDouble();

                          var total = supplierInvoiceDetailsList[index].price! *
                                  rounded.toInt() +
                              ((sumOfQuantityofShtuk *
                                      supplierInvoiceDetailsList[index]
                                          .price!) /
                                  supplierInvoiceDetailsList[index]
                                      .kolvoUpakovka!);

                          // print(total);
                          return InkWell(
                            onTap: () {
                              setState(() {});
                              // if (supplierInvoiceDetailsList[index].price ==
                              //     0.0) {
                              //   supplierInvoiceDetailsList[index].quantity =
                              //       supplierInvoiceDetailsList[index]
                              //               .quantity! +
                              //           1;
                              //   Navigator.push(
                              //       context,
                              //       MaterialPageRoute(
                              //           builder: (context) =>
                              //               ChangePriceSupplierScreen(
                              //                 supplierInvoiceDetails:
                              //                     supplierInvoiceDetailsList[
                              //                         index],
                              //                 refreshParent:
                              //                     refreshParentScreen,
                              //               )));
                              //   EasyLoading.showError("Ошибка цены",
                              //       dismissOnTap: true);
                              //   return;
                              // }
                              supplierInvoiceDetailsList[index].quantity =
                                  supplierInvoiceDetailsList[index].quantity! +
                                      1;
                              var total = 0.0;

                              supplierInvoiceDetailsList.forEach((element) {
                                var rounded = ((element.quantity));

                                var sumOfQuantityofShtuk =
                                    (((rounded! - rounded.toInt()) * 100)
                                            .round() /
                                        100);

                                sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                                        element.kolvoUpakovka!)
                                    .roundToDouble();

                                total += element.price! * rounded.toInt() +
                                    ((sumOfQuantityofShtuk * element.price!) /
                                        element.kolvoUpakovka!);
                              });

                              totalForNovoePostuplenie = total;
                            },
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 10, top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      if (supplierInvoiceDetailsList[index]
                                                  .img
                                                  .toString() ==
                                              "File: 'null'" ||
                                          supplierInvoiceDetailsList[index]
                                                  .img ==
                                              null)
                                        InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ChangePriceSupplierScreen(
                                                          supplierInvoiceDetails:
                                                              supplierInvoiceDetailsList[
                                                                  index],
                                                          refreshParent:
                                                              refreshParentScreen,
                                                        )));
                                          },
                                          child: Container(
                                            width: 100,
                                            height: 50,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                image: AssetImage(
                                                    "assets/images/1.jpg"),
                                                fit: BoxFit.fitWidth,
                                                alignment: Alignment.bottomLeft,
                                              ),
                                            ),
                                            child: Center(
                                              child: Text(
                                                "${supplierInvoiceDetailsList[index].name![0]}"
                                                "${supplierInvoiceDetailsList[index].name![supplierInvoiceDetailsList[index].name!.length - 1]}",
                                                style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20),
                                              ),
                                            ),
                                          ),
                                        )
                                      else
                                        InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ChangePriceSupplierScreen(
                                                          supplierInvoiceDetails:
                                                              supplierInvoiceDetailsList[
                                                                  index],
                                                          refreshParent:
                                                              refreshParentScreen,
                                                        )));
                                          },
                                          child: SizedBox(
                                              width: 100,
                                              height: 50,
                                              child: Image.file(
                                                  supplierInvoiceDetailsList[
                                                          index]
                                                      .img!)),
                                        ),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              if (supplierInvoiceDetailsList[
                                                      index]
                                                  .name!
                                                  .isNotEmpty)
                                                Text(
                                                    // "${productMaker.getNotArchiveTovar()[index].code} "
                                                    "• ${supplierInvoiceDetailsList[index].name}"),
                                              const SizedBox(
                                                height: 10,
                                              ),
                                              if (supplierInvoiceDetailsList[
                                                              index]
                                                          .quantity ==
                                                      0 &&
                                                  sumOfQuantityofShtuk == 0)
                                                const Text(
                                                  "Нажмите чтобы добавить",
                                                  style: TextStyle(
                                                      color: Colors.pink),
                                                )
                                              else
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    const Icon(
                                                      Icons
                                                          .shopping_cart_checkout_outlined,
                                                      color: Colors.pink,
                                                    ),
                                                    Text(
                                                        supplierInvoiceDetailsList[
                                                                index]
                                                            .price
                                                            .toString()),
                                                    // Row(
                                                    //   crossAxisAlignment:
                                                    //       CrossAxisAlignment
                                                    //           .end,
                                                    //   children: [
                                                    // Text(
                                                    //     productMaker
                                                    //         .getNotArchiveTovar()[
                                                    //             index]
                                                    //         .prodazhiCurrency!
                                                    //         .toLowerCase(),
                                                    //     style:
                                                    //         const TextStyle(
                                                    //             color: Colors
                                                    //                 .grey,
                                                    //             fontSize:
                                                    //                 11)),
                                                    // ],
                                                    // ),
                                                    Text(
                                                      " (${supplierInvoiceDetailsList[index].quantity!.toInt().toString()}-уп",
                                                      style: const TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 14),
                                                    ),
                                                    if (sumOfQuantityofShtuk !=
                                                        0)
                                                      Text(
                                                        ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                                        style: const TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14),
                                                      ),
                                                    const Text(
                                                      ")",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 17),
                                                    )
                                                  ],
                                                )
                                            ],
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          // Navigator.push(
                                          //     context,
                                          //     MaterialPageRoute(
                                          //         builder: (context) =>
                                          //             ChangePriceScreen(
                                          //               productMaker: productMaker
                                          //                       .getNotArchiveTovar()[
                                          //                   index],
                                          //             )));
                                        },
                                        child: Text(
                                          // "1",
                                          total.toStringAsFixed(2),
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                  SizedBox(
                    height: 70,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.green)),
                        onPressed: () async {
                          if (supplierInvoiceDetailsList
                              .every((element) => element.quantity == 0)) {
                            return;
                          }
                          AlertDialog newDialog = AlertDialog(
                            content: const Text("Вы точно хотите продолжить?"),
                            title: const Text("Внимание"),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text("Нет")),
                              TextButton(
                                  onPressed: () async {
                                    var total = 0.0;
                                    DateTime newDateTime = DateTime.now();

                                    supplierInvoiceDetailsList
                                        .forEach((element) {
                                      var rounded = ((element.quantity));

                                      var sumOfQuantityofShtuk =
                                          (((rounded! - rounded.toInt()) * 100)
                                                  .round() /
                                              100);

                                      sumOfQuantityofShtuk =
                                          (sumOfQuantityofShtuk *
                                                  element.kolvoUpakovka!)
                                              .roundToDouble();

                                      total +=
                                          element.price! * rounded.toInt() +
                                              ((sumOfQuantityofShtuk *
                                                      element.price!) /
                                                  element.kolvoUpakovka!);
                                    });
                                    SupplierInvoice newSupplierInvoice =
                                        SupplierInvoice(
                                      supplierId: widget.contragent!.id,
                                      datatime: newDateTime.toString(),
                                      total: total,
                                      returned: 0,
                                      quantity: supplierInvoiceDetailsList
                                          .where((element) =>
                                              element.quantity! > 0)
                                          .length,
                                      changeMoney: 0.0,
                                    );

                                    await ProductMakerDb
                                        .insertIntoSupplierInvoice(
                                            newSupplierInvoice);

                                    widget.listOfSupplierInvoice!
                                        .add(newSupplierInvoice);

                                    supplierInvoiceProvider
                                        .addToListOfSupplierInvoice(
                                            newSupplierInvoice);

                                    print("List length: ${list.length}");

                                    await ProductMakerDb
                                        .insertSupplierInvoiceDetails(
                                            supplierInvoiceDetailsList
                                                .where((element) =>
                                                    element.quantity! > 0)
                                                .toList(),
                                            newSupplierInvoice.id!);

                                    for (var all in supplierInvoiceDetailsList
                                        .where(
                                            (element) => element.quantity! > 0)
                                        .toList()) {
                                      supplierInvoiceDetailsProvider
                                          .addToListOfSupplierInvoiceDetails(
                                              all);
                                    }

                                    supplierInvoiceDetailsList
                                        .forEach((element) {
                                      element.id = null;
                                      element.supplierInvoiceId = null;
                                      element.quantity = 0;
                                      element.returned = 0;
                                    });
                                    totalForNovoePostuplenie = 0.0;
                                    Navigator.pop(context);
                                  },
                                  child: Text("Да")),
                            ],
                          );
                          showDialog(
                              context: context,
                              builder: (context) => newDialog);
                        },
                        child: Center(
                          child: Text(
                            "Добавить",
                            style: TextStyle(
                                color: supplierInvoiceDetailsList.every(
                                        (element) => element.quantity == 0)
                                    ? Colors.white70
                                    : Colors.white,
                                fontSize: 20),
                          ),
                        )),
                  )
                ],
              ),

              ///
              ///
              ///
              Column(
                children: [
                  if (listOfInvoice.isNotEmpty)
                    InkWell(
                      onTap: () {
                        showSearch(
                            context: context,
                            delegate: SupplierReturnedSearchWidget(
                                listOfSupplierInvoice: listOfInvoice));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: const [
                                Icon(Icons.search),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  "Поиск",
                                  style: TextStyle(fontSize: 20),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: listOfInvoice.length,
                        itemBuilder: (context, index) {
                          var eachSupplierInvoiceDetailes =
                              supplierInvoiceDetailsProvider
                                  .listSupplierInvoiceDetails
                                  .where((element) =>
                                      element.supplierInvoiceId ==
                                      listOfInvoice[index].id)
                                  .toList();
                          return InkWell(
                            onTap: () {
                              setState(() {});
                              var eachSupplierInvoiceDetailes =
                                  supplierInvoiceDetailsProvider
                                      .listSupplierInvoiceDetails
                                      .where((element) =>
                                          element.supplierInvoiceId ==
                                          listOfInvoice[index].id)
                                      .toList();

                              var total = 0.0;

                              eachSupplierInvoiceDetailes.forEach((element) {
                                var rounded = ((element.quantity));

                                var sumOfQuantityofShtuk =
                                    (((rounded! - rounded.toInt()) * 100)
                                            .round() /
                                        100);

                                sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                                        element.kolvoUpakovka!)
                                    .roundToDouble();

                                total += element.price! * rounded.toInt() +
                                    ((sumOfQuantityofShtuk * element.price!) /
                                        element.kolvoUpakovka!);
                              });

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SupplierEachDetailScrenn(
                                            total: total,
                                            listOfSupplierDetails:
                                                eachSupplierInvoiceDetailes,
                                            getDateTime:
                                                listOfInvoice[index].datatime,
                                            supplierInvoice:
                                                listOfInvoice[index],
                                            fromReturnedPage: true,
                                          )));
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${listOfInvoice[index].id}",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Дата: ${listOfInvoice[index].datatime.toString().substring(0, 11)}",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Время: ${listOfInvoice[index].datatime.toString().substring(11, 19)}",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  if (eachSupplierInvoiceDetailes.every(
                                      (element) => element.returned == 1))
                                    const Text(
                                      "Возвращено",
                                      style: TextStyle(color: Colors.redAccent),
                                    )
                                  else
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        const Text(
                                          "Общая сумма",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          listOfInvoice[index]
                                              .total!
                                              .toStringAsFixed(2),
                                          style: const TextStyle(
                                              fontStyle: FontStyle.normal),
                                        ),
                                      ],
                                    )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ])));
  }
}
