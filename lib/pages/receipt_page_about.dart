import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/receipt_product_model.dart';
import 'package:my_store/providers/receipt_and_tovar_provider.dart';
import 'package:my_store/screens/return_of_tovar_screen.dart';
import 'package:provider/provider.dart';
import '../models/receipt_model.dart';

class ReceiptPageAbout extends StatefulWidget {
  Receipt? receipt;
  List<ReceiptProduct>? productMaker;

  ReceiptPageAbout({Key? key, this.receipt, this.productMaker})
      : super(key: key);

  @override
  State<ReceiptPageAbout> createState() => _ReceiptPageAboutState();
}

class _ReceiptPageAboutState extends State<ReceiptPageAbout> {
  double totalForCahngeMoney = 0.0;
  double changeMoney = 0.0;

  @override
  void initState() {
    if (widget.productMaker != null) {
      widget.productMaker!
          .where((element) => element.returned == 0)
          .forEach((element) {
        var rounded = ((element.quantity!));

        var sumOfQuantityofShtuk =
            (((rounded - rounded.toInt()) * 100).round() / 100);

        sumOfQuantityofShtuk =
            (sumOfQuantityofShtuk * element.kolvoUpakovke!).roundToDouble();
        totalForCahngeMoney += (element.prodazhiPrice! * rounded.toInt()) +
            ((sumOfQuantityofShtuk * element.prodazhiPrice!) /
                element.kolvoUpakovke!);
      });

      changeMoney = double.parse(widget.receipt!.cash!) - totalForCahngeMoney;
      print("ChangeMoney : ${changeMoney}");
    }
  }

  void setSt(double setTotal, double setChangeMoney) {
    setState(() {
      totalForCahngeMoney = setTotal;
      changeMoney = setChangeMoney;
    });
  }

  void justRefresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var receiptProvider = Provider.of<ReceipAndTovarProvider>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("${widget.receipt!.id}"),
        backgroundColor: Colors.blueAccent,
        actions: [
          TextButton(
              onPressed: () {
                var notReturned = widget.productMaker!
                    .where((element) => element.returned == 0)
                    .toList();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ReturnOfTovar(
                              receipt: widget.receipt,
                              list: notReturned,
                              setSt: setSt,
                              refreshParent: justRefresh,
                            )));
              },
              child: const Text(
                "Возврат",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ))
        ],
      ),
      body: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    totalForCahngeMoney.toStringAsFixed(2),
                    style: const TextStyle(color: Colors.black, fontSize: 30),
                  ),
                  const Text("Итого")
                ],
              )
            ],
          ),
          const Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Divider(
              thickness: 1,
              color: Colors.grey,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Кассир: ${widget.receipt!.cashier}"),
                    const SizedBox(
                      height: 10,
                    ),
                    Text("Касса: ${widget.receipt!.kassa}"),
                    const SizedBox(
                      height: 10,
                    ),
                    if (widget.receipt!.client != null)
                      Row(
                        children: [
                          Text("Клиент: ${widget.receipt!.client!.name}")
                        ],
                      )
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: widget.productMaker!.length,
              itemBuilder: (context, index) {
                var rounded = ((widget.productMaker![index].quantity));

                var sumOfQuantityofShtuk =
                    (((rounded! - rounded.toInt()) * 100).round() / 100);

                print(
                    "sumOfQuantityofShtuk ______ : ${(sumOfQuantityofShtuk * widget.productMaker![index].kolvoUpakovke!).round()}");

                sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                        widget.productMaker![index].kolvoUpakovke!)
                    .roundToDouble();

                //
                var total = (widget.productMaker![index].prodazhiPrice! *
                        rounded.toInt()) +
                    ((sumOfQuantityofShtuk *
                            widget.productMaker![index].prodazhiPrice!) /
                        widget.productMaker![index].kolvoUpakovke!);

                return Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${widget.productMaker![index].naimenovanie}",
                            style: TextStyle(
                                fontWeight:
                                    widget.productMaker![index].returned == 0
                                        ? FontWeight.bold
                                        : FontWeight.normal,
                                color:
                                    widget.productMaker![index].returned! == 0
                                        ? Colors.black
                                        : Colors.grey),
                          ),
                          // if (int.parse(
                          //         widget.productMaker![index].kolvoUpakovka!) >
                          //     0)
                          //   Text(
                          //     "В упаковке: ${widget.productMaker![index].kolvoUpakovka}",
                          //     style: const TextStyle(color: Colors.grey),
                          //   ),
                          if (widget.productMaker![index].returned == 0)
                            Row(
                              children: [
                                Text(
                                  "(${widget.productMaker![index].quantity!.toInt().toString()}-уп",
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                                if (sumOfQuantityofShtuk != 0)
                                  Text(
                                    ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                    style: const TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                const Text(
                                  ")",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 17),
                                )
                              ],
                            ),
                          const SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (widget.productMaker![index].returned == 1)
                            const Text(
                              "Возвращено",
                              style: TextStyle(color: Colors.redAccent),
                            )
                          else
                            const Text("-")
                        ],
                      ),
                    ],
                  ),
                );
              }),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Divider(
              thickness: 1,
              color: Colors.grey,
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Итого"),
                      Text(totalForCahngeMoney.toStringAsFixed(2))
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Наличные"),
                      Text(double.parse(widget.receipt!.cash!)
                          .toStringAsFixed(2))
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  if (changeMoney != 0.0)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Сдачи"),
                        Text(changeMoney.toStringAsFixed(2))
                      ],
                    ),
                ],
              )),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Divider(
              thickness: 1,
              color: Colors.grey,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              children: [
                Text("Время: ${widget.receipt!.dateTime!.substring(0, 11)} в "
                    "${widget.receipt!.dateTime!.substring(11, 16)}")
              ],
            ),
          ),
          const SizedBox(
            height: 100,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: SizedBox(
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  AlertDialog newDialog = AlertDialog(
                    content: const Text("Вы точно хотите удалить?"),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text("Нет")),
                      TextButton(
                          onPressed: () async {
                            receiptProvider
                                .deleteReceiptProduct(widget.productMaker!);
                            receiptProvider.deleteReceipt(widget.receipt!.id!);

                            await ProductMakerDb.deleteReceiptProducts(
                                widget.productMaker!);
                            await ProductMakerDb.deleteReceipt(
                                widget.receipt!.id!);

                            receiptProvider.notify();

                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: const Text("Да"))
                    ],
                  );

                  showDialog(context: context, builder: (context) => newDialog);
                },
                child: const Text("Удалить"),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.green)),
              ),
            ),
          )
        ],
      ),
    );
  }
}
