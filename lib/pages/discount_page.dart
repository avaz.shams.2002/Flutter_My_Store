import 'package:flutter/material.dart';
import 'package:my_store/screens/discount_creator_screen.dart';
import 'package:my_store/screens/side_bar.dart';
import 'package:provider/provider.dart';

import '../providers/discount_provider.dart';
import '../widgets/discount_search_widget.dart';

class DiscountPage extends StatefulWidget {
  const DiscountPage({Key? key}) : super(key: key);

  @override
  State<DiscountPage> createState() => _DiscountPageState();
}

class _DiscountPageState extends State<DiscountPage> {
  int changeToDeleteButton = 0;

  @override
  Widget build(BuildContext context) {
    var discountProvider = Provider.of<DiscountProvier>(context);
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.pink,
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DiscountCreatorScreen()));
          },
          child: const Text(
            "+",
            style: TextStyle(fontSize: 20),
          ),
        ),
        drawer: const SideBar(),
        appBar: AppBar(
          iconTheme: IconThemeData(color: changeToDeleteButton == 0 ? Colors.white : Colors.black),
          backgroundColor:
              changeToDeleteButton == 0 ? Colors.blueAccent : Colors.white,
          title:  Text("Скидки", style: TextStyle(color: changeToDeleteButton == 0 ? Colors.white : Colors.black),),
          elevation: 0,
          actions: [
            if (changeToDeleteButton == 0)
              IconButton(
                  onPressed: () {
                    showSearch(context: context, delegate: DiscountSearch());
                  },
                  icon: const Icon(Icons.search)),
            if (changeToDeleteButton == 1)
              IconButton(
                  onPressed: () {
                    setState(() {
                      changeToDeleteButton = 0;
                    });
                  },
                  icon: Icon(Icons.close, color: Colors.black,))
          ],
        ),
        body: ListView.builder(
            itemCount: discountProvider.listDiscount.length,
            itemBuilder: (context, index) {
              return InkWell(
                  onLongPress: () {
                    setState(() {});
                    changeToDeleteButton = 1;
                  },
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DiscountCreatorScreen(
                                  discount:
                                      discountProvider.listDiscount[index],
                                )));
                  },
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 12),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Center(
                          child: Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.blue[100]),
                            child: const Center(
                                child: Icon(
                              Icons.discount,
                              color: Colors.grey,
                            )),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20, left: 20),
                            child: Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${discountProvider.listDiscount[index].name}",
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                    Row(
                                      children: [
                                        if (changeToDeleteButton == 0)
                                          Text(
                                              "${discountProvider.listDiscount[index].value}${discountProvider.listDiscount[index].percentOrSumma == 1 ? "%" : ''}"),
                                        if (changeToDeleteButton == 1)
                                          Container(
                                              width: 10,
                                              height: 10,
                                              child: IconButton(
                                                  onPressed: () {
                                                    print('object');
                                                  },
                                                  icon: const Icon(
                                                    Icons.delete,
                                                  )))
                                      ],
                                    )
                                  ],
                                ),
                                const Divider(
                                  color: Colors.grey,
                                  thickness: 1,
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ));
            }),
      ),
    );
  }
}
