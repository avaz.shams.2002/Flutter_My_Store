import 'package:flutter/material.dart';
import 'package:my_store/pages/supplier_page.dart';
import 'package:my_store/pages/supplier_creater_page.dart';
import 'package:my_store/providers/contact_person_provider.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:my_store/screens/contragent_archive.dart';
import 'package:my_store/widgets/contact_person_widget.dart';
import 'package:my_store/widgets/contragent_search_widget.dart';
import 'package:provider/provider.dart';

import '../models/contragent_maker_model.dart';
import '../providers/contragent_provider.dart';
import '../providers/supplier_invoice_provider.dart';
import '../screens/side_bar.dart';

class KontragentPage extends StatelessWidget {
  const KontragentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var contragentProvider = Provider.of<ContragentProvider>(context);
    var productPRovider = Provider.of<ProductMakerProvider>(context);
    var check = Provider.of<ContactPersonProvider>(context);
    var supplierInvoiceProvider = Provider.of<SupplierAllProvider>(context);
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
          floatingActionButton: FloatingActionButton(
              child: const Text("+",
                  style: TextStyle(color: Colors.white, fontSize: 20)),
              backgroundColor: Colors.pink,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SupplierCreatorPage(
                              contactWidget: [],
                            )));
              }),
          drawer: const SideBar(),
          appBar: AppBar(
            backgroundColor: Colors.blueAccent,
            title: const Text("Поставщики"),
            elevation: 0,
            actions: [
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ContragentArchiv()));
                  },
                  icon: const Icon(Icons.archive_outlined)),
              IconButton(
                  onPressed: () {
                    showSearch(
                        context: context,
                        delegate: ContragentSearchWidget(
                            listOfContragent: contragentProvider.list));
                  },
                  icon: const Icon(Icons.search))
            ],
          ),
          body: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    "Список поставщиков: ",
                    style: TextStyle(color: Colors.black, fontSize: 17),
                  ),
                ),
              ),
              Expanded(
                child: ListView.separated(
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(
                          color: Colors.grey,
                          thickness: 1,
                        ),
                    itemCount:
                        contragentProvider.listOfNotArchiveContragent().length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          var listOfSupplierInvoice = supplierInvoiceProvider
                              .listOfSupplierInvoice
                              .where((element) =>
                                  element.supplierId ==
                                  contragentProvider
                                      .listOfNotArchiveContragent()[index]
                                      .id)
                              .toList();

                          for (var all in supplierInvoiceProvider
                              .listOfSupplierInvoice) {
                            print(all.supplierId);
                          }
                          var listOfOwnSupplierProduct = productPRovider
                              .getNotArchiveTovar()
                              .where((element) =>
                                  element.postavshikId ==
                                  contragentProvider
                                      .listOfNotArchiveContragent()[index]
                                      .id)
                              .toList();

                          var listminus = productPRovider
                              .getNotArchiveTovar()
                              .where((element) => element.postavshikId == -1)
                              .toList();

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SupplierPage(
                                        //
                                        contragent: contragentProvider
                                                .listOfNotArchiveContragent()[
                                            index],
                                        //
                                        listOfProduct: listminus,
                                        //
                                        listOfOwnSupplierProduct:
                                            listOfOwnSupplierProduct,
                                        //
                                        listOfSupplierInvoice:
                                            listOfSupplierInvoice.isEmpty
                                                ? []
                                                : listOfSupplierInvoice,
                                        //
                                      )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Text(
                                    contragentProvider
                                        .listOfNotArchiveContragent()[index]
                                        .naimenovanie!,
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: IconButton(
                                      onPressed: () {
                                        var get = check.getContactFace(
                                            contragentProvider
                                                .listOfNotArchiveContragent()[
                                                    index]
                                                .id!);

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SupplierCreatorPage(
                                                      contragent: contragentProvider
                                                              .listOfNotArchiveContragent()[
                                                          index],
                                                      contactWidget: get,
                                                    )));
                                      },
                                      icon: const Icon(Icons.info_outline)))
                            ],
                          ),
                        ),
                      );
                    }),
              ),
            ],
          )),
    );
  }
}
