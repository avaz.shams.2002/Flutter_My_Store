import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/pages/create_product_page.dart';
import 'package:my_store/pages/oplata_page.dart';
import 'package:my_store/providers/category_provider.dart';
import 'package:my_store/providers/client_provider.dart';
import 'package:my_store/screens/check_screen.dart';
import 'package:my_store/pages/client_page.dart';
import 'package:my_store/screens/client_profile_screen.dart';
import 'package:my_store/screens/side_bar.dart';
import 'package:my_store/widgets/all_after_category_product_widget.dart';
import 'package:my_store/widgets/all_bottom_category_products_widget.dart';
import 'package:my_store/widgets/all_categories_widget.dart';
import 'package:my_store/widgets/all_products_widget.dart';
import 'package:my_store/widgets/custom_show_delegate_widget.dart';
import 'package:provider/provider.dart';
import '../providers/product_maker_provider.dart';
import '../widgets/category_search_widget.dart';
import '../widgets/each_products_search_widget.dart';

class SalesPage extends StatefulWidget {
  SalesPage({Key? key}) : super(key: key);

  @override
  State<SalesPage> createState() => _SalesPageState();
}

class _SalesPageState extends State<SalesPage> {
  Map<String, int?> map = {'Все товары': -2, 'Категории ↓': -1};

  late String typeTovar = 'Все товары';
  int changeSearch = 0;
  late dynamic productsSync = -2;

  List<ProductMaker> listPro = [];

  int? checkForCategory;
  late final TextEditingController _searchController =
      TextEditingController(text: '');
  final focus = FocusNode();

  void childFuncRefresh(List<ProductMaker> list, int category) {
    setState(() {
      listPro = list;
      checkForCategory = category;
    });
  }

  List<Widget> buttonValues = [
    Row(
      children: const [
        Icon(Icons.delete, color: Colors.grey),
        SizedBox(
          width: 10,
        ),
        Text("Очистить чек"),
      ],
    ),
    Row(
      children: const [
        Icon(Icons.sync, color: Colors.grey),
        SizedBox(
          width: 10,
        ),
        Text("Синхронизировать"),
      ],
    ),
  ];

  @override
  void initState() {
    Future.delayed(const Duration(), () {
      FocusScope.of(context).requestFocus(focus);
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    });
  }

  @override
  Widget build(BuildContext context) {
    var productMaker = Provider.of<ProductMakerProvider>(context);
    var categoryProvider = Provider.of<CategoryProvider>(context);
    var clientProvider = Provider.of<ClientProvider>(context);
    if (categoryProvider.getCategoryNotArchive().isNotEmpty) {
      for (int i = 0;
          i < categoryProvider.getCategoryNotArchive().length;
          i++) {
        map[categoryProvider.getCategoryNotArchive()[i].name!] =
            categoryProvider.getCategoryNotArchive()[i].id!;
      }
    }

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: GestureDetector(
        onTap: () async {
          focusButNotKeyboard();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          drawer: const SideBar(),
          appBar: AppBar(
            backgroundColor: Colors.blueAccent,
            elevation: 0,
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(context, PageRouteBuilder(
                        pageBuilder: (BuildContext context, _, __) {
                          return const CheckScreen();
                        },
                      ));
                    },
                    child: Badge(
                      animationType: BadgeAnimationType.scale,
                      showBadge:
                          productMaker.getWhichisAdded().isEmpty ? false : true,
                      badgeContent: Row(
                        children: [
                          if (productMaker.getWhichisAdded().isNotEmpty)
                            Text("${productMaker.getWhichisAdded().length}"),
                        ],
                      ),
                      child: const Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Text(
                          "Чек",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                      badgeColor: Colors.white,
                      toAnimate: true,
                      shape: BadgeShape.circle,
                    ),
                  ),
                  Row(
                    children: [
                      DropdownButtonHideUnderline(
                        child: DropdownButton(
                            icon: const Icon(
                              Icons.more_vert,
                              color: Colors.white,
                            ),
                            items: buttonValues
                                .map((e) => DropdownMenuItem(
                                      child: e,
                                      value: e,
                                    ))
                                .toList(),
                            onChanged: (val) async {
                              if (val == buttonValues[0]) {
                                productsSync = -2;
                                await ProductMakerDb.setZeroQuantity(0, 0.0);
                                productMaker.setZeroCart();
                                focusButNotKeyboard();
                              }
                              if (val == buttonValues[1]) {
                                ///
                                ///
                                /// Here will be sync with server
                                ///
                                ///
                              }
                            }),
                      ),
                      if (clientProvider.addedClient.isEmpty)
                        IconButton(
                            onPressed: () {
                              Navigator.push(context, PageRouteBuilder(
                                  pageBuilder: (BuildContext context, _, __) {
                                return const ClientPage();
                              }));
                            },
                            icon: const Icon(Icons.person_add)),
                      if (clientProvider.addedClient.isNotEmpty)
                        Stack(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ClientProfileScreen(
                                                client: clientProvider
                                                    .addedClient.first,
                                              )));
                                },
                                icon: const Icon(Icons.person_rounded)),
                            const Positioned(
                                bottom: 15,
                                left: 5,
                                child: Icon(
                                  Icons.check,
                                  size: 15,
                                ))
                          ],
                        ),
                      IconButton(
                          onPressed: () {
                            addByCamera(productMaker, categoryProvider);
                          },
                          icon: const Icon(Icons.camera_alt)),
                    ],
                  )
                ],
              )
            ],
          ),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: InkWell(
                  onTap: () {
                    if (productMaker.allPrice() == 0.0) {
                      return;
                    }
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OplataPage(
                                  allPrice: productMaker
                                      .allPrice()
                                      .toStringAsFixed(2),
                                  listProductMaker:
                                      productMaker.getWhichisAdded(),
                                  client: clientProvider.addedClient.isEmpty? null : clientProvider.addedClient.first,
                                )));
                    focusButNotKeyboard();
                  },
                  child: Container(
                    color: Colors.green,
                    height: 70,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "ОПЛАТИТЬ",
                            style: TextStyle(
                                color: productMaker.allPrice() == 0
                                    ? Colors.white70
                                    : Colors.white,
                                fontSize: 16),
                          ),
                          Text(
                            productMaker.allPrice().toStringAsFixed(2),
                            style: TextStyle(
                                color: productMaker.allPrice() == 0
                                    ? Colors.white70
                                    : Colors.white,
                                fontSize: 16),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      typeTovar,
                      style: const TextStyle(fontSize: 16),
                    ),
                    DropdownButtonHideUnderline(
                        child: DropdownButton(
                            items: map
                                .map((key, value) {
                                  return MapEntry(
                                      key,
                                      DropdownMenuItem(
                                          value: key,
                                          onTap: () {
                                            setState(() {
                                              productsSync = value;
                                              checkForCategory = null;
                                              if (productsSync != null) {
                                                listPro = [];
                                                listPro = productMaker
                                                    .getNotArchiveTovar()
                                                    .where((element) =>
                                                        element.categoryId ==
                                                        productsSync.toString())
                                                    .toList();
                                              }
                                              focusButNotKeyboard();
                                            });
                                          },
                                          child: Text(key)));
                                })
                                .values
                                .toList(),
                            onChanged: (val) {
                              setState(() {
                                typeTovar = val.toString();
                              });
                            })),
                    if (productsSync == -2)
                      IconButton(
                          onPressed: () {
                            showSearch(
                                context: context,
                                delegate: CustomShowDelegate(
                                    productmaker: productMaker.productsmaker));
                          },
                          icon: const Icon(Icons.search)),
                    if (productsSync == -1)
                      IconButton(
                          onPressed: () {
                            showSearch(
                                context: context,
                                delegate: CategorySearch(
                                    refresh: childFuncRefresh,
                                    checkCategory: checkForCategory,
                                    list: listPro,
                                    productMaker:
                                        productMaker.getNotArchiveTovar(),
                                    listCategory: categoryProvider
                                        .getCategoryNotArchive()));
                          },
                          icon: const Icon(Icons.search)),
                    if (productsSync != -2 && productsSync != -1)
                      IconButton(
                          onPressed: () {
                            showSearch(
                                context: context,
                                delegate: EachProductSearch(list: listPro));
                          },
                          icon: const Icon(Icons.search))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Stack(
                  children: [
                    TextField(
                      decoration: const InputDecoration(
                          hintText: "Введите штрихкод",
                          hintStyle: TextStyle(fontSize: 14)),
                      focusNode: focus,
                      keyboardType: TextInputType.number,
                      autofocus: true,
                      controller: _searchController,
                      onSubmitted: (sub) async {
                        setState(() {});
                        addByBarcode(productMaker, categoryProvider);
                      },
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: IconButton(
                          onPressed: () {
                            setState(() {});
                            _searchController.text = '';
                            focusButNotKeyboard();
                          },
                          icon: const Icon(Icons.close)),
                    )
                  ],
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: Card(
                    color: Colors.white,
                    elevation: 0,
                    shadowColor: Colors.black,
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        const SizedBox(
                          height: 15,
                        ),
                        if (productsSync == -2)
                          productMaker.productsmaker.isEmpty
                              ? const Text("")
                              : const AllProductsWidget(),
                        if (productsSync == -1)
                          (checkForCategory == null)
                              ? AllCategoriesWidget(
                                  checkForCategory: checkForCategory,
                                  listPro: listPro,
                                  notify: childFuncRefresh,
                                )
                              : AllAfterCategoryProductWidget(
                                  listPro: listPro,
                                ),
                        if (productsSync != null)
                          if (checkForCategory == null)
                            AllBottomCategoryProductsWidget(
                              listPro: listPro,
                            )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void addByBarcode(productMaker, categoryProvider) async {
    if (_searchController.text.isEmpty) {
      FocusScope.of(context).requestFocus(focus);
      Future.delayed(const Duration(milliseconds: 50), () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      });
      return;
    }
    var listProduct = productMaker
        .getNotArchiveTovar()
        .where((element) => element.shtrihkod == _searchController.text.trim());

    if (listProduct.isEmpty) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CreateProductPage(
                    image_: null,
                    category: categoryProvider.categoryList.isEmpty
                        ? []
                        : categoryProvider.getCategoryNotArchive(),
                    newCategory: false,
                    shtrihcode: _searchController.text.trim(),
                  )));

      FocusScope.of(context).requestFocus(focus);
      SystemChannels.textInput.invokeListMethod('TextInput.hide');
      Future.delayed(const Duration(milliseconds: 100), () {
        _searchController.text = '';
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      });
      return;
    }
    await ProductMakerDb.addItemToDbProduct(listProduct.first);
    productMaker.notify();
    _searchController.text = '';
    FocusScope.of(context).requestFocus(focus);
    SystemChannels.textInput.invokeListMethod('TextInput.hide');
  }

  void addByCamera(productMaker, categoryProvider) {
    FlutterBarcodeScanner.scanBarcode(
            "#000000", "Cancel", true, ScanMode.BARCODE)
        .then((value) async {
      if (value == '-1') {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => SalesPage()));
        return;
      }
      var listProduct = productMaker
          .getNotArchiveTovar()
          .where((element) => element.shtrihkod == value);

      if (listProduct.isEmpty) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CreateProductPage(
                      image_: null,
                      category: categoryProvider.categoryList.isEmpty
                          ? []
                          : categoryProvider.getCategoryNotArchive(),
                      newCategory: false,
                      shtrihcode: value,
                    )));
        return;
      }
      await ProductMakerDb.addItemToDbProduct(listProduct.first);
      productMaker.notify();
    });
  }

  void focusButNotKeyboard() {
    FocusScope.of(context).requestFocus(focus);
    Future.delayed(const Duration(milliseconds: 5), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    });
  }
}
