import 'package:flutter/material.dart';
import 'package:my_store/database/db/product_maker_db.dart';
import 'package:my_store/models/receipt_model.dart';
import 'package:my_store/models/receipt_product_model.dart';
import 'package:my_store/providers/receipt_and_tovar_provider.dart';
import 'package:my_store/screens/oplacheno_screen.dart';
import 'package:my_store/screens/oplata_razdelno_screen.dart';
import 'package:provider/provider.dart';

import '../models/client_model.dart';
import '../models/product_make_model.dart';

class OplataPage extends StatefulWidget {
  String? allPrice;
  List<ProductMaker>? listProductMaker;
  Client? client;

  OplataPage({Key? key, this.allPrice, this.listProductMaker, this.client})
      : super(key: key);

  @override
  State<OplataPage> createState() => _OplataPageState();
}

class _OplataPageState extends State<OplataPage> {
  late TextEditingController priceController =
      TextEditingController(text: '0.0');

  late double price = 0;

  List<ReceiptProduct> list = [];

  @override
  void initState() {
    priceController = TextEditingController(text: widget.allPrice.toString());
    price = double.parse(widget.allPrice!);
    if (widget.listProductMaker != null) {
      for (int i = 0; i < widget.listProductMaker!.length; i++) {
        list.add(ReceiptProduct(
          receiptId: widget.listProductMaker![i].receiptId,
          naimenovanie: widget.listProductMaker![i].naimenovanie,
          quantity: widget.listProductMaker![i].quantity,
          kolvoUpakovke: int.parse(widget.listProductMaker![i].kolvoUpakovka!),
          prodazhiPrice: widget.listProductMaker![i].prodazhiPrice,
          returned: 0,
        ));
        print(widget.listProductMaker![i].quantity);
      }
    }
    print("List mi ; ${list.length}");
  }

  @override
  Widget build(BuildContext context) {
    var receiptProvider = Provider.of<ReceipAndTovarProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blueAccent,
        actions: [
          TextButton(
              onPressed: () {
                print(widget.client!.name);
              },
              child: const Text(
                "Текст",
                style: TextStyle(color: Colors.white),
              )),
          TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => OplataRazdelnoScreen(
                              allPrice: double.parse(widget.allPrice!),
                            )));
              },
              child: const Text(
                "Разделить",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        "${widget.allPrice}",
                        style: const TextStyle(fontSize: 30),
                      ),
                      const Text(
                        "К оплате",
                        style: const TextStyle(color: Colors.grey),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                Text(
                  "Полученная сумма наличных",
                  style: TextStyle(color: Colors.blueAccent),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            TextField(
              onChanged: (val) {
                if (val == '') {
                  return;
                }
                setState(() {});
              },
              onTap: () {
                setState(() {
                  priceController.text = '';
                });
              },
              controller: priceController,
              keyboardType: TextInputType.number,
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.white70)),
                onPressed: () async {
                  FocusManager.instance.primaryFocus!.unfocus();
                  if (priceController.text.isEmpty) {
                    priceController.text = '0.0';
                    return;
                  }
                  if (double.parse(priceController.text) < price) {
                    print("smqZaldlclcdl");
                    return;
                  }



                  DateTime today = DateTime.now();
                  if (widget.client != null) {
                    widget.client!.numberOfVisits =
                        widget.client!.numberOfVisits! + 1;
                    widget.client!.lastVisit = today.toString();
                    await ProductMakerDb.updateClient(widget.client!);
                  }
                  Receipt newReceipt = Receipt(
                      total: widget.allPrice.toString(),
                      cashier: '',
                      kassa: '',
                      cash: priceController.text,
                      dateTime: today.toString().substring(0, 19),
                      changeMoney: (double.parse(priceController.text) - price)
                          .toStringAsFixed(1),
                      client: widget.client);


                  await ProductMakerDb.insertReceipt(newReceipt);
                  print("Receipt Id : ${newReceipt.id}");
                  receiptProvider.addReceipt(newReceipt);
                  await ProductMakerDb.insertProductReceipt(
                      newReceipt.id!, list);

                  receiptProvider.addReceiptProduct(list);
                  widget.listProductMaker = [];
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => OplachenoScrenn(
                              allPrice: double.parse(widget.allPrice!),
                              sdachi:
                                  double.parse(priceController.text) - price)));
                  print("yes");
                },
                child: priceController.text.isNotEmpty
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.money,
                            color: double.parse(priceController.text) < price
                                ? Colors.grey
                                : Colors.black,
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          Text(
                            "НАЛИЧНЫЕ",
                            style: TextStyle(
                                color:
                                    double.parse(priceController.text) < price
                                        ? Colors.grey
                                        : Colors.black),
                          )
                        ],
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(
                            Icons.money,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Text(
                            "НАЛИЧНЫЕ",
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      )),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.white70)),
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(Icons.account_balance_wallet_outlined,
                          color: Colors.black),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        "КАРТА",
                        style: TextStyle(color: Colors.black),
                      )
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
