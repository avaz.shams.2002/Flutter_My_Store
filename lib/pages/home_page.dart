import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_store/screens/side_bar.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // Provider.of<CheckUser>(context, listen: false)
    //     .setUser(user: FirebaseAuth.instance.currentUser, context: context);
    return WillPopScope(
      onWillPop: () async {
        AlertDialog alertDialog = AlertDialog(
          content: Text("Выйти?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Нет")),
            TextButton(
                onPressed: () {
                  exit(0);
                },
                child: Text("Да")),
          ],
        );
        showDialog(context: context, builder: (context) => alertDialog);
        return Future.value(false).then((value) => false);
        //return false; -- нависи мешад
      },
      child: DefaultTabController(
        length: 4,
        child: Scaffold(
            drawer: const SideBar(),
            appBar: AppBar(
              backgroundColor: Colors.blueAccent,
              elevation: 0,
              bottom: const TabBar(
                  isScrollable: true,
                  labelColor: Colors.white,
                  tabs: [
                    Tab(
                      text: "Продажи",
                    ),
                    Tab(
                      text: "Деньги",
                    ),
                    Tab(
                      text: "Заказы",
                    ),
                    Tab(
                      text: "Точки продаж",
                    ),
                  ]),
              title: const Text("Показатели"),
              systemOverlayStyle: SystemUiOverlayStyle.dark,
            ),
            body: const TabBarView(children: [
              Center(
                child: Text("Продажи"),
              ),
              Center(
                child: Text("Деньги"),
              ),
              Center(
                child: Text("Заказы"),
              ),
              Center(
                child: Text("точки продаж"),
              ),
            ])),
      ),
    );
  }
}
