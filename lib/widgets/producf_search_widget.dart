import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:my_store/models/product_make_model.dart';
import 'package:my_store/screens/tovar_about.dart';

class ProductSearchWidget extends SearchDelegate {
  List<ProductMaker>? listProduct;

  ProductSearchWidget({this.listProduct});

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            FlutterBarcodeScanner.scanBarcode(
                    "#000000", "Cancel", true, ScanMode.BARCODE)
                .then((value) => query = value);
          },
          icon: const Icon(Icons.camera_alt)),
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.close))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<ProductMaker> _list = [];
    for (var all in listProduct!) {
      if (all.naimenovanie
              .toString()
              .toUpperCase()
              .contains(query.toUpperCase()) ||
          all.shtrihkod!.toUpperCase().contains(query.toUpperCase())) {
        _list.add(all);
      }
    }

    return ListView.builder(
        itemCount: _list.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TovarAbout(
                            productMaker: _list[index],
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _list[index].naimenovanie!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<ProductMaker> _list = [];
    for (var all in listProduct!) {
      if (all.naimenovanie
              .toString()
              .toUpperCase()
              .contains(query.toUpperCase()) ||
          all.shtrihkod!.toUpperCase().contains(query.toUpperCase())) {
        _list.add(all);
      }
    }

    return ListView.builder(
        itemCount: _list.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        TovarAbout(productMaker: _list[index]),
                  ));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _list[index].naimenovanie!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
