import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';
import '../models/product_make_model.dart';
import '../providers/discount_provider.dart';
import '../providers/product_maker_provider.dart';
import '../screens/change_price_screen.dart';

class AllBottomCategoryProductsWidget extends StatelessWidget {
  List<ProductMaker>? listPro;

  AllBottomCategoryProductsWidget({Key? key, this.listPro}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var productMaker = Provider.of<ProductMakerProvider>(context);
    var discountProvider = Provider.of<DiscountProvier>(context);
    return ListView.separated(
      physics: NeverScrollableScrollPhysics(),
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: Colors.black,
        thickness: 1,
      ),
      shrinkWrap: true,
      itemCount: listPro!.length,
      itemBuilder: (context, index) {
        var rounded = ((listPro![index].quantity));

        var sumOfQuantityofShtuk =
            (((rounded - rounded.toInt()) * 100).round() / 100);
        print("shit rounded : $sumOfQuantityofShtuk");
        print(
            "sumOfQuantityofShtuk ______ : ${(sumOfQuantityofShtuk * int.parse(listPro![index].kolvoUpakovka!)).round()}");

        sumOfQuantityofShtuk =
            (sumOfQuantityofShtuk * int.parse(listPro![index].kolvoUpakovka!))
                .roundToDouble();

        return InkWell(
          onTap: () async {
            await ProductMakerDb.addItemToDbProduct(listPro![index]);
            productMaker.notify();
          },
          child: Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    if (listPro![index].image.toString() == "File: 'null'" ||
                        listPro![index].image == null)
                      InkWell(
                        onTap: () {


                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChangePriceScreen(
                                        productMaker: listPro![index],
                                        discount: discountProvider.listDiscount,
                                      )));
                        },
                        child: Container(
                          width: 100,
                          height: 50,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage("assets/images/1.jpg"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.bottomLeft,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "${listPro![index].naimenovanie![0]}"
                              "${listPro![index].naimenovanie![listPro![index].naimenovanie!.length - 1]}",
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      )
                    else
                      InkWell(
                        onTap: () {


                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChangePriceScreen(
                                      discount: discountProvider.listDiscount,
                                      productMaker: listPro![index])));
                        },
                        child: Container(
                            width: 100,
                            height: 50,
                            child: Image.file(listPro![index].image!)),
                      ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (listPro![index].naimenovanie!.isNotEmpty)
                              Text(
                                  "${listPro![index].code} • ${listPro![index].naimenovanie}"),
                            const SizedBox(
                              height: 10,
                            ),
                            listPro![index].quantity == 0
                                ? const Text(
                                    "Нажмите чтобы добавить",
                                    style: TextStyle(color: Colors.pink),
                                  )
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      const Icon(
                                        Icons.shopping_cart_checkout_outlined,
                                        color: Colors.pink,
                                      ),
                                      Text(
                                        " (${listPro![index].quantity.toInt().toString()}-уп",
                                        style: const TextStyle(
                                            color: Colors.black, fontSize: 14),
                                      ),
                                      if (sumOfQuantityofShtuk != 0)
                                        Text(
                                          ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 14),
                                        ),
                                      const Text(
                                        ")",
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 17),
                                      )
                                    ],
                                  )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangePriceScreen(
                                      discount: discountProvider.listDiscount,
                                      productMaker: listPro![index],
                                    )));
                      },
                      child: Text(
                        listPro![index].getTotalOfProduct().toStringAsFixed(2),
                        style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
