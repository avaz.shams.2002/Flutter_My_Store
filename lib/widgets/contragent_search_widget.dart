import 'package:flutter/material.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:my_store/providers/supplier_invoice_provider.dart';
import 'package:provider/provider.dart';

import '../models/contragent_maker_model.dart';
import '../pages/supplier_page.dart';
import '../pages/supplier_creater_page.dart';
import '../providers/contact_person_provider.dart';

class ContragentSearchWidget extends SearchDelegate {
  List<Contragent>? listOfContragent;

  ContragentSearchWidget({this.listOfContragent});

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.close))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Contragent> resultList = [];
    var productPRovider = Provider.of<ProductMakerProvider>(context);
    for (var all in listOfContragent!) {
      if (all.naimenovanie!.toUpperCase().contains(query.toUpperCase())) {
        resultList.add(all);
      }
    }
    var check = Provider.of<ContactPersonProvider>(context);

    var supplierInvoiceProvider = Provider.of<SupplierAllProvider>(context);
    return ListView.separated(
        separatorBuilder: (BuildContext context, int index) => const Divider(
              color: Colors.grey,
              thickness: 1,
            ),
        itemCount: resultList.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.pop(context);
              var listOfSupplierInvoice = supplierInvoiceProvider
                  .listOfSupplierInvoice
                  .where((element) =>
                      element.supplierId ==
                          resultList[index].id)
                  .toList();

              for (var all in supplierInvoiceProvider.listOfSupplierInvoice) {
                print(all.supplierId);
              }
              var listOfOwnSupplierProduct = productPRovider
                  .getNotArchiveTovar()
                  .where((element) =>
                      element.postavshikId ==
                          resultList[index].id)
                  .toList();

              var listminus = productPRovider
                  .getNotArchiveTovar()
                  .where((element) => element.postavshikId == -1)
                  .toList();

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SupplierPage(
                            //
                            contragent: resultList[index],
                            //
                            listOfProduct: listminus,
                            //
                            listOfOwnSupplierProduct: listOfOwnSupplierProduct,
                            //
                            listOfSupplierInvoice: listOfSupplierInvoice.isEmpty
                                ? []
                                : listOfSupplierInvoice,
                            //
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(
                        resultList[index].naimenovanie!,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: IconButton(
                          onPressed: () {
                            var get =
                                check.getContactFace(resultList[index].id!);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SupplierCreatorPage(
                                          contragent: resultList[index],
                                          contactWidget: get,
                                        )));
                          },
                          icon: const Icon(Icons.info_outline)))
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Contragent> resultList = [];

    for (var all in listOfContragent!) {
      if (all.naimenovanie!.toUpperCase().contains(query.toUpperCase())) {
        resultList.add(all);
      }
    }
    var productPRovider = Provider.of<ProductMakerProvider>(context);
    var check = Provider.of<ContactPersonProvider>(context);

    var supplierInvoiceProvider = Provider.of<SupplierAllProvider>(context);
    return ListView.separated(
        separatorBuilder: (BuildContext context, int index) => const Divider(
              color: Colors.grey,
              thickness: 1,
            ),
        itemCount: resultList.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.pop(context);
              var listOfSupplierInvoice = supplierInvoiceProvider
                  .listOfSupplierInvoice
                  .where((element) =>
              element.supplierId ==
                  resultList[index].id)
                  .toList();

              for (var all in supplierInvoiceProvider.listOfSupplierInvoice) {
                print(all.supplierId);
              }
              var listOfOwnSupplierProduct = productPRovider
                  .getNotArchiveTovar()
                  .where((element) =>
              element.postavshikId ==
                  resultList[index].id)
                  .toList();

              var listminus = productPRovider
                  .getNotArchiveTovar()
                  .where((element) => element.postavshikId == -1)
                  .toList();

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SupplierPage(
                        //
                        contragent: resultList[index],
                        //
                        listOfProduct: listminus,
                        //
                        listOfOwnSupplierProduct: listOfOwnSupplierProduct,
                        //
                        listOfSupplierInvoice: listOfSupplierInvoice.isEmpty
                            ? []
                            : listOfSupplierInvoice,
                        //
                      )));
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(
                        resultList[index].naimenovanie!,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: IconButton(
                          onPressed: () {
                            var get =
                                check.getContactFace(resultList[index].id!);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SupplierCreatorPage(
                                          contragent: resultList[index],
                                          contactWidget: get,
                                        )));
                          },
                          icon: const Icon(Icons.info_outline)))
                ],
              ),
            ),
          );
        });
  }
}
