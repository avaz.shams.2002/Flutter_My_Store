import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../models/supplier_invoice_model.dart';
import '../providers/supplier_invoice_details_provider.dart';
import '../screens/supplier_each_detail_screen.dart';

class SupplierReturnedSearchWidget extends SearchDelegate {
  List<SupplierInvoice> listOfSupplierInvoice;

  SupplierReturnedSearchWidget({required this.listOfSupplierInvoice});

  var dateTimeStart = DateTime.now();
  var dateTimeEnd = DateTime.now();
  var dateTime = DateTimeRange(start: DateTime.now(), end: DateTime.now());
  DateTimeRange? newDateTime;
  bool empty = false;

  String getDateTime = '';

  @override
  void initState() {
    dateTime = DateTimeRange(start: dateTimeStart, end: dateTimeEnd);
    getDateTime =
        "Период с ${dateTime.start.toString().substring(0, 11)} по ${dateTime.end.toString().substring(0, 11)}: ";
  }

  List<SupplierInvoice> listByDay = [];

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () async {
            listByDay = [];
            newDateTime = await showDateRangePicker(
                context: context,
                initialDateRange: dateTime,
                locale: const Locale("ru", "RU"),
                firstDate: DateTime(2010),
                lastDate: DateTime(2100));

            if (newDateTime == null) return;
            FocusManager.instance.primaryFocus!.unfocus();
            DateTime startDate = newDateTime!.start;
            DateTime endDate = newDateTime!.end;

            dateTime = DateTimeRange(start: startDate, end: endDate);
            getDateTime =
                "Период с ${dateTime.start.toString().substring(0, 11)} по ${dateTime.end.toString().substring(0, 11)}: ";
            int difference = endDate.difference(startDate).inDays;

            var items = List<DateTime>.generate(difference + 1, (index) {
              DateTime date = startDate;

              return date.add(Duration(days: index));
            });

            if (items.isEmpty) {
              items.add(startDate);
            }

            for (var allList in listOfSupplierInvoice) {
              for (var all in items) {
                String day =
                    "${all.year}-${all.month < 10 ? "0${all.month}" : all.month}-${all.day < 10 ? "0${all.day}" : all.day}"
                        .trim();

                if (allList.datatime!.substring(0, 11).trim() == day) {
                  listByDay.add(allList);
                }
              }
            }
            empty = false;
            query = '';
            if (listByDay.isEmpty) {
              FocusManager.instance.primaryFocus?.unfocus();
              // EasyLoading.showError("Поиск не дал результатов")
              //     .timeout(const Duration(microseconds: 500));
              empty = true;
            }
          },
          icon: const Icon(Icons.date_range)),
      IconButton(
          onPressed: () {
            query = '';
            listByDay = [];
            getDateTime = '';
            dateTime =
                DateTimeRange(start: DateTime.now(), end: DateTime.now());
            empty = false;
          },
          icon: const Icon(Icons.close))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<SupplierInvoice> emptyListOfSupplierInvoice = [];

    if (query.isNotEmpty) {
      listByDay = [];
    }

    for (var all in listOfSupplierInvoice) {
      if (all.id.toString().toUpperCase().contains(query.toUpperCase()) ||
          all.datatime
              .toString()
              .toUpperCase()
              .trim()
              .contains(query.toUpperCase().trim())) {
        emptyListOfSupplierInvoice.add(all);
      }
    }
    var supplierInvoiceProvider =
        Provider.of<SupplierInvoiceDetailsProvider>(context);

    if (listByDay.isNotEmpty) {
      if (empty == false) {
        emptyListOfSupplierInvoice = listByDay;
      }
    }
    if (empty == true) {
      return Column(
        children: [
          Padding(
            padding:
                EdgeInsets.only(left: 12, top: getDateTime.isEmpty ? 0 : 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  getDateTime,
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
          ),
          const Expanded(child: Text("")),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text("Поиск не дал результатов"),
            ],
          ),
          const Expanded(child: Text("")),
        ],
      );
    }

    return ListView.builder(
        itemCount: emptyListOfSupplierInvoice.length,
        itemBuilder: (context, index) {
          var geList = supplierInvoiceProvider.listSupplierInvoiceDetails
              .where((element) =>
                  element.supplierInvoiceId ==
                  emptyListOfSupplierInvoice[index].id)
              .toList();
          return InkWell(
            onTap: () {
              var geList = supplierInvoiceProvider.listSupplierInvoiceDetails
                  .where((element) =>
                      element.supplierInvoiceId ==
                      emptyListOfSupplierInvoice[index].id)
                  .toList();

              var total = 0.0;

              geList.forEach((element) {
                var rounded = ((element.quantity));

                var sumOfQuantityofShtuk =
                    (((rounded! - rounded.toInt()) * 100).round() / 100);

                sumOfQuantityofShtuk =
                    (sumOfQuantityofShtuk * element.kolvoUpakovka!)
                        .roundToDouble();

                total += element.price! * rounded.toInt() +
                    ((sumOfQuantityofShtuk * element.price!) /
                        element.kolvoUpakovka!);
              });

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SupplierEachDetailScrenn(
                            total: total,
                            listOfSupplierDetails: geList,
                            getDateTime:
                                emptyListOfSupplierInvoice[index].datatime,
                            supplierInvoice: emptyListOfSupplierInvoice[index],
                            fromReturnedPage: false,
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${emptyListOfSupplierInvoice[index].id}",
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "Дата: ${emptyListOfSupplierInvoice[index].datatime.toString().substring(0, 11)}",
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "Время: ${emptyListOfSupplierInvoice[index].datatime.toString().substring(11, 19)}",
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                  if (geList.every((element) => element.returned == 1))
                    const Text(
                      "Возвращено",
                      style: TextStyle(color: Colors.redAccent),
                    )
                  else
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "Общая сумма",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          emptyListOfSupplierInvoice[index]
                              .total!
                              .toStringAsFixed(2),
                          style: const TextStyle(fontStyle: FontStyle.normal),
                        ),
                      ],
                    )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<SupplierInvoice> emptyListOfSupplierInvoice = [];
    if (query.isNotEmpty) {
      listByDay = [];
    }
    for (var all in listOfSupplierInvoice) {
      if (all.id.toString().toUpperCase().contains(query.toUpperCase()) ||
          all.datatime
              .toString()
              .toUpperCase()
              .trim()
              .contains(query.toUpperCase().trim())) {
        emptyListOfSupplierInvoice.add(all);
      }
    }
    var supplierInvoiceProvider =
        Provider.of<SupplierInvoiceDetailsProvider>(context);
    if (listByDay.isNotEmpty) {
      if (empty == false) {
        emptyListOfSupplierInvoice = listByDay;
      }
    }
    if (empty == true) {
      return Column(
        children: [
          Padding(
            padding:
                EdgeInsets.only(left: 12, top: getDateTime.isEmpty ? 0 : 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  getDateTime,
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
          ),
          const Expanded(child: Text("")),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text("Поиск не дал результатов"),
            ],
          ),
          const Expanded(child: Text("")),
        ],
      );
    }
    return ListView.builder(
        itemCount: emptyListOfSupplierInvoice.length,
        itemBuilder: (context, index) {
          var geList = supplierInvoiceProvider.listSupplierInvoiceDetails
              .where((element) =>
                  element.supplierInvoiceId ==
                  emptyListOfSupplierInvoice[index].id)
              .toList();
          return InkWell(
            onTap: () {
              var geList = supplierInvoiceProvider.listSupplierInvoiceDetails
                  .where((element) =>
                      element.supplierInvoiceId ==
                      emptyListOfSupplierInvoice[index].id)
                  .toList();

              var total = 0.0;

              geList.forEach((element) {
                var rounded = ((element.quantity));

                var sumOfQuantityofShtuk =
                    (((rounded! - rounded.toInt()) * 100).round() / 100);

                sumOfQuantityofShtuk =
                    (sumOfQuantityofShtuk * element.kolvoUpakovka!)
                        .roundToDouble();

                total += element.price! * rounded.toInt() +
                    ((sumOfQuantityofShtuk * element.price!) /
                        element.kolvoUpakovka!);
              });

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SupplierEachDetailScrenn(
                            total: total,
                            listOfSupplierDetails: geList,
                            getDateTime:
                                emptyListOfSupplierInvoice[index].datatime,
                            supplierInvoice: emptyListOfSupplierInvoice[index],
                            fromReturnedPage: false,
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${emptyListOfSupplierInvoice[index].id}",
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "Дата: ${emptyListOfSupplierInvoice[index].datatime.toString().substring(0, 11)}",
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "Время: ${emptyListOfSupplierInvoice[index].datatime.toString().substring(11, 19)}",
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                  if (geList.every((element) => element.returned == 1))
                    const Text(
                      "Возвращено",
                      style: TextStyle(color: Colors.redAccent),
                    )
                  else
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "Общая сумма",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          emptyListOfSupplierInvoice[index]
                              .total!
                              .toStringAsFixed(2),
                          style: const TextStyle(fontStyle: FontStyle.normal),
                        ),
                      ],
                    )
                ],
              ),
            ),
          );
        });
  }
}
