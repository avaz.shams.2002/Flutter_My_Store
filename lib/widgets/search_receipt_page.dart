import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../models/receipt_model.dart';
import '../models/receipt_product_model.dart';
import '../pages/receipt_page_about.dart';
import '../providers/receipt_and_tovar_provider.dart';

class SearchReceiptPage extends SearchDelegate {
  List<Receipt>? listReceipt;
  List<ReceiptProduct>? listReceiptProduct;

  SearchReceiptPage({this.listReceipt, this.listReceiptProduct});

  var dateTimeStart = DateTime.now();
  var dateTimeEnd = DateTime.now();
  var dateTime = DateTimeRange(start: DateTime.now(), end: DateTime.now());
  DateTimeRange? newDateTime;

  bool empty = false;

  String getDateTime = '';

  @override
  void initState() {
    dateTime = DateTimeRange(start: dateTimeStart, end: dateTimeEnd);

    getDateTime =
        "Период с ${dateTime.start.toString().substring(0, 11)} по ${dateTime.end.toString().substring(0, 11)}: ";
  }

  List<Receipt> listByDay = [];

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () async {
            listByDay = [];
            newDateTime = await showDateRangePicker(
                context: context,
                initialDateRange: dateTime,
                locale: const Locale("ru", "RU"),
                firstDate: DateTime(2010),
                lastDate: DateTime(2100));

            if (newDateTime == null) return;
            FocusManager.instance.primaryFocus!.unfocus();
            DateTime startDate = newDateTime!.start;
            DateTime endDate = newDateTime!.end;

            dateTime = DateTimeRange(start: startDate, end: endDate);

            getDateTime =
                "Период с ${dateTime.start.toString().substring(0, 11)} по ${dateTime.end.toString().substring(0, 11)}: ";

            int difference = endDate.difference(startDate).inDays;

            var items = List<DateTime>.generate(difference + 1, (index) {
              DateTime date = startDate;

              return date.add(Duration(days: index));
            });

            if (items.isEmpty) {
              items.add(startDate);
            }
            for (var allList in listReceipt!) {
              for (var all in items) {
                String day =
                    "${all.year}-${all.month < 10 ? "0${all.month}" : all.month}-${all.day < 10 ? "0${all.day}" : all.day}"
                        .trim();

                if (allList.dateTime!.substring(0, 11).trim() == day) {
                  listByDay.add(allList);
                }
              }
            }
            empty = false;
            query = '';
            if (listByDay.isEmpty) {
              FocusManager.instance.primaryFocus!.unfocus();
              // EasyLoading.showError("Поиск не дал результатов")
              //     .timeout(const Duration(microseconds: 500));
              empty = true;
            }
          },
          icon: const Icon(Icons.date_range)),
      IconButton(
          onPressed: () {
            query = '';
            listByDay = [];
            getDateTime = '';
            dateTime =
                DateTimeRange(start: DateTime.now(), end: DateTime.now());
            empty = false;
          },
          icon: const Icon(Icons.close))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Receipt> list = [];

    if (query.isNotEmpty) {
      listByDay = [];
    }

    var receiptProvider = Provider.of<ReceipAndTovarProvider>(context);
    for (var all in listReceipt!) {
      if (all.id.toString().contains(query) ||
          all.dateTime
              .toString()
              .substring(0, 11)
              .toUpperCase()
              .trim()
              .contains(query.toUpperCase().trim())) {
        list.add(all);
      }
    }

    if (listByDay.isNotEmpty) {
      if (empty == false) {
        list = listByDay;
      }
    }
    if (empty == true) {
      return Column(
        children: [
          Padding(
            padding:
                EdgeInsets.only(left: 12, top: getDateTime.isEmpty ? 0 : 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  getDateTime,
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
          ),
          const Expanded(child: Text("")),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text("Поиск не дал результатов"),
            ],
          ),
          const Expanded(child: Text("")),
        ],
      );
    }
    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          var returnedTovar = receiptProvider.listProductMaker
              .where((element) =>
                  element.receiptId == receiptProvider.listReceipt[index].id)
              .toList();
          return InkWell(
            onTap: () {
              List<ReceiptProduct> listReceipt = [];
              listReceipt = receiptProvider.listProductMaker
                  .where((element) =>
                      element.receiptId ==
                      receiptProvider.listReceipt[index].id)
                  .toList();
              // for (int i = 0; i < listReceipt.length; i++) {
              //   print(listReceipt[i].quantity);
              //   print("receipt id: ${listReceipt[i].receiptId}");
              //   print(listReceipt[i].prodazhiPrice);
              // }
              //
              // print("List length: ${listReceipt.length}");
              // print(receiptProvider.listProductMaker.length);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ReceiptPageAbout(
                            productMaker: listReceipt,
                            receipt: receiptProvider.listReceipt[index],
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      const Icon((Icons.money_outlined)),
                      const SizedBox(
                        width: 30,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(list[index].id.toString()),
                          Text(
                              "Время: ${list[index].dateTime!.substring(0, 11)} в "
                              "${list[index].dateTime!.substring(11, 16)}")
                        ],
                      )
                    ],
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(double.parse(list[index].total!)
                            .toStringAsFixed(2)),
                        if (returnedTovar
                            .every((element) => element.returned == 1))
                          const Text(
                            "Возвращено",
                            style: TextStyle(color: Colors.redAccent),
                          )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Receipt> list = [];
    var receiptProvider = Provider.of<ReceipAndTovarProvider>(context);

    if (query.isNotEmpty) {
      listByDay = [];
    }

    for (var all in listReceipt!) {
      if (all.id.toString().contains(query) ||
          all.dateTime
              .toString()
              .substring(0, 11)
              .toUpperCase()
              .trim()
              .contains(query.toUpperCase().trim())) {
        list.add(all);
      }
    }
    if (empty == true) {
      return Column(
        children: [
          Padding(
            padding:
                EdgeInsets.only(left: 12, top: getDateTime.isEmpty ? 0 : 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  getDateTime,
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
          ),
          const Expanded(child: Text("")),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text("Поиск не дал результатов"),
            ],
          ),
          const Expanded(child: Text("")),
        ],
      );
    }
    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          var returnedTovar = receiptProvider.listProductMaker
              .where((element) =>
                  element.receiptId == receiptProvider.listReceipt[index].id)
              .toList();
          return InkWell(
            onTap: () {
              List<ReceiptProduct> listReceipt = [];
              listReceipt = receiptProvider.listProductMaker
                  .where((element) =>
                      element.receiptId ==
                      receiptProvider.listReceipt[index].id)
                  .toList();
              // for (int i = 0; i < listReceipt.length; i++) {
              //   print(listReceipt[i].quantity);
              //   print("receipt id: ${listReceipt[i].receiptId}");
              //   print(listReceipt[i].prodazhiPrice);
              // }
              //
              // print("List length: ${listReceipt.length}");
              // print(receiptProvider.listProductMaker.length);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ReceiptPageAbout(
                            productMaker: listReceipt,
                            receipt: receiptProvider.listReceipt[index],
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      const Icon((Icons.money_outlined)),
                      const SizedBox(
                        width: 30,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(list[index].id.toString()),
                          Text(
                              "Время: ${list[index].dateTime!.substring(0, 11)} в "
                              "${list[index].dateTime!.substring(11, 16)}")
                        ],
                      )
                    ],
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(double.parse(list[index].total!)
                            .toStringAsFixed(2)),
                        if (returnedTovar
                            .every((element) => element.returned == 1))
                          const Text(
                            "Возвращено",
                            style: TextStyle(color: Colors.redAccent),
                          )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
