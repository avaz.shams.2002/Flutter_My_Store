import 'package:flutter/material.dart';
import 'package:my_store/screens/change_price_screen.dart';
import 'package:provider/provider.dart';

import '../models/product_make_model.dart';
import '../providers/discount_provider.dart';

class EachProductSearch extends SearchDelegate {
  List<ProductMaker>? list;

  EachProductSearch({this.list});

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<ProductMaker> res = [];
    for (var all in list!) {
      if (all.naimenovanie!.toUpperCase().contains(query.toUpperCase())) {
        res.add(all);
      }
    }
    var discountProvider = Provider.of<DiscountProvier>(context);
    return ListView.builder(
        itemCount: res.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {


              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangePriceScreen(
                            productMaker: res[index],
                            discount: discountProvider.listDiscount,
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    res[index].naimenovanie!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<ProductMaker> res = [];
    for (var all in list!) {
      if (all.naimenovanie!.toUpperCase().contains(query.toUpperCase())) {
        res.add(all);
      }
    }
    var discountProvider = Provider.of<DiscountProvier>(context);
    return ListView.builder(
        itemCount: res.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {


              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangePriceScreen(
                            productMaker: res[index],
                            discount: discountProvider.listDiscount,
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    res[index].naimenovanie!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
