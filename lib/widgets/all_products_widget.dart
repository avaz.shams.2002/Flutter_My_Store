import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

import '../database/db/product_maker_db.dart';
import '../providers/discount_provider.dart';
import '../providers/product_maker_provider.dart';
import '../screens/change_price_screen.dart';

class AllProductsWidget extends StatelessWidget {
  const AllProductsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var discountProvider = Provider.of<DiscountProvier>(context);
    var productMaker = Provider.of<ProductMakerProvider>(context);
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: Colors.black,
          thickness: 1,
        ),
        itemCount: productMaker.getNotArchiveTovar().length,
        itemBuilder: (context, index) {
          var rounded = ((productMaker.getNotArchiveTovar()[index].quantity));

          var sumOfQuantityofShtuk =
              (((rounded - rounded.toInt()) * 100).round() / 100);

          sumOfQuantityofShtuk = (sumOfQuantityofShtuk *
                  int.parse(
                      productMaker.getNotArchiveTovar()[index].kolvoUpakovka!))
              .roundToDouble();

          return InkWell(
            onTap: () async {
              // if (productMaker.getNotArchiveTovar()[index].prodazhiPrice ==
              //     0.0) {
              //   await ProductMakerDb.addItemToDbProduct(
              //       productMaker.getNotArchiveTovar()[index]);
              //   productMaker.notify();
              //
              //   var ownDiscountDetail = discountProvider.listDiscountDetail
              //       .where((element) =>
              //           element.productId ==
              //           productMaker.getNotArchiveTovar()[index].id)
              //       .toList();
              //
              //   Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //           builder: (context) => ChangePriceScreen(
              //                 productMaker:
              //                     productMaker.getNotArchiveTovar()[index],
              //                 discount: discountProvider.listDiscount,
              //                 discountDetails: ownDiscountDetail,
              //               )));
              //   EasyLoading.showError("Ошибка цены");
              //   return;
              // }
              await ProductMakerDb.addItemToDbProduct(
                  productMaker.getNotArchiveTovar()[index]);
              productMaker.notify();
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      if (productMaker
                                  .getNotArchiveTovar()[index]
                                  .image
                                  .toString() ==
                              "File: 'null'" ||
                          productMaker.getNotArchiveTovar()[index].image ==
                              null)
                        InkWell(
                          onTap: () {


                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChangePriceScreen(
                                          productMaker: productMaker
                                              .getNotArchiveTovar()[index],
                                          discount:
                                              discountProvider.listDiscount,
                                        )));
                          },
                          child: Container(
                            width: 100,
                            height: 50,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: AssetImage("assets/images/1.jpg"),
                                fit: BoxFit.fitWidth,
                                alignment: Alignment.bottomLeft,
                              ),
                            ),
                            child: Center(
                              child: Text(
                                "${productMaker.getNotArchiveTovar()[index].naimenovanie![0]}"
                                "${productMaker.getNotArchiveTovar()[index].naimenovanie![productMaker.getNotArchiveTovar()[index].naimenovanie!.length - 1]}",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                          ),
                        )
                      else
                        InkWell(
                          onTap: () {


                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChangePriceScreen(
                                          productMaker: productMaker
                                              .getNotArchiveTovar()[index],
                                          discount:
                                              discountProvider.listDiscount,
                                        )));
                          },
                          child: Container(
                              width: 100,
                              height: 50,
                              child: Image.file(productMaker
                                  .getNotArchiveTovar()[index]
                                  .image!)),
                        ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              if (productMaker
                                  .getNotArchiveTovar()[index]
                                  .naimenovanie!
                                  .isNotEmpty)
                                Text(
                                    "${productMaker.getNotArchiveTovar()[index].code} "
                                    "• ${productMaker.getNotArchiveTovar()[index].naimenovanie}"),
                              const SizedBox(
                                height: 10,
                              ),
                              if (productMaker
                                          .getNotArchiveTovar()[index]
                                          .quantity ==
                                      0 &&
                                  sumOfQuantityofShtuk == 0)
                                const Text(
                                  "Нажмите чтобы добавить",
                                  style: TextStyle(color: Colors.pink),
                                )
                              else
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    const Icon(
                                      Icons.shopping_cart_checkout_outlined,
                                      color: Colors.pink,
                                    ),
                                    Text(productMaker
                                        .getNotArchiveTovar()[index]
                                        .prodazhiPrice
                                        .toString()),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                            productMaker
                                                .getNotArchiveTovar()[index]
                                                .prodazhiCurrency!
                                                .toLowerCase(),
                                            style: const TextStyle(
                                                color: Colors.grey,
                                                fontSize: 11)),
                                      ],
                                    ),
                                    Text(
                                      " (${productMaker.getNotArchiveTovar()[index].quantity.toInt().toString()}-уп",
                                      style: const TextStyle(
                                          color: Colors.black, fontSize: 14),
                                    ),
                                    if (sumOfQuantityofShtuk != 0)
                                      Text(
                                        ", ${sumOfQuantityofShtuk.toInt()}-шт",
                                        style: const TextStyle(
                                            color: Colors.black, fontSize: 14),
                                      ),
                                    const Text(
                                      ")",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 17),
                                    )
                                  ],
                                ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {


                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChangePriceScreen(
                                        productMaker: productMaker
                                            .getNotArchiveTovar()[index],
                                        discount: discountProvider.listDiscount,
                                      )));
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              productMaker.productsmaker[index]
                                  .getTotalOfProduct()
                                  .toStringAsFixed(2),
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17),
                            ),
                            // if (productMaker.productsmaker[index].discount!
                            //         .percentOrSumma ==
                            //     1)
                            //   Text(
                            //     productMaker.productsmaker[index]
                            //                 .totalDiscount! ==
                            //             0.0
                            //         ? ""
                            //         : "-${productMaker.productsmaker[index].totalDiscount!}%",
                            //     style: const TextStyle(
                            //         color: Colors.grey, fontSize: 12),
                            //   ),
                            // if (productMaker.productsmaker[index].discount!
                            //         .percentOrSumma ==
                            //     2)
                            //   Text(
                            //     "-${((productMaker.productsmaker[index].totalDiscount! * productMaker.productsmaker[index].prodazhiPrice!) / 100) == 0.0 ? ""
                            //         "" : ((productMaker.productsmaker[index].totalDiscount! * productMaker.productsmaker[index].prodazhiPrice!) / 100).toStringAsFixed(2)}с",
                            //     style: const TextStyle(
                            //         color: Colors.grey, fontSize: 12),
                            //   ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
