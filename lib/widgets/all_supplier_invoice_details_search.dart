import 'package:flutter/material.dart';
import 'package:my_store/screens/change_price_supplier_screen.dart';

import '../models/supplier_invoice_details_model.dart';

class AllSupplierInvoiceDetailsSearch extends SearchDelegate {
  List<SupplierInvoiceDetails>? supplierList;
  dynamic refreshParent;

  AllSupplierInvoiceDetailsSearch({this.supplierList, this.refreshParent});

  @override
  List<Widget>? buildActions(BuildContext context) {
    IconButton(
        onPressed: () {
          query = '';
        },
        icon: const Icon(Icons.close));
  }

  @override
  Widget? buildLeading(BuildContext context) {
    IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: const Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<SupplierInvoiceDetails> emptySupplier = [];

    for (var all in supplierList!) {
      if (all.name!.toUpperCase().contains(query.toUpperCase())) {
        emptySupplier.add(all);
      }
    }

    return ListView.builder(
        itemCount: emptySupplier.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangePriceSupplierScreen(
                            supplierInvoiceDetails: emptySupplier[index],
                            refreshParent: refreshParent,
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                emptySupplier[index].name!,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<SupplierInvoiceDetails> emptySupplier = [];

    for (var all in supplierList!) {
      if (all.name!.toUpperCase().contains(query.toUpperCase())) {
        emptySupplier.add(all);
      }
    }

    return ListView.builder(
        itemCount: emptySupplier.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangePriceSupplierScreen(
                            supplierInvoiceDetails: emptySupplier[index],
                            refreshParent: refreshParent,
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                emptySupplier[index].name!,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
          );
        });
  }
}
