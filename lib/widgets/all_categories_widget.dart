import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/product_make_model.dart';
import '../providers/category_provider.dart';
import '../providers/category_provider.dart';
import '../providers/product_maker_provider.dart';
import '../screens/category_product_screen.dart';

class AllCategoriesWidget extends StatefulWidget {
  int? checkForCategory;

  final dynamic notify;
  List<ProductMaker>? listPro;

  AllCategoriesWidget(
      {Key? key, this.checkForCategory, this.listPro, this.notify})
      : super(key: key);

  @override
  State<AllCategoriesWidget> createState() => _AllCategoriesWidgetState();
}

class _AllCategoriesWidgetState extends State<AllCategoriesWidget> {
  @override
  Widget build(BuildContext context) {
    var productMaker = Provider.of<ProductMakerProvider>(context);
    var categoryProvider = Provider.of<CategoryProvider>(context);
    return ListView.separated(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            widget.checkForCategory =
                categoryProvider.getCategoryNotArchive()[index].id;
            widget.listPro = [];
            widget.listPro = productMaker
                .getNotArchiveTovar()
                .where((element) =>
                    element.categoryId == widget.checkForCategory.toString())
                .toList();
            print(widget.checkForCategory);
            print(widget.listPro);
            widget.notify(widget.listPro, widget.checkForCategory);
          },
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(categoryProvider
                                  .getCategoryNotArchive()[index]
                                  .colorInt!)),
                          width: 100,
                          height: 50,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(categoryProvider
                                .getCategoryNotArchive()[index]
                                .name!),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(categoryProvider
                                .getCategoryNotArchive()[index]
                                .createdAt
                                .toString()),
                          ],
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
      itemCount: categoryProvider.getCategoryNotArchive().length,
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: Colors.black,
        thickness: 1,
      ),
    );
  }
}
