import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';

import '../models/product_make_model.dart';
import '../providers/discount_provider.dart';
import '../screens/change_price_screen.dart';

class CustomShowDelegate extends SearchDelegate {
  List<ProductMaker>? productmaker;

  CustomShowDelegate({this.productmaker});

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            FlutterBarcodeScanner.scanBarcode(
                    "#000000", "Cancel", true, ScanMode.BARCODE)
                .then((value) => query = value);
          },
          icon: Icon(Icons.camera_alt)),
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<ProductMaker> pro = [];
    for (var get in productmaker!) {
      if (get.naimenovanie
              .toString()
              .toUpperCase()
              .contains(query.toUpperCase()) ||
          get.shtrihkod
              .toString()
              .toUpperCase()
              .contains(query.toUpperCase())) {
        print(productmaker!.length);
        pro.add(get);
      }
    }
    return ListView.builder(
        itemCount: pro.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  pro[index].naimenovanie!,
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    var discountProvider = Provider.of<DiscountProvier>(context);
    List<ProductMaker> pro = [];
    for (var get in productmaker!) {
      if (get.naimenovanie
              .toString()
              .toUpperCase()
              .contains(query.toUpperCase()) ||
          get.shtrihkod
              .toString()
              .toUpperCase()
              .contains(query.toUpperCase())) {
        print(productmaker!.length);
        pro.add(get);
      }
    }
    return ListView.builder(
        itemCount: pro.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.pop(context);



              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangePriceScreen(
                            productMaker: pro[index],
                            discount: discountProvider.listDiscount
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pro[index].naimenovanie!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
