import 'package:flutter/material.dart';
import 'package:my_store/screens/catergory_creator_screen.dart';

import '../models/category_maker_model.dart';

class CategorySearchPageWidget extends SearchDelegate {
  List<Category>? listOfCategory;
  CategorySearchPageWidget(this.listOfCategory);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.close))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Category> listRes = [];
    for (var all in listOfCategory!) {
      if (all.name!.toUpperCase().contains(query.toUpperCase())) {
        listRes.add(all);
      }
    }
    return ListView.builder(
        itemCount: listRes.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CategoryCreatorScreen(
                            category: listRes[index],
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                listRes[index].name!,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Category> listRes = [];
    for (var all in listOfCategory!) {
      if (all.name!.toUpperCase().contains(query.toUpperCase())) {
        listRes.add(all);
      }
    }
    return ListView.builder(
        itemCount: listRes.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CategoryCreatorScreen(
                            category: listRes[index],
                          )));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                listRes[index].name!,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
            ),
          );
        });
  }
}
