import 'package:flutter/material.dart';
import 'package:my_store/providers/contact_person_provider.dart';
import 'package:provider/provider.dart';

class ContactPersonWidget extends StatelessWidget {
  int? id;
  int? contragent_id;
  late String? nname = '';
  late String? ddolzhnost = '';
  late String? ttelephone = '';
  late String? eemail = '';
  late String? ccomment = '';
  ContactPersonWidget(
      {Key? key,
        this.id,
      this.contragent_id,
      this.nname = '',
      this.ddolzhnost = '',
      this.ttelephone = '',
      this.eemail = '',
      this.ccomment = ''})
      : super(key: key);

  final _formKey = GlobalKey<FormState>();

  late TextEditingController name = TextEditingController(text: nname);
  late TextEditingController dolzhnost =
      TextEditingController(text: ddolzhnost);
  late TextEditingController telephone =
      TextEditingController(text: ttelephone);
  late TextEditingController email = TextEditingController(text: eemail);
  late TextEditingController comment = TextEditingController(text: ccomment);

  @override
  Widget build(BuildContext context) {
    var setContact = Provider.of<ContactPersonProvider>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: [
          TextButton(
              onPressed: () {
                // print(name.text);
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                if (!setContact.itemExists(this)) {
                  setContact.addWidget(
                    fam: name.text.isEmpty ? '' : name.text,
                    dol: dolzhnost.text.isEmpty ? '' : dolzhnost.text,
                    tel: telephone.text.isEmpty ? '' : telephone.text,
                    email: email.text.isEmpty ? '' : email.text,
                    commemt: comment.text.isEmpty ? '' : comment.text,
                  );
                }
                setContact.notify();

                Navigator.pop(context);
              },
              child: const Text(
                "СОХРАНИТЬ",
                style: const TextStyle(color: Colors.white, fontSize: 20),
              )),
          if (setContact.itemExists(this))
            IconButton(
                onPressed: () {
                  setContact.deleteWidget(this);
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.delete,
                  color: Colors.white,
                ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 60, right: 60),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: name,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
                    hintText: "ФИО"),
                validator: (value) {
                  if (value!.isEmpty) return "Поле не может быть пустым";
                },
              ),
              TextField(
                controller: dolzhnost,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
                    hintText: "Должность"),
              ),
              TextFormField(
                validator: (val) {
                  if (val!.isEmpty) {
                    return 'Поле не может быть пустым';
                  }
                },
                controller: telephone,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
                    hintText: "Телефон"),
              ),
              TextField(
                controller: email,
                decoration: const InputDecoration(
                    hintStyle:
                        const TextStyle(color: Colors.grey, fontSize: 14),
                    hintText: "Email"),
              ),
              TextField(
                controller: comment,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
                    hintText: "Комментарий"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
