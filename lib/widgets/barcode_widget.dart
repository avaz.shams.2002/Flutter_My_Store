import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/barcode_provider.dart';
import '../screens/type_code.dart';

class BarcodeWidget extends StatelessWidget {
  String? code;
  BarcodeWidget({Key? key, this.code}) : super(key: key);

  late final TextEditingController _controller =
      TextEditingController(text: code);
  TextEditingController get controller => _controller;
  // final TextEditingController _controllerCode = TextEditingController(text: '');
  // TextEditingController get controllerCode => _controllerCode;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      readOnly: true,
      decoration: const InputDecoration(
          labelText: "Тип",
          labelStyle: TextStyle(color: Colors.grey, fontSize: 14)),
    );
  }
}
