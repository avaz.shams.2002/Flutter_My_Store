import 'package:flutter/material.dart';

import '../models/category_maker_model.dart';
import '../models/product_make_model.dart';
import '../screens/category_product_screen.dart';

class CategorySearch extends SearchDelegate {
  List<Category>? listCategory = [];
  int? checkCategory;
  List<ProductMaker>? productMaker;
  List<ProductMaker>? list;
  dynamic refresh;
  CategorySearch(
      {this.listCategory,
      this.checkCategory,
      this.list,
      this.refresh,
      this.productMaker});

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(Icons.close))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Category> pro = [];
    for (var get in listCategory!) {
      if (get.name.toString().toUpperCase().contains(query.toUpperCase())) {
        print(listCategory!.length);
        pro.add(get);
      }
    }
    return ListView.builder(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemCount: pro.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              checkCategory = pro[index].id;
              list = [];
              list = productMaker!
                  .where((element) =>
                      element.categoryId == checkCategory.toString())
                  .toList();
              print(list!.length);
              refresh(list, checkCategory);
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pro[index].name!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Category> pro = [];
    for (var get in listCategory!) {
      if (get.name.toString().toUpperCase().contains(query.toUpperCase())) {
        print(listCategory!.length);
        pro.add(get);
      }
    }
    return ListView.builder(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemCount: pro.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              checkCategory = pro[index].id;
              list = [];
              list = productMaker!
                  .where((element) =>
                      element.categoryId == checkCategory.toString())
                  .toList();
              print(list!.length);

              refresh(list, checkCategory);
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pro[index].name!,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
