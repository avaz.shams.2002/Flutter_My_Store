import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class ShtrihCodeWidget extends StatelessWidget {
  ShtrihCodeWidget({Key? key}) : super(key: key);
  // late String _data = '';
  late TextEditingController _typeController = TextEditingController(text: '');

  TextEditingController get typeController => _typeController;

  @override
  Widget build(BuildContext context) {
    var _textStyle = TextStyle(color: Colors.grey, fontSize: 14);
    return Stack(
      children: [
        TextField(
          controller: _typeController,
          decoration: InputDecoration(labelText: "Код", labelStyle: _textStyle),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: IconButton(
              onPressed: () {
                FlutterBarcodeScanner.scanBarcode(
                        "#000000", "Cancel", true, ScanMode.BARCODE)
                    .then((value) => _typeController.text = value);
              },
              icon: const Icon(
                Icons.camera_alt,
                color: Colors.blueGrey,
              )),
        )
      ],
    );
  }
}
