import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/product_make_model.dart';
import '../providers/product_maker_provider.dart';

class Widgetttt extends StatefulWidget {
  const Widgetttt({Key? key}) : super(key: key);

  @override
  State<Widgetttt> createState() => _WidgettttState();
}

class _WidgettttState extends State<Widgetttt> {
  Map<String, int?> map = {"Создать товар": -2};

  List<String> edIzmereniyList = [
    'шт',
    'гр',
    'кг',
    'см',
    'м',
    'см2',
    'м2',
    'л',
    'мг',
    'мл',
    'час',
    'мин',
    'пор'
  ];

  late TextEditingController _getOrCreateProductController =
      TextEditingController(text: '');
  late TextEditingController _naimenovanieController =
      TextEditingController(text: '');
  late TextEditingController _categoryController =
      TextEditingController(text: '');
  late TextEditingController _kolVoUpakovka = TextEditingController(text: '1');
  late TextEditingController _upakovka = TextEditingController(text: '');
  late TextEditingController _senaProdazhi = TextEditingController(text: '0.0');
  List<ProductMaker> list = [];

  @override
  void initState() {
    var products =
        Provider.of<ProductMakerProvider>(context, listen: false).productsmaker;
    for (int i = 0; i < products.length; i++) {
      map[products[i].naimenovanie!] = products[i].id;
    }
    print(map);
  }

  @override
  Widget build(BuildContext context) {
    var productsProvider = Provider.of<ProductMakerProvider>(context);
    return Column(
      children: [
        Expanded(
          child: ListView(
            children: [
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: Stack(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            const Expanded(
                              child: Text(
                                "Товар :",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                            ),
                            SizedBox(
                              width: 150,
                              child: TextField(
                                keyboardType: TextInputType.multiline,
                                readOnly: true,
                                controller: _getOrCreateProductController,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 5,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                          items: map
                              .map((key, value) {
                                return MapEntry(
                                    key,
                                    DropdownMenuItem(
                                      onTap: () {
                                        setState(() {});
                                        if (value == -2) {
                                          _getOrCreateProductController.text =
                                              '';
                                          _naimenovanieController.text = '';
                                          _categoryController.text = '';
                                          _kolVoUpakovka.text = '1';
                                          _upakovka.text = '';
                                          return;
                                        }
                                        productsProvider.productsmaker
                                            .where((element) =>
                                                element.id == value)
                                            .forEach((element) {
                                          _getOrCreateProductController.text =
                                              element.naimenovanie!;
                                          _naimenovanieController.text =
                                              element.naimenovanie!;
                                          _categoryController.text =
                                              element.groupe!;
                                          _upakovka.text = element.upakovka!;
                                          _kolVoUpakovka.text =
                                              element.kolvoUpakovka!;

                                          list.add(element);
                                        });
                                      },
                                      value: key,
                                      child: Text(key),
                                    ));
                              })
                              .values
                              .toList(),
                          onChanged: (val) {}),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Expanded(
                      child: Text(
                        "Наименование :",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: TextField(
                        decoration: const InputDecoration(
                            label: Text("Наименование"),
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 12)),
                        controller: _naimenovanieController,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Expanded(
                      child: Text(
                        "Категории :",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: TextField(
                        decoration: const InputDecoration(
                            labelText: "Категории",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black))),
                        controller: _categoryController,
                        readOnly: true,
                        onTap: () {},
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Expanded(
                      child: Text(
                        "Кол-во в упаковке:",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: TextField(
                        controller: _kolVoUpakovka,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            labelText: "Кол-во",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Expanded(
                      child: Text(
                        "Упаковка :",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: TextField(
                        readOnly: true,
                        onTap: () {
                          showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (context) => Container(
                                    height: MediaQuery.of(context).size.height,
                                    color: Colors.blueAccent,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 25),
                                      child: Scaffold(
                                          appBar: AppBar(
                                            backgroundColor: Colors.blueAccent,
                                            elevation: 0,
                                            leading: IconButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                icon: const Icon(
                                                    Icons.arrow_back)),
                                            iconTheme: const IconThemeData(
                                                color: Colors.white),
                                          ),
                                          body: ListView.separated(
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10,
                                                    top: 10,
                                                    bottom: 10),
                                                child: InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        _upakovka.text =
                                                            edIzmereniyList[
                                                                index];
                                                      });
                                                      Navigator.pop(context);
                                                    },
                                                    child: Text(
                                                      edIzmereniyList[index],
                                                      style: const TextStyle(
                                                          fontSize: 20),
                                                    )),
                                              );
                                            },
                                            itemCount: edIzmereniyList.length,
                                            separatorBuilder:
                                                (BuildContext context,
                                                        int index) =>
                                                    const Divider(),
                                          )),
                                    ),
                                  ));
                        },
                        controller: _upakovka,
                        decoration: const InputDecoration(
                            labelText: "Упаковка*",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Expanded(
                      child: Text(
                        "Упаковка :",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: TextFormField(
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Пустое поле";
                          }
                        },
                        onTap: () {
                          _senaProdazhi.text = '';
                        },
                        controller: _senaProdazhi,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 14),
                            labelText: "Цена продажи"),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 70,
          child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.green)),
              onPressed: () {},
              child: const Center(
                child: Text(
                  "Добавить",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              )),
        )
      ],
    );
  }
}
