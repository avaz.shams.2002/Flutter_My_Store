import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:my_store/pages/home_page.dart';
import 'package:my_store/pages/login_page.dart';
import 'package:my_store/providers/category_provider.dart';
import 'package:my_store/providers/client_provider.dart';
import 'package:my_store/providers/discount_provider.dart';
import 'package:my_store/providers/product_cart_provider.dart';
import 'package:my_store/providers/product_maker_provider.dart';
import 'package:my_store/providers/barcode_provider.dart';
import 'package:my_store/providers/check_field_login_page.dart';
import 'package:my_store/providers/check_field_reg_page.dart';
import 'package:my_store/providers/contact_person_provider.dart';
import 'package:my_store/providers/contragent_provider.dart';
import 'package:my_store/providers/counries_from_api_provider.dart';
import 'package:my_store/providers/current_page_provider.dart';
import 'package:my_store/providers/payment_account_provider.dart';
import 'package:my_store/providers/prices_provider.dart';
import 'package:my_store/providers/receipt_and_tovar_provider.dart';
import 'package:my_store/providers/shtrihcode_provider.dart';
import 'package:my_store/providers/supplier_invoice_details_provider.dart';
import 'package:my_store/providers/supplier_invoice_provider.dart';
import 'package:my_store/providers/user_check.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<ClientProvider>(create: (_) => ClientProvider()),
      ChangeNotifierProvider<SupplierInvoiceDetailsProvider>(
          create: (_) => SupplierInvoiceDetailsProvider()),
      ChangeNotifierProvider<SupplierAllProvider>(
          create: (_) => SupplierAllProvider()),
      ChangeNotifierProvider<ReceipAndTovarProvider>(
          create: (_) => ReceipAndTovarProvider()),
      ChangeNotifierProvider<DiscountProvier>(create: (_) => DiscountProvier()),
      ChangeNotifierProvider<CategoryProvider>(
          create: (_) => CategoryProvider()),
      ChangeNotifierProvider<ProductCartProvider>(
          create: (_) => ProductCartProvider()),
      ChangeNotifierProvider<ContragentProvider>(
          create: (_) => ContragentProvider()),
      ChangeNotifierProvider<ShtrihCodePRovider>(
          create: (_) => ShtrihCodePRovider()),
      ChangeNotifierProvider<PayMentAccountProvider>(
          create: (_) => PayMentAccountProvider()),
      ChangeNotifierProvider<ContactPersonProvider>(
          create: (_) => ContactPersonProvider()),
      ChangeNotifierProvider<Prices>(create: (_) => Prices()),
      ChangeNotifierProvider<Barcode>(create: (_) => Barcode()),
      ChangeNotifierProvider<CountriesFromApiProvider>(
          create: (_) => CountriesFromApiProvider()),
      ChangeNotifierProvider<ProductMakerProvider>(
          create: (_) => ProductMakerProvider()),
      ChangeNotifierProvider<CheckUser>(create: (_) => CheckUser()),
      ChangeNotifierProvider<CurrentStateProvider>(
          create: (_) => CurrentStateProvider()),
      ChangeNotifierProvider<CheckFieldRegPage>(
          create: (_) => CheckFieldRegPage()),
      ChangeNotifierProvider<CheckFieldLoginPage>(
          create: (_) => CheckFieldLoginPage())
    ],
    child: MaterialApp(
      builder: EasyLoading.init(),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('ru'),
      ],
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.white),
      home: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snap) {
          if (snap.hasData) {
            return const HomePage();
          } else {
            return const LoginPage();
          }
        },
      ),
    ),
  ));
}
